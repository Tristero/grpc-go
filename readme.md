# gRPC et Go

Ce sont des notes de cours et traductions issues du MOOC de S. Maarek sur Udemy [gRPC [Golang] Master Class: Build Modern API & Microservices](https://www.udemy.com/course/grpc-golang/).

## Pré-requis

Il y a **deux pré-requis** indispensables pour la bonne compréhension du cours :

- une bonne connaissance de Go (savoir ce qu'est une interface, la composition, les structures)
- une connaissance de base de Protocol Buffer 3 (savoir ce qu'est un message, comment utiliser protoc, ...)

## V1 (obsolète)

Le dépôt comportait à l'origine deux répertoires :

- ```calculateur``` : contient les réponses aux exercices et qui **n'est pas disponible ici**. La raison de son absence de présence est double. La première est que le formateur propose sa solution et la seconde est que le développeur se doit d'utiliser ses propres compétences (c'est en forgeant que l'on devient forgeron !) et non attendre une solution tombant du ciel. **NB** dans la section avancée (chapitre 8), on utilisera ce répertoire de façon effective !
- ```salutation``` qui contient l'ensemble des codes. Aucune branche n'a été créée par API : il n'y en a qu'une seule. Je n'avais pas prévu de poster mes notes sur GL.

Nouvelle version :

- Les notes de cours ont été revues et corrigées
- chaque TP dispose de son propre répertoire et devient un module propre

Les notes se trouvent dans le répertoire [cours](./cours/0-cours.md).

## V2 - 2022

- le cours est revu par un nouveau formateur
- de même que le code source
- la version V1 ne sera plus présente que sous la forme d'une archive à la fin de la création de la V2
