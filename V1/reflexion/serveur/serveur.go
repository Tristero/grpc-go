package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"reflexion/calculpb"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type service struct{}

func (*service) FindMax(stream calculpb.CalculService_FindMaxServer) error {

	var max int64

	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			log.Println("... Fin de réception et de transmission")
			return nil
		}
		if err != nil {
			log.Printf("Erreur à la réception d'une donnée : %v", err)
			return err
		}
		log.Printf("Réception de la valeur : %d", msg.GetNb())
		if msg.GetNb() > max {
			log.Printf("%d est supérieur à la dernière valeur stockée %d (réponse envoyée)", msg.GetNb(), max)
			max = msg.GetNb()

			if err := stream.Send(&calculpb.MaxReponse{
				Nb: max,
			}); err != nil {
				return err
			}
		}
	}

}

func (*service) Moyenne(stream calculpb.CalculService_MoyenneServer) error {
	log.Println("Appel d'un calcul de moyenne")
	var somme int64 = 0

	i := 0
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Panicf("Erreur à la réception d'un message  : %v", err)
		}

		somme += req.GetNb()
		i++
	}

	rep := &calculpb.CalcReponse{
		Reponse: float32(somme) / float32(i),
	}

	return stream.SendAndClose(rep)
}

func (*service) CalculPremiers(req *calculpb.PremierRequete, stream calculpb.CalculService_CalculPremiersServer) error {
	log.Println("Début de la transaction")

	nb := req.GetNb()
	var k int64 = 2

	for nb > 1 {
		if (nb % k) == 0 {
			fmt.Println(k)
			reponse := &calculpb.PremierReponse{
				Rslt: k,
			}
			if err := stream.Send(reponse); err != nil {
				return err
			}
			nb = nb / k
		} else {
			k = k + 1
		}

		time.Sleep(50 * time.Millisecond)
	}
	log.Println("Fin de la transaction")
	return nil
}

func (*service) CalculSomme(ctx context.Context, req *calculpb.SommeRequete) (*calculpb.SommeReponse, error) {
	log.Println("Appel de la méthode CalculSomme")

	rslt := req.Somme.X + req.Somme.Y
	log.Printf("Avec \nX=%d\nY=%d\nRésultat calculé attendu par le serveur = %d",
		rslt,
		req.Somme.X,
		req.Somme.Y,
	)

	return &calculpb.SommeReponse{
		Reponse: rslt,
	}, nil
}

func main() {
	fmt.Println("Lancement du serveur intégrant un service de réflexion")

	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Erreur à la création du listener : %v", err)
	}
	defer l.Close()

	s := grpc.NewServer()
	calculpb.RegisterCalculServiceServer(s, &service{})

	reflection.Register(s)

	if err := s.Serve(l); err != nil {
		log.Panicf("Erreur au lancement du serveur : %v", err)
	}
}
