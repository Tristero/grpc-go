package main

import (
	"blog/blogpb"
	"context"
	"fmt"
	"log"

	"google.golang.org/grpc"
)

var blogID string

func main() {

	fmt.Println("Lancement du client")
	cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Connexion impossible : %v", err)
	}
	defer cnx.Close()

	client := blogpb.NewBlogServiceClient(cnx)

	// Fonctions helpers
	//        Création du blog
	createBlog(client)

	//        Lecture du blog
	readBlog(client)

	//        Mise à jour du blog
	updateBlog(client)

	//        Suppression d'une entrée du blog
	deleteBlog(client)
}

// création d'une entrée pour le blog
func createBlog(client blogpb.BlogServiceClient) {
	log.Println("Création d'une nouvelle entrée pour le blog")

	rep, err := client.CreateBlog(
		context.Background(),
		&blogpb.CreateBlogRequest{
			Blog: &blogpb.Blog{
				AuthorId: "Alice",
				Content:  "Contenu d'un article",
				Title:    "Premier article",
			},
		})
	if err != nil {
		log.Panicf("Erreur à l'envoi de la requête : %v", err)
	}

	b := rep.GetBlog()
	fmt.Printf("Article enregistré : %s", b.String())
	blogID = b.GetId()
}

// Lecture d'une entrée
func readBlog(client blogpb.BlogServiceClient) {
	log.Println("Lecture d'un article")

	// Premier cas : une erreur volontaire. L'ID est aléatoire
	_, err := client.ReadBlog(context.Background(), &blogpb.ReadBlogRequest{
		BlogID: "random1234",
	})
	if err != nil {
		log.Printf("Comme prévu nous avons une erreur : %v", err)
	}

	// Second cas : la réponse retourne une valeur
	rep, err := client.ReadBlog(context.Background(),
		&blogpb.ReadBlogRequest{
			BlogID: blogID,
		})
	if err != nil {
		log.Printf("Erreur inattendue : %v", err)
		return
	}
	log.Printf("Donnée reçue: %v", rep.GetBlog())
}

// Mise à jour d'une entrée
func updateBlog(client blogpb.BlogServiceClient) {
	log.Println("Mise à jour d'une entrée du blog")

	// Mise à jour de l'entrée
	updtBlog := &blogpb.Blog{
		Id:       blogID,
		AuthorId: "Nouvel auteur",
		Title:    "Titre modifié",
		Content:  "Nouveau contenu. Ou plutôt contenu modifié",
	}

	rep, err := client.UpdateBlog(
		context.Background(),
		&blogpb.UpdateBlogRequest{
			Blog: updtBlog,
		})
	if err != nil {
		log.Printf("Erreur sur la mise à jour de l'entrée : %v", err)
	}

	log.Printf("Mise à jour de l'entrée : %v", rep.GetBlog())
}

func deleteBlog(client blogpb.BlogServiceClient) {
	log.Println("Demande de suppression d'un article")

	// 1-envoi de la demande de suppression
	rep, err := client.DeleteBlog(
		context.Background(),
		&blogpb.DeleteBlogRequest{
			BlogID: blogID,
		},
	)
	// 2-gestion des erreurs
	if err != nil {
		log.Printf("Erreur est survenue au moment de la suppression : %v", err)
	}

	// 3-si tout va bien on affiche le résultat
	log.Printf("Document %s a été supprimé avec succès", rep.GetBlogID())
}
