package main

import (
	"blog/blogpb"
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
)

var coll *mongo.Collection

type blogItem struct {
	ID       primitive.ObjectID `bson:"_id,omitempty"`
	AuthorID string             `bson:"author_id"`
	Content  string             `bson:"content"`
	Title    string             `bson:"title"`
}

type service struct{}

func (*service) CreateBlog(ctx context.Context, req *blogpb.CreateBlogRequest) (*blogpb.CreateBlogResponse, error) {

	blog := req.GetBlog()

	// mappage des champs de la requête avec un objet blogItem nouveau
	data := blogItem{
		AuthorID: blog.GetAuthorId(),
		Content:  blog.GetContent(),
		Title:    blog.GetTitle(),
	}

	// enregistrement de la nouvelle entrée dans la base à l'aide de InsertOne
	log.Println("Demande d'insertion d'une nouvelle entrée pour le blog")
	rslt, err := coll.InsertOne(ctx, data)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("Erreur interne : %v", err),
		)
	}

	if oid, ok := rslt.InsertedID.(primitive.ObjectID); ok {
		log.Printf("Enregistrement réussi : ID n°%s", oid.Hex())
		return &blogpb.CreateBlogResponse{
			Blog: &blogpb.Blog{
				Id:       oid.Hex(),
				AuthorId: blog.GetAuthorId(),
				Title:    blog.GetTitle(),
				Content:  blog.GetContent(),
			},
		}, nil
	}
	return nil, status.Errorf(
		codes.Internal,
		"Erreur de casting sur l'identifiant unique",
	)
}

func (*service) ReadBlog(ctx context.Context, req *blogpb.ReadBlogRequest) (*blogpb.ReadBlogResponse, error) {
	log.Println("Demande de lecture d'un article")

	// récupération de l'ID et convertion
	blogID, err := primitive.ObjectIDFromHex(req.GetBlogID())

	// Gestion de l'erreur : code erreur est que l'on a reçu un argument non valide
	if err != nil {
		return nil, status.Errorf(
			codes.InvalidArgument,
			fmt.Sprintf("L'ID demandé n'existe pas : %v", err),
		)
	}

	// Récupération du document dans la base avec la fonction bson.D
	blog := &blogItem{}
	rslt := coll.FindOne(
		ctx,
		bson.D{primitive.E{Key: "_id", Value: blogID}},
	)
	if err := rslt.Decode(blog); err != nil {
		return nil, status.Errorf(
			codes.NotFound,
			fmt.Sprintf("Document non trouvé avec l'ID transmis : %v", err),
		)
	}

	return &blogpb.ReadBlogResponse{
		Blog: &blogpb.Blog{
			Id:       blog.ID.Hex(),
			AuthorId: blog.AuthorID,
			Content:  blog.Content,
			Title:    blog.Title,
		},
	}, nil
}

func (*service) UpdateBlog(ctx context.Context, req *blogpb.UpdateBlogRequest) (*blogpb.UpdateBlogResponse, error) {
	log.Println("Demande de modification d'un document/article")

	// Récupération de la structure Blog
	blog := req.GetBlog()
	oid, err := primitive.ObjectIDFromHex(blog.GetId())
	if err != nil {
		return nil, status.Errorf(
			codes.InvalidArgument,
			"Le document demandé n'existe pas",
		)
	}

	// Mise à jour de notre structure interne
	data := &blogItem{
		AuthorID: blog.GetAuthorId(),
		Content:  blog.GetContent(),
		Title:    blog.GetTitle(),
	}
	filtre := bson.D{
		primitive.E{
			Key:   "_id",
			Value: oid,
		},
	}
	// Version alternative avec bson.M
	//filter := bson.M{"_id": oid}

	// Mise à jour du document dans la base de données
	_, err = coll.ReplaceOne(
		ctx,
		filtre,
		data,
	)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("Le document demandé ne peut pas être mis à jour : %v", err),
		)
	}

	// Retour en mappant les champs de data avec le type Blog
	return &blogpb.UpdateBlogResponse{
		Blog: &blogpb.Blog{
			Id:       oid.Hex(),
			AuthorId: data.AuthorID,
			Content:  data.Content,
			Title:    data.Title,
		},
	}, nil
}

func (*service) DeleteBlog(ctx context.Context, req *blogpb.DeleteBlogRequest) (*blogpb.DeleteBlogResponse, error) {
	log.Println("Demande de suppression d'une entrée dans le blog")

	// 1-récupération de l'ID envoyé par le client
	id := req.GetBlogID()

	// 2-transformation en un objet reconnaissable par MongoDB
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, status.Errorf(
			codes.InvalidArgument,
			fmt.Sprintf("Document introuvable : %v", err),
		)
	}

	// 3-suppression
	rslt, err := coll.DeleteOne(
		ctx,
		bson.D{primitive.E{
			Key:   "_id",
			Value: oid,
		}},
	)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("Erreur inattendue est survenue au moment de supprimer le document : %v", err),
		)
	}
	// 4-Erreur si le nombre de suppression est inférieur strict à 1
	if rslt.DeletedCount < 1 {
		return nil, status.Errorf(
			codes.NotFound,
			fmt.Sprintf("Erreur - le document n'a pas été supprimé : %v", err),
		)
	}

	// 5-Si tout s'est bien déroulé on retourne l'ID envoyé par le client
	return &blogpb.DeleteBlogResponse{
		BlogID: req.GetBlogID(),
	}, nil
}

func (*service) ListBlog(req *blogpb.ListBlogRequest, stream blogpb.BlogService_ListBlogServer) error {
	log.Println("Demande de récupération de l'ensemble des entrées du blog")

	// 1-Récupération des documents
	ctx := context.Background()
	cur, err := coll.Find(ctx, bson.D{})
	if err != nil {
		return status.Errorf(
			codes.Internal,
			fmt.Sprintf("Erreur avec la demande Find() : %v", err),
		)
	}

	// 2-Gestion du curseur
	defer cur.Close(ctx)

	for cur.Next(ctx) {
		b := &blogItem{}
		if err := cur.Decode(b); err != nil {
			return status.Errorf(
				codes.Internal,
				fmt.Sprintf("Erreur de décodage : %v", err),
			)
		}
		// Envoi en flux des données
		if err := stream.Send(&blogpb.ListBlogResponse{
			Blog: &blogpb.Blog{
				Id:       b.ID.Hex(),
				AuthorId: b.AuthorID,
				Title:    b.Title,
				Content:  b.Content,
			},
		}); err != nil {
			return status.Errorf(
				codes.Internal,
				fmt.Sprintf("Erreur dans l'envoi d'un document : %v", err),
			)
		}
	}
	return nil
}

func main() {
	fmt.Println("Lancement du serveur")
	// Connexion avec le serveur MongoDB
	// Création d'un contexte avec un timeout
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Connexion à la base de données
	// Si le serveur est local :
	// client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	// Si le serveur est déployé via Atlas : ici pour des questions de sécurité basique
	// on se créera une fonction de connexion retournant une chaîne représentant l'URI
	// complète
	client, err := mongo.Connect(ctx,
		options.Client().ApplyURI(connect()))
	if err != nil {
		log.Fatalf("Erreur à la création du client : %v", err)
	}

	// Diffère la déconnexion à l'arrêt du programme
	defer func() {
		if err := client.Disconnect(ctx); err != nil {
			log.Panicf("Erreur à la déconnexion : %v", err)
		}
	}()

	// Vérification de la connexion avec la base
	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		// Pas connexion -> Panic
		log.Panicf("Erreur à l'accès à MongoDB : %v", err)
	} else {
		log.Println("Connexion avec MongoDB réussie")
	}

	// On se connecte à la base et à une collection :
	// si elles n'existent pas alors elles sont créées
	coll = client.Database("mydb").Collection("blog")

	// Si le code crash, on souhaite obtenir le nom de fichier et le numéro de la ligne
	// Cela nous permet de connaître où le crash s'est déroulé
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Erreur à la création du listener")
	}
	defer func() {
		log.Println("Appel du defer : arrêt du Listener")
		l.Close()
	}()

	s := grpc.NewServer()
	blogpb.RegisterBlogServiceServer(s, &service{})
	// Ajout de la réflexion au service
	reflection.Register(s)
	
	// Attente du Ctrl+C pour sortir
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	// Lancement du serveur
	go func() {
		log.Println("Lancement du serveur gRPC")
		if err := s.Serve(l); err != nil {
			log.Panicf("Erreur du serveur : %v", err)
		}
		<-ch
	}()

	// Blocage jusqu'à la réception du signal d'arrêt
	<-ch

	// Arrêt du serveur
	log.Println("Arrêt du serveur gRPC")
	s.Stop()
	log.Println("Fin du programme")
}
