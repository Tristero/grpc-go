package main

import (
	"api-stream-client/hellopb"
	"fmt"
	"io"
	"log"
	"net"
	"strings"

	"google.golang.org/grpc"
)

type srvService struct{}

func (*srvService) ServiceStreamClient(stream hellopb.HelloService_ServiceStreamClientServer) error {
	log.Println("Invocation du service ServiceStreamClient")

	var builder strings.Builder

	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			// Envoi de la réponse
			rep := builder.String()
			return stream.SendAndClose(&hellopb.HelloReponse{
				Reponse: rep,
			})
		}
		if err != nil {
			return err
		}
		builder.WriteString("Hello ")
		builder.WriteString(msg.Message.GetPrenom())
		builder.WriteString(" ! ")
	}
}

func main() {
	fmt.Println("Serveur - API Stream Client")

	lst, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Erreur à la création du serveur : %v", err)
	}
	defer lst.Close()

	srvgRPC := grpc.NewServer()
	hellopb.RegisterHelloServiceServer(srvgRPC, &srvService{})

	log.Panic(srvgRPC.Serve(lst))

}
