package main

import (
	"api-stream-client/hellopb"
	"context"
	"fmt"
	"log"
	"time"

	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Client - API stream Client")

	cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Erreur à la création de la connexion : %v", err)
	}
	defer cnx.Close()

	client := hellopb.NewHelloServiceClient(cnx)

	stream, err := client.ServiceStreamClient(context.Background())
	if err != nil {
		log.Panicf("Erreur à la création du client : %v", err)
	}

	noms := [][]string{
		{"Ada", "Lovelace"},
		{"Alan", "Turing"},
		{"Brian", "Kernighan"},
		{"Dennis", "Ritchie"},
		{"Ken", "Thompson"},
		{"Rob", "Pike"},
		{"Robert", "Griesemer"},
	}

	for i, nom := range noms {
		req := &hellopb.HelloRequete{
			Message: &hellopb.HelloMessage{
				Nom:    nom[1],
				Prenom: nom[0],
			},
		}

		// Envoi de la requête
		log.Printf("Envoi de la requête n° %d", i+1)
		if err := stream.Send(req); err != nil {
			log.Printf("Erreur à l'envoi des données : %v", err)
			break
		}

		time.Sleep(100 * time.Millisecond)
	}

	// Fin de boucle et attente de la réponse du serveur
	rep, err := stream.CloseAndRecv()
	if err != nil {
		log.Panicf("Erreur à la réception de la réponse du serveur : %v", err)
	}
	log.Printf("Réponse du serveur : %v", rep.GetReponse())
}
