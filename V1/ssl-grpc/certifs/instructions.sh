#!/bin/bash

# Résumé
# fichiers privés : ca.key, server.key, server.pem, server.crt
# fichiers "partagés" : ca.crt (requis par le client), server.crt (requis par le CA)
# Script inspiré de https://github.com/grpc/grpc-java/tree/master/examples#generating-self-signed-certificates-for-use-with-grpc
# Pour éviter une erreur, bien vérifier que le fichier cache .rnd existe : 
# set RANDFILE=$HOME/.rnd

SERVER_CN=localhost

# 1 - génère le CA + certificat de confiance (ca.crt)
openssl genrsa -passout pass:1111 -des3 -out ca.key 4096
openssl req -passin pass:1111 -new -x509 -days 3650 -key ca.key -out ca.crt -subj "/CN=${SERVER_CN}"

# 2 - génère la clé privée du serveur (server.key)
openssl genrsa -passout pass:1111 -des3 -out server.key 4096

# 3 - crée un certificat signé depuis le CA (server.csr)
openssl req -passin pass:1111 -new -key server.key -out server.csr -subj "/CN=${SERVER_CN}" -config ssl.cnf

# 4 - signe le certificat avec le CA nouvellement créé (il s'autosigne) - server.crt
openssl x509 -req -passin pass:1111 -days 3650 -in server.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out server.crt -extensions req_ext -extfile ssl.cnf

# 5 - conversion du certificat serveur en format .pem (server.pem) utilisable par gRPC
openssl pkcs8 -topk8 -nocrypt -passin pass:1111 -in server.key -out server.pem