package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"ssl-grpc/hellopb"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

var tls bool

func init() {
	flag.BoolVar(&tls, "ssl", false, "accepte une connexion sécurisée")
}

func main() {
	flag.Parse()
	fmt.Println("Lancement du client sécurisé")

	opts := grpc.WithInsecure()
	if tls {
		// si on demande une connexion sécurisée alors l'option doit changer et
		// on doit récupérer le certificat
		creds, err := credentials.NewClientTLSFromFile("../certifs/ca.crt", "")
		if err != nil {
			log.Fatalf("Echec à la récupération du justificatif : %v", err)
		}
		opts = grpc.WithTransportCredentials(creds)
	}

	cnx, err := grpc.Dial("localhost:50051", opts)

	if err != nil {
		log.Fatalf("Echec à la connexion : %v", err)
	}
	defer cnx.Close()

	client := hellopb.NewHelloSSLServiceClient(cnx)
	rep, err := client.HelloSSL(context.Background(), &hellopb.HelloRequete{
		Hello: &hellopb.Hello{
			Nom:    "Strange",
			Prenom: "Stephen",
		},
	})
	if err != nil {
		log.Panicf("Erreur à l'envoi de la donnée : %v", err)
	}
	fmt.Printf("Réponse : %v\n", rep.GetReponse())
}
