package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"ssl-grpc/hellopb"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

var tls bool

func init() {
	flag.BoolVar(&tls, "ssl", false, "active ou non la connexion sécurisée")
}

type service struct{}

func (*service) HelloSSL(ctx context.Context, req *hellopb.HelloRequete) (*hellopb.HelloReponse, error) {
	fmt.Printf("La fonction SalutationRequete a été invoquée avec %v\n", req)

	// Extraction des nom et prénom
	p, n := req.GetHello().GetPrenom(), req.GetHello().GetNom()
	// Création de la réponse à retourner
	rep := fmt.Sprintf("Salut %s %s", p, n)
	// Réponse sous forme d'une structure dont on récupère l'adresse
	reponse := &hellopb.HelloReponse{
		Reponse: rep,
	}
	return reponse, nil
}

func main() {
	flag.Parse()
	if tls {
		fmt.Println("Lancement du serveur sécurisé")
	} else {
		fmt.Println("Lancement du serveur NON-sécurisé")

	}
	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Erreur à la création du listener : %v", err)
	}
	defer l.Close()

	// Création des options de connexion
	var opts = []grpc.ServerOption{}

	// Vérification de la connexion sécurisée ou non
	if tls {
		// Création de credentials
		creds, err := credentials.NewServerTLSFromFile("../certifs/server.crt", "../certifs/server.pem")
		if err != nil {
			log.Panicf("Erreur à la création des credentials : %s", err)
		}
		opts = append(opts, grpc.Creds(creds))

	}
	// Ajout des certificats via une option : grpc.Creds
	s := grpc.NewServer(opts...)

	hellopb.RegisterHelloSSLServiceServer(s, &service{})
	if err := s.Serve(l); err != nil {
		log.Panicf("Erreur au lancement du serveur : %v", err)
	}
}
