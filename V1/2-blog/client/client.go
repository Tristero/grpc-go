package main

import (
	"blog/blogpb"
	"context"
	"fmt"
	"log"

	"google.golang.org/grpc"
)

func main() {

	fmt.Println("Lancement du client")
	cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Connexion impossible : %v", err)
	}
	defer cnx.Close()

	client := blogpb.NewBlogServiceClient(cnx)

	createBlog(client)
}

// création d'une entrée pour le blog
func createBlog(client blogpb.BlogServiceClient) {
	log.Println("Création d'une nouvelle entrée pour le blog")

	rep, err := client.CreateBlog(
		context.Background(),
		&blogpb.CreateBlogRequest{
			Blog: &blogpb.Blog{
				AuthorId: "Alice",
				Content:  "Contenu du premier article",
				Title:    "Premier article",
			},
		})
	if err != nil {
		log.Panicf("Erreur à l'envoi de la requête : %v", err)
	}

	fmt.Printf("Article enregistré : %v", rep.GetBlog())
}
