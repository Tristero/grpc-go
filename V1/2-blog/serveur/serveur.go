package main

import (
	"blog/blogpb"
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var coll *mongo.Collection

type blogItem struct {
	ID       primitive.ObjectID `bson:"_id,omitempty"`
	AuthorID string             `bson:"author_id"`
	Content  string             `bson:"content"`
	Title    string             `bson:"title"`
}

type service struct{}

func (*service) CreateBlog(ctx context.Context, req *blogpb.CreateBlogRequest) (*blogpb.CreateBlogResponse, error) {

	blog := req.GetBlog()

	// mappage des champs de la requête avec un objet blogItem nouveau
	data := blogItem{
		AuthorID: blog.GetAuthorId(),
		Content:  blog.GetContent(),
		Title:    blog.GetTitle(),
	}

	// enregistrement de la nouvelle entrée dans la base à l'aide de InsertOne
	log.Println("Demande d'insertion d'une nouvelle entrée pour le blog")
	rslt, err := coll.InsertOne(ctx, data)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("Erreur interne : %v", err),
		)
	}

	if oid, ok := rslt.InsertedID.(primitive.ObjectID); ok {
		log.Printf("Enregistrement réussi : ID n°%s", oid.Hex())
		return &blogpb.CreateBlogResponse{
			Blog: &blogpb.Blog{
				Id:       oid.Hex(),
				AuthorId: blog.GetAuthorId(),
				Title:    blog.GetTitle(),
				Content:  blog.GetContent(),
			},
		}, nil
	}
	return nil, status.Errorf(
		codes.Internal,
		"Erreur de casting sur l'identifiant unique",
	)

}

func main() {
	fmt.Println("Lancement du serveur")
	// Connexion avec le serveur MongoDB
	// Création d'un contexte avec un timeout
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Connexion à la base de données
	// Si le serveur est local :
	// client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	// Si le serveur est déployé via Atlas : ici pour des questions de sécurité basique
	// on se créera une fonction de connexion retournant une chaîne représentant l'URI
	// complète
	client, err := mongo.Connect(ctx,
		options.Client().ApplyURI(connect()))
	if err != nil {
		log.Fatalf("Erreur à la création du client : %v", err)
	}

	// Diffère la déconnexion à l'arrêt du programme
	defer func() {
		if err := client.Disconnect(ctx); err != nil {
			log.Panicf("Erreur à la déconnexion : %v", err)
		}
	}()

	// Vérification de la connexion avec la base
	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		// Pas connexion -> Panic
		log.Panicf("Erreur à l'accès à MongoDB : %v", err)
	} else {
		log.Println("Connexion avec MongoDB réussie")
	}

	// On se connecte à la base et à une collection :
	// si elles n'existent pas alors elles sont créées
	coll = client.Database("mydb").Collection("blog")

	// Si le code crash, on souhaite obtenir le nom de fichier et le numéro de la ligne
	// Cela nous permet de connaître où le crash s'est déroulé
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Erreur à la création du listener")
	}
	defer func() {
		log.Println("Appel du defer : arrêt du Listener")
		l.Close()
	}()

	s := grpc.NewServer()
	blogpb.RegisterBlogServiceServer(s, &service{})

	// Attente du Ctrl+C pour sortir
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	// Lancement du serveur
	go func() {
		log.Println("Lancement du serveur gRPC")
		if err := s.Serve(l); err != nil {
			log.Panicf("Erreur du serveur : %v", err)
		}
		<-ch
	}()

	// Blocage jusqu'à la réception du signal d'arrêt
	<-ch

	// Arrêt du serveur
	log.Println("Arrêt du serveur gRPC")
	s.Stop()
	log.Println("Fin du programme")
}
