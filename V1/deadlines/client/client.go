package main

import (
	"context"
	"deadlines/hellopb"
	"flag"
	"fmt"
	"log"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var duree int

func init() {
	flag.IntVar(&duree, "duree", 1, "indique la durée de la deadline (entier)")
}

func main() {
	flag.Parse()
	fmt.Println("Lancement du client testant les deadlines")

	cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Erreur à la création de la connexion : %v", err)
	}
	defer cnx.Close()

	client := hellopb.NewHelloServiceClient(cnx)

	// Gestion du Timeout
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(duree)*time.Second)
	defer cancel()

	rep, err := client.HelloDeadlineService(ctx, &hellopb.HelloRequete{
		Requete: &hellopb.Hello{
			Nom:    "Turing",
			Prenom: "Alan",
		},
	})
	if err != nil {
		if s, ok := status.FromError(err); ok {
			if s.Code() == codes.DeadlineExceeded {
				log.Println("Timeout ! La deadline a été atteinte ...")
			} else {
				log.Printf("Erreur inattendue : %v", s.Message())
			}
		} else {
			log.Panicf("erreur à l'envoi de la requête : %v", err)
		}
		// on arrête le client normalement : il n'y a rien d'autre à faire
		return
	}
	fmt.Println(rep.GetReponse())
}
