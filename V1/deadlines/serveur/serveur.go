package main

import (
	"context"
	"deadlines/hellopb"
	"fmt"
	"log"
	"net"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type service struct{}

func (*service) HelloDeadlineService(ctx context.Context, req *hellopb.HelloRequete) (*hellopb.HelloReponse, error) {
	log.Printf("Appel de la méthode HelloDeadlineService avec %v", req)

	for i := 0; i < 3; i++ {
		if ctx.Err() == context.DeadlineExceeded {
			log.Println(ctx.Err())
			// le client a annulé la requête
			log.Println("Deadline atteinte pour le client : annulation de la requête")
			// on retourne une erreur
			return nil, status.Error(codes.Canceled, "le client a annulé la requête")
		}
		time.Sleep(1 * time.Second)
	}

	reponse := fmt.Sprintf("Hello %s %s", req.GetRequete().GetPrenom(), req.GetRequete().GetNom())

	return &hellopb.HelloReponse{
		Reponse: reponse,
	}, nil
}

func main() {
	fmt.Println("Lancement du serveur - gestion des deadlines")

	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Erreur au lancement du listener : %v", err)
	}
	defer l.Close()

	s := grpc.NewServer()
	hellopb.RegisterHelloServiceServer(s, &service{})
	if err := s.Serve(l); err != nil {
		log.Panicf("Erreur au lancement du serveur gRPC : %v", err)
	}
}
