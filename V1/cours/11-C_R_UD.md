# Lecture d'un blog

## 1-Protocol Buffer

Le service RPC se nommera ```ReadBlog``` et demandera en argument un ```ReadBlogRequest``` pour retourner une ```ReadBlogResponse```. Tout simplement.

La requête envoyée au serveur consiste en l'envoi d'une simple chaîne de caractère : **l'id de l'article**.

La réponse est plus pernicieuse car il y a une possibilité que la réponse ne retourne rien : à ce moment-là, on retournera un ```NOT_FOUND```. Sinon c'est l'article de type ```Blog``` qui sera retourné.

Implémentation :

```proto
message ReadBlogRequest {
    string blogID = 1;
}

message ReadBlogResponse {
    Blog blog = 1;
}

service BlogService {
    rpc CreateBlog (CreateBlogRequest) returns (CreateBlogResponse);
    rpc ReadBlog (ReadBlogRequest) returns (ReadBlogResponse);
}
```

On compile le code Protocol Buffer. Et le nouveau code Go généré fait que notre code ```serveur.go``` est dorénavant erroné : il nous faut implémenter la méthode ```ReadBlog()```.

## 2-Modification du serveur

### 1-Implémentation de la méthode ReadBlog

La signature de la nouvelle fonction associée au serveur :

```go
func (*server) ReadBlog(ctx context.Context, req *blogpb.ReadBlogRequest) (*blogpb.ReadBlogResponse, error) {}
```

Le cas le plus "difficile" à gérer est celui où il n'y a pas d'article. Autrement le code à implémenter ne posera aucun problème.

Tout d'abord on récupère l'ID envoyé dans la requête client puis il doit être converti en ```ObjectID``` puisque l'ID reçu est de type ```string```. Pour cela, la bibliothèque MongBD ```primitive``` dipose d'une fonction ```ObjectIDFromHex()``` qui demande un chaîne en paramètre (l'ID) et retourne un type ```primitive.ObjectID``` et une erreur.

### 2-Gestion de l'erreur

Gérons d'abord l'erreur. Pour cela, on doit retourner nil et une erreur. Pour l'erreur, on utilisera ce que l'on a déjà vu : la fonction ```status.Errorf()``` à laquelle on passera deux arguments :

- code erreur : ```codes.InvalidArgument```
- une chaîne : un message formaté avec ```Sprintf()```

*Reamrque* : Pourquoi InvalidArgument plutôt que NotFound ? C'est au choix mais on peut arguer le fait que l'utilisateur a transmis un mauvais argument.

```go
// récupération de l'ID et convertion
blogID, err := primitive.ObjectIDFromHex(req.GetBlogID())

// Gestion de l'erreur : code erreur est que l'on a reçu un argument non valide
if err != nil {
    return nil, status.Errorf(
        codes.InvalidArgument,
        fmt.Sprintf("L'ID demandé n'existe pas : %v", err),
    )
}
```

### 3-Récupération de l'article

Pour stocker notre future entrée lue depuis la base de données, on va se créer une variable de type ```*BlogItem``` sans aucune valeur :

```go
blog := &BlogItem{}
```

Nous voulons récupérer l'article désiré. Le driver MongoDB dispose de la méthode ```FindOne()```. Elle est associée à un objet de type ```Collection``` : ici ```coll```. On passera à ```FindOne()``` deux arguments : le contexte et un *filtre*. **A noter** que l'on peut passer un troisième argument : des options (que nous ne verrons pas ici).

Mais comment passer notre ID puisque l'on doit passer un filtre ? La documentation n'est pas claire : côté documentation, on nous dit que le "filtre" est un document contenant des opérateurs de requête ([doc officielle de MongoDB](https://docs.mongodb.com/manual/reference/command/find/)). Le filtre doit être un document BSON. La solution est de faire appel au package ```bson``` : celui-ci dipose d'un type ```M``` que l'on emploie comme suit :

```go
bson.M{"foo": "bar", "hello": "world", "pi": 3.14159}
```

**Version alternative** - On peut utiliser ```bson.D{}``` à la place de ```M```. ```D``` sera un slice de documents alors que ```M``` représentera une Map de type ```map[string]interface{}```. Avec ```M```, on a une "liste" ordonnée et ```D``` on a une liste non-ordonnée. Mais la méthode ```D``` nécessite de préciser la clé et la valeur : pour cela il faut recourir à ```primitive.E{Key: clé, Value: valeur}```

```go
bson.D{primitive.E{Key: "foo", Value: "bar"}, primitive.E{Key: "hello", Value: "world"}, primitive.E{Key: "pi", Value: 3.14159}}
```

Donc le code ressemblera avec ```bson.M``` :

```go
coll.FindOne(
    ctx,
    bson.M{"_id": blogID},
)
```

La version avec ```bson.D``` :

```go
rslt := coll.FindOne(
    ctx,
    bson.D{primitive.E{Key: "_id", Value: blogID}},
)
```

La fonction ```FindOne()``` retourne un pointeur de type ```mongo.SingleResult```. Ce type est une structure contenant quatre champs non publiques. ```mongo.SingleResult``` représente un document unique que l'on doit décoder avec sa méthode ```Decode()```. Cette méthode prend un argument qui sera du type correspondant à la structure du document retourné (ici ```blogItem```) et elle retournera une erreur :

```go
if rslt.Decode(data) != nil {}
```

On pourra traiter les données stockées dans l'objet ```blog```.

Et pour gérer l'erreur et la retourner, on va utiliser à nouveau ```status.Errorf()``` comme précédemment.

```go
if err := rslt.Decode(blog); err != nil {
    return nil, status.Errorf(
        codes.NotFound,
        fmt.Sprintf("Document non trouvé avec l'ID transmis : %v", err),
    )
}
```

Nous pouvons dorénavant nous focaliser sur le contenu du document stocké dans ```blog```.

Pour retourner le contenu, il nous faut extraire le contenu de ```blog``` et le mapper. Nous avons un problème avec le champ ```blog.ID``` car c'est un type ```primitive.ObjectID``` et on demande un type ```string```. On dispose d'une solution : on extrait la valeur avec la méthode ```Hex()``` qui ne prend aucune valeur (puisque c'est une fonction associée au type ```ObjectID```) et retourne une chaîne.

### 4-Code complet de notre fonction

```go
func (*service) ReadBlog(ctx context.Context, req *blogpb.ReadBlogRequest) (*blogpb.ReadBlogResponse, error) {
    log.Println("Demande de lecture d'un article")

    // récupération de l'ID et convertion
    blogID, err := primitive.ObjectIDFromHex(req.GetBlogID())

    // Gestion de l'erreur : code erreur est que l'on a reçu un argument non valide
    if err != nil {
        return nil, status.Errorf(
            codes.InvalidArgument,
            fmt.Sprintf("L'ID demandé n'existe pas : %v", err),
        )
    }

    // Récupération du document dans la base avec la fonction bson.D
    blog := &blogItem{}
    rslt := coll.FindOne(
        ctx,
        bson.D{primitive.E{Key: "_id", Value: blogID}},
    )
    if err := rslt.Decode(blog); err != nil {
        return nil, status.Errorf(
            codes.NotFound,
            fmt.Sprintf("Document non trouvé avec l'ID transmis : %v", err),
        )
    }

    return &blogpb.ReadBlogResponse{
        Blog: &blogpb.Blog{
            Id:       blog.ID.Hex(),
            AuthorId: blog.AuthorID,
            Content:  blog.Content,
            Title:    blog.Title,
        },
    }, nil
}
```

## 3-Modification du client

### 1-Fonction helper

Comme pour la création, on utilise une fonction helper :

```go
...

func main(){

    ...

    // Fonctions helpers
    //        Création du blog
    createBlog(client)

    //        Lecture du blog
    readBlog(client)

}

func readBlog(client blogpb.BlogServiceClient) {}
```

### 2-Objectifs

Nous avons deux points à gérer :

- implémenter la méthode ```ReadBlog()``` en lui passant un contexte, et une requête (toujours aucune option) qui demande un champ BlogId de type ```string```
- et cette méthode retourne deux valeurs : un pointeur ```ReadBlogResponse``` et une erreur que l'on va gérer.

On créera deux appels de cette méthode ```ReadBlog()``` : l'un avec un BlogId erroné et l'autre en récupérant la valeur à la création d'un blog (via une modification légère du code de la méthode helper ```createBlog()```)

### 3-Traitement des erreurs et des données

Pour que cette modification n'impacte pas l'ensemble de la fonction-helper, nous allons créer une variable globale ```blogID``` de type string et lui affecter le résultat de l'appel de ```GetId()```

Code :

```go
// on se crée une variable globale sans affectation
var blogID string

...

func createBlog(client blogpb.BlogServiceClient){

    ...

    b := rep.GetBlog()
    fmt.Printf("Article enregistré : %s", b.String())
    blogID = b.GetId()
}
```

### 4-Code de readBlog()

Pour le premier appel de ```ReadBlog()```, nous allons utiliser un ID inventé et nous ne nous intéresserons pas à la réponse (puisqu'elle sera ```nil```). De même, nous n'utiliserons pas ```log.Fatalf()``` mais ```log.Printf()```.

Le second appel de ```ReadBlog()``` utilisera un ID valide généré par l'appel de ```createBlog()```.

```go
func readBlog(client blogpb.BlogServiceClient) {
    log.Println("Lecture d'un article")

    // Premier cas : une erreur volontaire. L'ID est aléatoire
    _, err := client.ReadBlog(context.Background(), &blogpb.ReadBlogRequest{
        BlogID: "random1234",
    })
    if err != nil {
        log.Printf("Comme prévu nous avons une erreur : %v", err)
    }

    // Second cas : la réponse retourne une valeur
    rep, err := client.ReadBlog(context.Background(),
        &blogpb.ReadBlogRequest{
            BlogID: blogID,
        })
    if err != nil {
        log.Printf("Erreur inattendue : %v", err)
        return
    }
    log.Printf("Donnée reçue: %v", rep.GetBlog())
}
```

Il ne reste plus qu'à lancer le client et observer le comportement des deux appels. On pourra changer les valeurs des champs dans ```&blogpb.Blog{}``` de la fonction-helper ```createBlog()```.

```txt
Lancement du client
2021/02/19 10:26:51 Création d'une nouvelle entrée pour le blog
Article enregistré : id:"602f845bf2420022adc9caa7"  author_id:"Alice"  title:"Premier article"  content:"Contenu d'un article"2021/02/19 10:26:51 Lecture d'un article
2021/02/19 10:26:51 Comme prévu nous avons une erreur : rpc error: code = InvalidArgument desc = L'ID demandé n'existe pas : encoding/hex: invalid byte: U+0072 'r'
2021/02/19 10:26:52 Donnée reçue: id:"602f845bf2420022adc9caa7"  author_id:"Alice"  title:"Premier article"  content:"Contenu d'un article"
```

L'ensemble du code source est stocké dans le répertoire : "3-blog"

[<- Précédent](10-C_RUD.md) || [Suivant ->](12-CR_U_D.md)
