# 6-Streaming Client

Le streaming client est la capacité d'envoyer un flux de données à partir du client et cette capacité n'est possible qu'avec HTTP/2. La réponse du serveur sera unique. **ATTENTION** : il n'y a **aucune** garantie que la réponse du serveur sera donnée **APRES** la bonne réception des données du client. C'est au serveur de décider quand répondre au client.

Utilité :

- l'envoi de données massives vers un serveur
- quand le serveur doit traiter des données qui sont gourmandes en ressource : dans ce cas, il convient de les traiter les unes à la suite des autres.
- quand le client a besoin de "pusher" des données vers un serveur sans réellement attendre de réponse de ce dernier (i.e. la télémétrie)

On utilisera, côté Protocol Buffer, le mot-clé ```stream``` pour l'argument de la requête RPC. Cette requête nécessitera obligatoirement un message pour la requête et un autre pour la réponse.

## 1-Définition du code Protocole Buffer

Donc l'ojectif est d'envoyer beaucoup de message de type ```HelloMessage``` et de recevoir un résultat unique de type ```string```. Le service RPC devra préfixer le  paramètre de la fonction ou procédure avec ```stream```.

Ce qui se transcrit avec Protocol Buffer comme suit :

```proto
syntax = "proto3";

package apiStreamClient;
option go_package=".;hellopb";

message HelloMessage {
    string Prenom = 1;
    string Nom = 2;
}

message HelloRequete {
    HelloMessage message = 1;
}

message HelloReponse {
    string reponse = 1;
}

service HelloService {
    rpc ServiceStreamClient(stream HelloRequete) returns (HelloReponse){};
}
```

Générons le code source avec ```protoc```. Comme précédemment on appellera ```go mod tidy``` pour récupérer les dépendances (et optionnellement ```go mod vendor```)

## 2-Serveur et API streaming client

### 1-Implémentation du serveur (code de base)

Comme vu précédemment, le serveur est le plus compliqué à créer :

- Création d'un type struct vide (pour le futur service gRPC)
- Création d'une instance de serveur de base avec ```net.Listen()``` auquel on passe le nom du protocole et une adresse avec le port à ouvrir. Cela retourne une instance ```Listener``` et une erreur
- Gestion de l'erreur
- Différer la fermeture du ```Listener```
- Création d'un serveur gRPC avec ```grpc.NewServer()``` qui retourne un pointeur de serveur gRPC (```grpc.Server```)
- Appel de ```RegisterHelloServiceServer``` depuis la bibliothèque générée ```hellopb``` auquel on passe le pointeur ```grpc.Server``` et un pointeur du type associé au service (le type struct vide créé en premier)
- Lancement du serveur avec ```Serve()``` auquel on passe l'instance de type ```Listener``` (avec gestion de l'erreur)

### 2-Implémentation de ServiceStreamClient

**Attention** la difficulté porte ici sur la mise en place du code : notre serveur devra répondre une fois que le client aura envoyé toutes ses requêtes. **Il faut conserver à l'esprit qu'en théorie, le serveur peut répondre quand il veut !**

Ici c'est le serveur qui va récupérer un flux, traiter les données et en retourner un résultat sous forme d'une chaîne.

Nous allons implémenter notre méthode ```ServiceStreamClient```  pour cela on regarde l'interface ```HelloServiceServer``` et on copie la signature en la réactualisant pour notre serveur :

```go
func (*srvService) ServiceStreamClient(stream hellopb.HelloService_ServiceStreamClientServer) error {
    return nil
}
```

Ici le paramètre ```stream``` est un flux de données. Si on regarde les méthodes disponibles, notre variable ```stream``` dispose d'une méthode permettant la réception du flux de données : ```Recv()``` qui ne prend aucun argument et qui retourne deux types :

- un pointeur de type ```*hellopb.HelloReponse```
- une erreur

Nous allons inclure cette méthode dans une boucle ```for``` infinie  car nous ne pouvons pas savoir à l'avance le nombre de message qu'un client va envoyer. Lorsque le serveur ne recevra plus de données, une erreur sera levée de type ```io.EOF``` alors à ce moment, il faudra utiliser une méthode ```SendAndClose()```  qui nous permettra de retourner un résultat. ```SendAndClose``` prendra en argument un type ```*hellopb.HelloReponse```.

```SendAndClose``` retourne une erreur : son traitement sera très simple puisque notre méthode ```ServiceStreamClient``` retourne une erreur, on retournera directement depuis ```SendAndClose```.

Nous devrons ajouter **ensuite** une gestion d'erreur habituelle. **L'ordre importe** : d'abord la gestion du fin de flux **puis** la gestion de tout type d'erreur.

Qu'allons-nous renvoyer : une chaîne cumulant les prénoms expédiés par le client (on ne se préoccupera pas du nom)

### 3-Code du serveur

```go
package main

import (
    "api-stream-client/hellopb"
    "fmt"
    "io"
    "log"
    "net"
    "strings"

    "google.golang.org/grpc"
)

type srvService struct{}

func (*srvService) ServiceStreamClient(stream hellopb.HelloService_ServiceStreamClientServer) error {
    log.Println("Invocation du service ServiceStreamClient")

    var builder strings.Builder

    for {
        msg, err := stream.Recv()
        if err == io.EOF {
            // Envoi de la réponse
            rep := builder.String()
            return stream.SendAndClose(&hellopb.HelloReponse{
                Reponse: rep,
            })
        }
        if err != nil {
            return err
        }
        builder.WriteString("Hello ")
        builder.WriteString(msg.Message.GetPrenom())
        builder.WriteString(" ! ")
    }
}

func main() {
    fmt.Println("Serveur - API Stream Client")

    lst, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur à la création du serveur : %v", err)
    }
    defer lst.Close()

    srvgRPC := grpc.NewServer()
    hellopb.RegisterHelloServiceServer(srvgRPC, &srvService{})

    log.Panic(srvgRPC.Serve(lst))

}
```

## 3-Client et Streaming client

### 1-Code de base du client

Rappel :

- Création d'une connexion vers le serveur avec ```net.Dial()``` (retourne une instance de connexion et une erreur). On utilisera l'option ```gprc.WithInsecure()```
- Gestion de l'erreur retournée par la fonction précédente
- Différer la fermeture de la connexion
- Création d'un client gRPC avec l'appel de la fonction générée ```client := hellopb.NewHelloServiceClient()``` à laquelle est passée la connexion

### 2-Implémentation du streaming côté client

On appelle depuis notre objet client ```client``` la méthode ```ServiceStreamClient``` : elle demande un contexte et éventuellement des options, et retourne deux éléments :

- un objet de type ```hellopb.HelloService_ServiceStreamClientClient```
- une erreur

Le contexte est toujours le même : ```Background()```.

Jusqu'ici nous passions à nos méthodes **des requêtes** or ici il n'en est rien (nous n'avons qu'un paramètre et c'est un type ```Context```). Nous n'avions affaire qu'à des requêtes mais ici ce n'est plus le cas ; **nous devons gérer un flux**. Ce flux est l'objet retourné par ```ServiceStreamClient``` et dispose de méthodes d'envoi et de réception de données. Et c'est ```Send()``` que l'on sollicitera aussi souvent que l'on désire et une fois que l'on en aura fini, nous utiliserons ```CloseAndRecv()```. Cette dernière indique que l'on ferme le flux et que l'on attend la réponse du serveur.

Nous allons simuler l'envoi de plusieurs messages. Pour cela, nous allons créer un slice de pointeur ```hellopb.HelloRequete```. Pour plus de concision et de sûreté, on va générer automatiquement nos requêtes et les envoyer au serveur. La méthode ```Send()``` retourne une erreur que l'on va gérer :

```go
noms := [][]string{
    []string{"Ada", "Lovelace"},
    []string{"Alan", "Turing"},
    []string{"Brian", "Kernighan"},
    []string{"Dennis", "Ritchie"},
    []string{"Ken", "Thompson"},
    []string{"Rob", "Pike"},
    []string{"Robert", "Griesemer"},
}

for _, nom := range noms {
    req := &hellopb.HelloRequete{
        Message: &hellopb.HelloMessage{
            Nom:    nom[0],
            Prenom: nom[1],
        },
    }
    
// Envoi de la requête :
if err := stream.Send(req); err != nil {
    log.Printf("Erreur à l'envoi des données : %v", err)
    break
    // on peut abréger ce code avec log.Panicf()
}
```

Nous simulons une latence réseau en ajoutant un ```time.Sleep()``` à 100 millisecondes.

Et une fois que l'envoi a été effectué, nous allons cloturer le flux et attendre une réponse que l'on traitera à l'aide de la méthode ```CloseAndRecv()```.

Cette méthode retourne deux valeurs :

- un objet de type ```*hellopb.HelloReponse```
- une erreur

L'objet ```*hellopb.HelloReponse``` sera traité avec sa méthode ```GetReponse()``` qui retourne la chaîne stockée dans le champ ```Reponse```.

**Code complet** :

```go
package main

import (
    "api-stream-client/hellopb"
    "context"
    "fmt"
    "log"
    "time"

    "google.golang.org/grpc"
)

func main() {
    fmt.Println("Client - API stream Client")

    cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
    if err != nil {
        log.Fatalf("Erreur à la création de la connexion : %v", err)
    }
    defer cnx.Close()

    client := hellopb.NewHelloServiceClient(cnx)

    stream, err := client.ServiceStreamClient(context.Background())
    if err != nil {
        log.Panicf("Erreur à la création du client : %v", err)
    }

    noms := [][]string{
        {"Ada", "Lovelace"},
        {"Alan", "Turing"},
        {"Brian", "Kernighan"},
        {"Dennis", "Ritchie"},
        {"Ken", "Thompson"},
        {"Rob", "Pike"},
        {"Robert", "Griesemer"},
    }

    for i, nom := range noms {
        req := &hellopb.HelloRequete{
            Message: &hellopb.HelloMessage{
                Nom:    nom[1],
                Prenom: nom[0],
            },
        }

        // Envoi de la requête
        log.Printf("Envoi de la requête n° %d", i+1)
        if err := stream.Send(req); err != nil {
            log.Printf("Erreur à l'envoi des données : %v", err)
            break
        }

        time.Sleep(100 * time.Millisecond)
    }

    // Fin de boucle et attente de la réponse du serveur
    rep, err := stream.CloseAndRecv()
    if err != nil {
        log.Panicf("Erreur à la réception de la réponse du serveur : %v", err)
    }
    log.Printf("Réponse du serveur : %v", rep.GetReponse())
}
```

[<- Précédent](5-Streaming-server.md) || [Suivant ->](7-Streaming-BiDi.md)
