# Go et gRPC

TABLE DES MATIÈRES

- [1 - Introduction](1-introduction.md)
- [2 - Pré-requis](2-pre-requis.md)
- [3 - Codes boilerplates](3-boilerplate-codes.md)
- [4 - API unaire](4-API-unaire.md)
- [5 - API server streaming](5-Streaming-server.md)
- [6 - API client streaming](6-Streaming-client.md)
- [7 - API streaming bidirectionnel](7-Streaming-BiDi.md)
- [8 - Notions avancées](8-Notions-avancees.md)
- [9 - gRPC et MongoDB](9-CRUD.md)
- [10 - CRUD : Création](10-C_RUD.md)
- [11 - CRUD : Lecture](11-C_R_UD.md)
- [12 - CRUD : Mise à jour](12-CR_U_D.md)
- [13 - CRUD : Suppression](13-CRU_D.md)
- [14 - Autre opération : liste](14-Liste.md)
- [15 - gRPC, MongoDB et Evans](15-Evans.md)
