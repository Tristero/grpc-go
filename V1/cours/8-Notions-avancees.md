
# 8-Notions avancées

## 1-Gestion des erreurs

### 1-Présentation de la gestion des erreurs

Il est commun pour nos APIs de retourner des codes d'erreur. Par exemple, HTTP dispose de ses propres codes d'erreur : on trouve 2XX en cas de succès, 5XX en cas de problème côté serveur ... ces codes sont standards mais manquent de clarté.

Avec gRPC, il y a peu de codes d'erreur : [lien doc officielle](https://grpc.io/docs/guides/error.html) et [document plus exhaustif](http://avi.im/grpc-errors/)

La doc officielle présente des codes pour des erreurs générales (i.e. serveur en arrêt, méthode absente du serveur), des échecs réseau, des erreurs de protocole (i.e. erreur de parsing, erreur dans la compression) mais la documentation n'est pas très organisée ni claire. Au total, on a quelques codes erreur mais ils sont partagés mais simples : par ex., ```GRPC_STATUS_UNKNOWN``` peut signifier une erreur générale (le serveur a levé une exception) ou une erreur de protocole (erreur de parsing).

Mais la documentation manque d'information sur la façon de gérer les erreurs côté serveur et côté client : la solution est apporté par un guide pratique [avi.im](http://avi.im/grpc-errors/)

### 2-TP présentation

Nous allons implémenter une API unaire simple ```RacineCarre```. Nous implémenterons la gestion des erreurs côté serveur ET côté client.

Mise en place du projet :

```sh
mkdir -p ./erreurs/serveur ./erreurs/client ./erreurs/calculpb
touch ./erreurs/serveur/serveur.go ./erreurs/client/client.go ./erreurs/calculpb/calcul.proto
cd ./erreur
go mod init erreurs
 ```

### 3-TP Protocole Buffer pour RacineCarré

Dans ce dernier on crée un fichier ```calcul.proto``` dans lequel on crée nos messages et notre procédure gRPC. Notre requête enverra un nombre et le serveur se chargera de retourner une réponse (nombre décimal).

**ATTENTION** : il est très important de documenter son code. C'est ce que nous allons faire : nous allons documenter l'erreur. Cela permettra aux autres développeurs de connaître le message qui sera retourné :

```proto
syntax = "proto3";

package erreurs;

option go_package=".;erreurspb";

message RacineCarreReq{
    int64 nb = 1;
}

message RacineCarreRep {
    double reponse = 1;
}

service RacineCarreService {
    // gestion des erreurs
    // cette RPC lèvera une exception si le chiffre envoyé est négatif
    // L'erreur retournée est de type INVALID_ARGUMENT
    rpc RacineCarre(RacineCarreReq) returns (RacineCarreRep){}
}
```

Générons le code : ```protoc *.proto --go_out=plugins=grpc:.```

### 4-Serveur et gestion des erreurs

Pour notre serveur, nous allons créer (si cela n'a pas été encore fait avec les exercices) le coeur du serveur :

```go
package main

import (
    "erreurs/calculpb"
    "fmt"
    "log"
    "net"

    "google.golang.org/grpc"
)

type service struct{}

func main() {
    fmt.Println("Serveur de calcul de racine carrée - Gestion des erreurs")

    l, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur dans la création du Listener : %v", err)
    }
    defer l.Close()

    s := grpc.NewServer()

    calculpb.RegisterRacineCarreServiceServer(s, &service{})

    if err := s.Serve(l); err != nil {
        log.Panicf("Erreur au lancement du serveur : %v", err)
    }
}
```

Nous allons créer une fonction-helper pour gérer notre nouveau service côté serveur. Pour cela on édite ```calcul.pb.go``` et on observe l'interface ```RacineCarreServiceServer``` :

```go
// RacineCarreServiceServer is the server API for RacineCarreService service.
type RacineCarreServiceServer interface {
    // gestion des erreurs
    // cette RPC lèvera une exception si le chiffre envoyé est négatif
    // L'erreur retournée est de type INVALID_ARGUMENT
    RacineCarre(context.Context, *RacineCarreReq) (*RacineCarreRep, error)
}
```

Notre commentaire inclu dans le fichier ```*.proto``` a été intégré dans le fichier Go.

```go
func (*service) RacineCarre(ctx context.Context, req *calculpb.RacineCarreReq) (*calculpb.RacineCarreRep, error) {
    return nil, nil
}
```

Nous allons récupérer notre nombre via le pointeur ```*calculpb.RacineCarreReq``` et la méthode ```GetNb()```. On utilisera une condition ```if``` simple pour calculer ou retourner une erreur.

Si le nombre est inférieur à 0, on retourne un nil comme réponse et une erreur. Cette dernière se traite très simplement avec ```status.Errorf()``` (du package ```google.golang.org/grpc/status```). Cette méthode nécessite deux arguments : un code erreur (une énumération depuis ```codes``` du package ```google.golang.org/grpc/codes```) et une string. Nous allons utiliser ```codes.InvalidArgument``` en premier argument et un message simple.

En cas de succès, on retourne le calcul de la racine carrée et un nil

#### Code de la méthode

```go
func (*service) RacineCarre(ctx context.Context, req *calculpb.RacineCarreReq) (*calculpb.RacineCarreRep, error) {
    if req.GetNb() < 0 {
        return nil, status.Errorf(
            codes.InvalidArgument,
            "Le nombre reçu est négatif alors qu'il ne peut être que positif : %v", req.Nb,
        )
    }
    return &calculpb.RacineCarreRep{
        Reponse: math.Sqrt(float64(req.GetNb())),
    }, nil
}
```

### 5-Client et les erreurs

Implémentons un client :

```go
package main

import (
    "context"
    "erreurs/calculpb"
    "fmt"
    "log"

    "google.golang.org/grpc"
)

func main() {
    fmt.Println("Client - calcul de racine carrée / gestion des erreurs")

    cnx, err :=grpc.Dial("localhost:50051", grpc.WithInsecure())
    if err!=nil {
        log.Fatalf("Erreur à la création de la connexion : %v", err)
    }
    defer cnx.Close()

    client := calculpb.NewRacineCarreServiceClient(cnx)
    
    // appel correct

    // appel incorrect
}
```

Pour gérer l'erreur il suffira de la récupérer via le package ```"google.golang.org/grpc/status"``` et la fonction ```FromError()``` à laquelle on passera en argument l'erreur. Cette fonction retourne deux éléments :

- un pointeur ```status.Status```
- un booléen

Le booléen va nous renseigner sur le "type" d'erreur et répond à la question : est-ce une erreur utilisateur (true) ou une erreur plus grave issue du framework (false) ?

Le traitement du pointeur ```status.Status``` nous permet non seulement de récupérer le message d'erreur mais aussi le code. Et en fonction du code erreur on peut "jouer" dessus :

```go
if err != nil {
    if s, ok := status.FromError(err); ok {
        ...
        if s.Code() == codes.InvalidArgument {
            ...
        }
        ...
    }
    ...
}
```

#### Code complet du client

Pour notre code, nous allons créer une fonction helper pour éviter la redondance des tests :

```go
package main

import (
    "context"
    "erreurs/calculpb"
    "fmt"
    "log"

    "google.golang.org/grpc"
    "google.golang.org/grpc/codes"
    "google.golang.org/grpc/status"
)

func main() {
    fmt.Println("Client - calcul de racine carrée / gestion des erreurs")

    cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
    if err != nil {
        log.Fatalf("Erreur à la création de la connexion : %v", err)
    }
    defer cnx.Close()

    client := calculpb.NewRacineCarreServiceClient(cnx)

    var nbcorrect, nbincorrect int64 = 42, -42
    // appel correct
    calculRacineCarree(client, nbcorrect)

    // appel incorrect
    calculRacineCarree(client, nbincorrect)

}

func calculRacineCarree(client calculpb.RacineCarreServiceClient, nb int64) {
    rep, err := client.RacineCarre(context.Background(),
        &calculpb.RacineCarreReq{
            Nb: nb,
        })

    //  Gestion des erreurs
    if err != nil {
        if s, ok := status.FromError(err); ok {
            // erreur utilisateur :
            // Affichage du contenu du pointeur ```status.Status```
            fmt.Printf("Message du serveur : %v\n", s.Message())
            fmt.Println(s.Code())
            // On traite notre erreur particulière InvalidArgument
            if s.Code() == codes.InvalidArgument {
                fmt.Println("Erreur : argument invalide. Le nombre passé en argument est négatif.")
                // on arrête ici
                return
            }
        } else {
            // erreur framework
            log.Fatalf("Erreur importante à l'appel de la fonction RacineCarre : %v\n", err)
            // on arrête ici
            return

        }
    }
    log.Printf("La racine carrée de 42 est %v", rep.GetReponse())
}
```

## 2-Deadlines

### 1-Intro aux deadlines

Il est important de gérer les problématiques de latence. Mais la durée est propre à chacun, mais aussi au type de données échangées : pour des petites "transactions", une faible durée (max. 1 seconde) est largement suffisante. En revanche, pour des données beaucoup plus importantes alors il faudra augmenter la taille de la fenêtre temporelle : jusqu'à plusieurs minutes !

Pour toute information sur les deadlines, consultez le [blog](https://grpc.io/blog/deadlines)

**NB** : Le serveur vérifiera si la deadline a été atteinte. Si celle-ci a été atteinte alors le travail est annulé. Les deadlines sont propagées à travers des appels gRPC chaînés. Cela signifie qu'une chaîne d'appel de procédures entre microservices (on dispose de serveurs gRPC A, B, et C): A => B => C où A appelle le serveur gRPC B qui appelle le serveur gRPC C ; la deadline est transmise de A vers B et de B vers C.

### 2-Présentation du TP

Pour notre mise en pratique, on va implémenter au niveau du serveur un retour après 3 secondes. Et le serveur vérifiera si le client a annulé la requête. Et côté client, nous implémenterons deux deadlines : une de 5 secondes et une de 1 seconde.

Projet est établi comme suit :

```sh
mkdir -p ./deadlines/serveur ./deadlines/client ./deadlines/hellopb
touch ./deadlines/serveur/serveur.go ./deadlines/client/client.go ./deadlines/hellopb/hello.proto
cd ./deadlines
go mod init deadlines
```

### 3-Protocol Buffer

On va utiliser une API unaire simple. On édite ```hello.proto``` et on crée un service RPC qui prendra en argument un objet ```HelloRequete``` et retournera une chaine :

```proto
syntax = "proto3";

package hello;
option go_package=".;hellopb";

message Hello {
    string nom =1;
    string prenom = 2;
}

message HelloRequete {
    Hello requete = 1;
}

message HelloReponse {
    string reponse = 1;
}

service HelloService {
    rpc  HelloDeadlineService (HelloRequete) returns (HelloReponse);
}
```

Une fois le fichier généré, à la racine de notre module Go : il faut taper ```go mod tidy``` pour récupérer les dépendances.

### 4-Deadlines côté client

Il s'agit d'implémenter un client simple dans le cadre d'une API unaire. La différence avec ce que nous avons vu jusqu'ici concerne le contexte : le package context permet de gérer les deadlines. Si on reprend notre code par défaut du client, nous allons effectuer quelques petites modifications simples : on se crée une variable initialisée avec ```context.Background```.

```go
package main

import (
    "context"
    "deadlines/hellopb"
    "fmt"
    "log"

    "google.golang.org/grpc"
)

func main() {
    fmt.Println("Lancement du client testant les deadlines")

    cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
    if err != nil {
        log.Fatalf("Erreur à la création de la connexion : %v", err)
    }
    defer cnx.Close()

    client := hellopb.NewHelloServiceClient(cnx)

    // on se crée une variable pour le context.Background
    ctx := context.Background()
    rep, err := client.HelloDeadlineService(ctx, &hellopb.HelloRequete{
        Requete: &hellopb.Hello{
            Nom:    "Turing",
            Prenom: "Alan",
        },
    })
    if err != nil {
        log.Panicf("erreur à l'envoi de la requête : %v", err)
    }
    fmt.Println(rep.GetReponse())
}
```

C'est avec le package ```context``` que nous allons pouvoir manipuler le temps de latence car il dispose d'une méthode ```withTimeout()``` (et non ```WithDeadline``` !).

Cette méthode demande deux paramètres :

- un contexte parent : ici ce sera le contexte racine, ```context.Background()```
- une durée pour le timeout : nous allons choisir 5 secondes.

Et elle retourne deux éléments :

- un objet de type ```context.Context``` que nous passerons en premier argument à notre client ``````
- une fonction d'annulation que l'on va utiliser avec ```defer``` juste après sa déclaration et initialisation.

```go
...
    ctx, cancel := context.WithTimeout(context.Background(), time.Duration(5*time.Second))
    defer cancel()
...
```

Pour traiter l'erreur retournée par le client gRPC et liée à la deadline, nous allons appeler la fonction ```FromError()``` du package ```google.golang.org/grpc/status``` à laquelle on passera l'erreur en paramètre. Cela nous retournera deux valeurs, un type ```*status.Status``` et un booléen. Si le booléen est vérifié on va se charger de comparer le code-erreur avec ```DeadlineExceeded``` :

```go
    rep, err := client.HelloDeadlineService(ctx, &hellopb.HelloRequete{
        Requete: &hellopb.Hello{
            Nom:    "Turing",
            Prenom: "Alan",
        },
    })

    if err != nil {
        if s, ok := status.FromError(err); ok {
            if s.Code() == codes.DeadlineExceeded {
                log.Println("Timeout ! La deadline a été atteinte ...")
            } else {
                log.Printf("Erreur inattendue : %v", s.Message())
            }
        } else {
            log.Panicf("erreur à l'envoi de la requête : %v", err)
        }
        // on oublie de retourner directement car il n'y a plus de traitement à réaliser
        return
    }
```

#### Code complet du client gérant la deadline

```go
package main

import (
    "context"
    "deadlines/hellopb"
    "fmt"
    "log"
    "time"

    "google.golang.org/grpc"
    "google.golang.org/grpc/codes"
    "google.golang.org/grpc/status"
)

func main() {
    fmt.Println("Lancement du client testant les deadlines")

    cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
    if err != nil {
        log.Fatalf("Erreur à la création de la connexion : %v", err)
    }
    defer cnx.Close()

    client := hellopb.NewHelloServiceClient(cnx)

    // Gestion du Timeout
    ctx, cancel := context.WithTimeout(context.Background(), time.Duration(5*time.Second))
    defer cancel()

    rep, err := client.HelloDeadlineService(ctx, &hellopb.HelloRequete{
        Requete: &hellopb.Hello{
            Nom:    "Turing",
            Prenom: "Alan",
        },
    })
    if err != nil {
        if s, ok := status.FromError(err); ok {
            if s.Code() == codes.DeadlineExceeded {
                log.Println("Timeout ! La deadline a été atteinte ...")
            } else {
                log.Printf("Erreur inattendue : %v", s.Message())
            }
        } else {
            log.Panicf("erreur à l'envoi de la requête : %v", err)
        }
        return
    }
    fmt.Println(rep.GetReponse())
}
```

**Remarque** : on pourra utiliser un flag pour passer un entier pour la durée de la deadline (confère le code  ```deadlines```). Attention il faudra convertir le type ```int``` en type ```int64``` avec ```time.Duration```.

### 5-Deadlines côté serveur

Si on regarde du côté du fichier ```hello.pb.go``` au niveau de l'interface ```HelloServiceServer```, on trouvera une nouvelle méthode à implémenter sur ```serveur.go``` : ```HelloDeadlineService(context.Context, *HelloRequete) (*HelloReponse, error)```.

Nous allons faire une première implémentation d'une API unaire traditionnelle.

Voici notre code de départ :

```go
package main

import (
    "context"
    "deadlines/hellopb"
    "fmt"
    "log"
    "net"

    "google.golang.org/grpc"
)

type service struct{}

func (*service) HelloDeadlineService(ctx context.Context, req *hellopb.HelloRequete) (*hellopb.HelloReponse, error) {
    log.Println("Appel de la méthode HelloDeadlineService")
    reponse := fmt.Sprintf("Hello %s %s", req.GetRequete().GetPrenom(), req.GetRequete().GetNom())
    return &hellopb.HelloReponse{
        Reponse: reponse,
    }, nil
}

func main() {
    fmt.Println("Lancement du serveur - gestion des deadlines")

    l, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur au lancement du listener : %v", err)
    }
    defer l.Close()

    s := grpc.NewServer()
    hellopb.RegisterHelloServiceServer(s, &service{})
    if err := s.Serve(l); err != nil {
        log.Panicf("Erreur au lancement du serveur gRPC : %v", err)
    }
}
```

Nous allons instaurer un temps de latence de 3 secondes : plus précisément 3 x 1 seconde. Et à chaque seconde, nous allons vérifier s'il y a eu une annulation de la part du client. Nous allons créer une boucle for intégrant la fonction ```time.Sleep``` :

```go
func (*service) HelloDeadlineService(ctx context.Context, req *hellopb.HelloRequete) (*hellopb.HelloReponse, error) {
    log.Println("Appel de la méthode HelloDeadlineService")

    for i := 0; i < 3; i++ {
        time.Sleep(1 * time.Second)
    }

    ...
}
```

Pour contrôler l'arrêt, nous allons manipuler encore une fois l'objet ```context.Context``` et sa méthode ```Err()```. Le package ```context``` dispose de deux code-erreurs : ```DeadlineExceeded``` et ```Canceled```.

Dans un premier temps, nous allons comparer l'erreur retournée par le contexte avec la valeur ```DeadlineExceeded``` : cette erreur est envoyée par le client. Nous afficherons un message à la console et nous retournerons une requête ```nil``` et un message d'erreur en appelant ```status.Error()``` qui retournera un type erreur traditionnel. **ATTENTION** ```status.Error()``` demande deux arguments :

- un code de type ```codes.Code```
- un message de type ```string```

```go
func (*service) HelloDeadlineService(ctx context.Context, req *hellopb.HelloRequete) (*hellopb.HelloReponse, error) {
    log.Println("Appel de la méthode HelloDeadlineService")

    for i := 0; i < 3; i++ {
        if ctx.Err() == context.DeadlineExceeded {
            // le client a annulé la requête
            log.Println("Deadline atteinte pour le client : annulation de la requête")
            // on retourne une erreur
            return nil, status.Error(codes.Canceled, "le client a annulé la requête")
        }
        time.Sleep(1 * time.Second)
    }

    reponse := fmt.Sprintf("Hello %s %s", req.GetRequete().GetPrenom(), req.GetRequete().GetNom())
    return &hellopb.HelloReponse{
        Reponse: reponse,
    }, nil
}
```

Si le client coupe sa connexion avant les 3 secondes, le serveur affichera un message d'erreur à la console.

#### Code complet du serveur

```go
package main

import (
    "context"
    "deadlines/hellopb"
    "fmt"
    "log"
    "net"
    "time"

    "google.golang.org/grpc"
    "google.golang.org/grpc/codes"
    "google.golang.org/grpc/status"
)

type service struct{}

func (*service) HelloDeadlineService(ctx context.Context, req *hellopb.HelloRequete) (*hellopb.HelloReponse, error) {
    log.Printf("Appel de la méthode HelloDeadlineService avec %v", req)

    for i := 0; i < 3; i++ {
        if ctx.Err() == context.DeadlineExceeded {
            log.Println(ctx.Err())
            // le client a annulé la requête
            log.Println("Deadline atteinte pour le client : annulation de la requête")
            // on retourne une erreur
            return nil, status.Error(codes.Canceled, "le client a annulé la requête")
        }
        time.Sleep(1 * time.Second)
    }

    reponse := fmt.Sprintf("Hello %s %s", req.GetRequete().GetPrenom(), req.GetRequete().GetNom())

    return &hellopb.HelloReponse{
        Reponse: reponse,
    }, nil
}

func main() {
    fmt.Println("Lancement du serveur - gestion des deadlines")

    l, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur au lancement du listener : %v", err)
    }
    defer l.Close()

    s := grpc.NewServer()
    hellopb.RegisterHelloServiceServer(s, &service{})
    if err := s.Serve(l); err != nil {
        log.Panicf("Erreur au lancement du serveur gRPC : %v", err)
    }
}
```

### 6-Conclusion et résumé

Pour gérer les deadlines/timeouts, il faut gérer les objets ```context.Context``` et les erreurs avec les sous-packages ```status``` et ```codes```.

## 3-SSL

En production, toutes les communications se doivent d'être encryptées : on rappelle que gRPC repose sur HTTP/2 et cette version recommande l'utilisation d'une connexion chiffrée.

Pour recourir à une communication SSL, il nous faudra générer un certificat.

Le protocole SSL permet le chiffrement et le déchiffrement des deux côtés : serveur et client. Mais ce protocole est "supplanté" par TLS (Transport Layer Security).

Il est très important de noter qu'il existe deux manières d'utiliser SSL :

- pour le chiffrement (i.e. du navigateur vers un serveur) : on parle de vérification unidirectionnelle ou *1-way verification*
- pour l'authentification : on parle de vérification bidirectionnelle ou *2-way verification*

Nous n'aborderons que le chiffrement. Pas l'authentification.

Comment mettre en place les certificats ?

Avant toute chose, il nous faut un tiers de confiance appelé autorité de certification (*certificate authority (CA)* - lien [wikipédia](https://fr.wikipedia.org/wiki/Autorit%C3%A9_de_certification))qui peut être public (si on a un serveur public) ou privé.

Le serveur doit générer une clé privée. Cette clé va générer un certificat qui sera transmis à une autorité de certification (*CA*) pour qu'elle le signe et reconnaisse que ce certificat provienne bien de cette société, pour ce serveur en particulier. En retour, la *CA* retourne un certifcat signé. Donc du côté serveur nous avons besoin :

- d'une clé privée (fichier PEM)
- d'un certificat signé par une *CA* (fichier CRT)

Le client lui dispose d'un certificat public racine et lors d'une communication SSL, le serveur transmet son certificat signé. Le client vérifie que le certificat est valide. Une fois la vérification effective, la communication sécurisée peut être lancée.

De notre côté, qu'allons nous mettre en place ?

- la clé privée du serveur
- le certificat du serveur
- une autorité de certificat
- une communication TLS

Jusqu'ici nos communications étaient diffusées en clair : on se rappelle que du côté client on utilisait la fonction ```grpc.WithInsecure()```, et du côté serveur la création du serveur s'effectuait sans option.

Maintenant il nous faudra modifier notre code avec l'apport de *credentials* c'est-à-dire les éléments de sécurisation des communications.

La [doc officielle](https://grpc.io/docs/guides/auth.html#go) dispose d'exemples.

### 1-TP pré-requis

Création du projet :

```sh
mkdir -p ./ssl-grpc/serveur ./ssl-grpc/client ./ssl-grpc/hellopb ./ssl-grpc/certifs
touch ./ssl-grpc/serveur/serveur.go ./ssl-grpc/client/client.go ./ssl-grpc/hellopb/hello.proto ./ssl-grpc/certifs/ssl.cnf
cd ./ssl-grpc
go mod init ssl-grpc
```

### 2-Création des certificats locaux

Dans le répertoire ```./ssl-grpc/certifs```, éditons d'abord ```ssl.cnf``` :

```cnf
[ req ]
default_bits = 4096
distinguished_name = dn
req_extensions = req_ext
prompt = no

[ dn ]
CN = localhost

[ req_ext ]
subjectAltName = @alt_names

[alt_names]
DNS.1 = localhost
```

Puis nous allons créer un script shell (que l'on rendra exécutable) :

```sh
#!/bin/bash

# Résumé
# fichiers privés : ca.key, server.key, server.pem, server.crt
# fichiers "partagés" : ca.crt (requis par le client), server.crt (requis par le CA)

# Pour éviter une erreur, bien vérifier que le fichier cache .rnd existe :
set RANDFILE=$HOME/.rnd

SERVER_CN=localhost

# 1 - génère le CA + certificat de confiance (ca.crt)
openssl genrsa -passout pass:1111 -des3 -out ca.key 4096
openssl req -passin pass:1111 -new -x509 -days 365 -key ca.key -out ca.crt -subj "/CN=${SERVER_CN}"

# 2 - génère la clé privée du serveur (server.key)
openssl genrsa -passout pass:1111 -des3 -out server.key 4096

# 3 - crée un certificat signé depuis le CA (server.csr)
openssl req -passin pass:1111 -new -key server.key -out server.csr -subj "/CN=${SERVER_CN}"

# 4 - signe le certificat avec le CA nouvellement créé (il s'autosigne) - server.crt
openssl x509 -req -passin pass:1111 -days 365 -in server.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out server.crt

# 5 - conversion du certificat serveur en format .pem (server.pem) utilisable par gRPC
openssl pkcs8 -topk8 -nocrypt -passin pass:1111 -in server.key -out server.pem
```

Le code ne fonctionnera que pour le ```localhost```.

**NB** : parfois ```openssl``` peut se plaindre de l'absence d'un fichier caché nommé ```.rnd```. Il suffit de lui indiquer où le trouver (s'il n'existe pas : on peut le créer avec ```touch``` par exemple où on le souhaite mais à ce moment là il faudra modifier le chemin d'accès de la variable ```RANDFILE```)

Ce script génèrera 6 fichiers différents. A l'étape 1, on va générer sans aucun doute le fichier (```ca.key```) le plus important. C'est celui qui, en production, ne devra **jamais** être partagé ! On génèrera un certificat (```ca.crt```) qui sera, lui, à partager.

Dans ces six fichiers, 4 portent le nom "server.xxx" : seul ```server.key``` ne devra **jamais** être partagé ! ```server.csr``` est le fichier qui sera partagé avec l'autorité de certification. Quant à ```server.crt```, c'est le certificat signé par le *CA* : il faudra le conserver sur le serveur. Enfin ```server.pem``` n'est qu'une conversion de format.

### 3-Création du fichier Protocol Buffer

On édite/crée le fichier ```./ssl-grpc/hellopb/hello.proto```.

```proto
syntax = "proto3";

package sslgrpc;
option go_package=".;hellopb";

message Hello {
    string Nom = 1;
    string Prenom = 2;
}

message HelloRequete {
    Hello hello = 1;
}

message HelloReponse {
    string reponse = 1;
}

service HelloSSLService {
    rpc HelloSSL (HelloRequete) returns (HelloReponse);
}
```

Et on génère le code Go avec ```protoc hello.proto --go_out=plugins=grpc:.```

Puis on intégre les dépendances avec ```go mod tidy```

### 4-Mise en place du serveur

Dans ce fichier, on va créer nos *credentials* (justificatifs, références) à partir du sous-package ```credentials``` en utilisant la fonction ```NewServerTLSFromFile()``` qui nécessite deux paramètres :

- 1er : le chemin pointant vers le fichier certificat avec le nom du fichier(string)
- 2nd : le chemin vers la clé privée avec le nom du fichier (string)

et retourne deux éléments :

- un objet ```TransportCredentials```
- une erreur

```go
creds, err := credentials.NewServerTLSFromFile("../certifs/server.crt", "../certifs.server.pem")
```

Dans un premier temps, on va gérer l'erreur tout simplement :

```go
if err != nil {
    log.Fatalf("Erreur sur les accès aux fichiers : %v", sslErr)
    return
}
```

Pour la création du serveur, il s'agit de passer le résultat d'une fonction ```grpc.Creds()``` à laquelle on aura passé nos *credentials*.

Ce qui donne le code complet :

**ATTENTION** au chemin d'accès si l'on a fait un copier-coller !!!

```go
package main

import (
    "context"
    "fmt"
    "log"
    "net"
    "ssl-grpc/hellopb"

    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials"
)

type service struct{}

func (*service) HelloSSL(ctx context.Context, req *hellopb.HelloRequete) (*hellopb.HelloReponse, error) {
    fmt.Printf("La fonction SalutationRequete a été invoquée avec %v\n", req)

    // Extraction des nom et prénom
    p, n := req.GetHello().GetPrenom(), req.GetHello().GetNom()
    // Création de la réponse à retourner
    rep := fmt.Sprintf("Salut %s %s", p, n)
    // Réponse sous forme d'une structure dont on récupère l'adresse
    reponse := &hellopb.HelloReponse{
        Reponse: rep,
    }
    return reponse, nil
}

func main() {
    fmt.Println("Lancement du serveur sécurisé")

    l, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur à la création du listener : %v", err)
    }
    defer l.Close()

    // Création de credentials
    creds, err := credentials.NewServerTLSFromFile("../certifs/server.crt", "../certifs/server.pem")
    if err != nil {
        log.Panicf("Erreur à la création des credentials : %s", err)
    }
    // Ajout des certificats via une option : grpc.Creds
    s := grpc.NewServer(grpc.Creds(creds))

    hellopb.RegisterHelloSSLServiceServer(s, &service{})
    if err := s.Serve(l); err != nil {
        log.Panicf("Erreur au lancement du serveur : %v", err)
    }
}
```

### 5-Mise en place du client

Si on lance le serveur : il ne devrait pas y avoir d'erreurs. En revanche, si on lance le client tel qu'on le code jusqu'à présent : on aura une erreur du fait de l'appel de ```grpc.WithInsecure()```. Cette erreur ressemblera à ```code = Unavailable desc```.

Il nous faudra utiliser la fonction ```NewClientTLSFromFile()``` du package ```credientials```. Cette fonction nécessite deux paramètres :

- 1er un fichier de certificat (issu de l'autorité de certification ou *CA*) : ce sera ```ca.crt```
- 2nd un nom de serveur à redéfinir : nulle nécessité ici donc on laisse une chaîne vide.

Et cette fonction, comme pour le serveur retourne deux éléments :

- un objet de type ```credentials.TransportCredentials```
- et une erreur.

Maintenant nous allons remplacer l'option ```grpc.WithInsecure()``` par l'option ```grpc.WithTransportCredentials()``` à laquelle on passe notre variable ```creds```.

Code complet :

```go
package main

import (
    "context"
    "fmt"
    "log"
    "ssl-grpc/hellopb"

    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials"
)

func main() {
    fmt.Println("Lancement du client sécurisé")

    // Récupération du certificat
    creds, err := credentials.NewClientTLSFromFile("../certifs/ca.crt", "")
    if err != nil {
        log.Fatalf("Echec à la récupération du justificatif : %v", err)
    }

    cnx, err := grpc.Dial("localhost:50051", grpc.WithTransportCredentials(creds))

    if err != nil {
        log.Fatalf("Echec à la connexion : %v", err)
    }
    defer cnx.Close()

    client := hellopb.NewHelloSSLServiceClient(cnx)
    rep, err := client.HelloSSL(context.Background(), &hellopb.HelloRequete{
        Hello: &hellopb.Hello{
            Nom:    "Strange",
            Prenom: "Stephen",
        },
    })
    if err != nil {
        log.Panicf("Erreur à l'envoi de la donnée : %v", err)
    }
    fmt.Printf("Réponse : %v\n", rep.GetReponse())
}
```

### 6-Sécurisation vs non-sécurisation des communications

#### côté serveur

Nous allons juste modifier le code.

```gprc.NewServer()``` requiert en argument un slice d'options de type ```grpc.ServerOption```. Ici nous allons "switcher" entre utilisation du TLS et sa non-utilisation :

```go
package main

import (
    "context"
    "flag"
    "fmt"
    "log"
    "net"
    "ssl-grpc/hellopb"

    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials"
)

var tls bool

func init() {
    flag.BoolVar(&tls, "ssl", false, "active ou non la connexion sécurisée")
}

type service struct{}

func (*service) HelloSSL(ctx context.Context, req *hellopb.HelloRequete) (*hellopb.HelloReponse, error) {
    fmt.Printf("La fonction SalutationRequete a été invoquée avec %v\n", req)

    // Extraction des nom et prénom
    p, n := req.GetHello().GetPrenom(), req.GetHello().GetNom()
    // Création de la réponse à retourner
    rep := fmt.Sprintf("Salut %s %s", p, n)
    // Réponse sous forme d'une structure dont on récupère l'adresse
    reponse := &hellopb.HelloReponse{
        Reponse: rep,
    }
    return reponse, nil
}

func main() {
    flag.Parse()
    if tls {
        fmt.Println("Lancement du serveur sécurisé")
    } else {
        fmt.Println("Lancement du serveur NON-sécurisé")

    }
    l, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur à la création du listener : %v", err)
    }
    defer l.Close()

    // Création des options de connexion
    var opts = []grpc.ServerOption{}

    // Vérification de la connexion sécurisée ou non
    if tls {
        // Création de credentials
        creds, err := credentials.NewServerTLSFromFile("../certifs/server.crt", "../certifs/server.pem")
        if err != nil {
            log.Panicf("Erreur à la création des credentials : %s", err)
        }
        opts = append(opts, grpc.Creds(creds))

    }
    // Ajout des certificats via une option : grpc.Creds
    s := grpc.NewServer(opts...)

    hellopb.RegisterHelloSSLServiceServer(s, &service{})
    if err := s.Serve(l); err != nil {
        log.Panicf("Erreur au lancement du serveur : %v", err)
    }
}
```

#### côté client

On va pouvoir jouer sur le type communication TLS ou pas. On reprend le même principe que pour le serveur mais plus simplement car on va "switcher" entre les fonctions ```WithInsecure()``` et ```WithTransportCredentials()``` :

```go
package main

import (
    "context"
    "flag"
    "fmt"
    "log"
    "ssl-grpc/hellopb"

    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials"
)

var tls bool

func init() {
    flag.BoolVar(&tls, "ssl", false, "accepte une connexion sécurisée")
}

func main() {
    flag.Parse()
    fmt.Println("Lancement du client sécurisé")

    opts := grpc.WithInsecure()
    if tls {
        // si on demande une connexion sécurisée alors l'option doit changer et
        // on doit récupérer le certificat
        creds, err := credentials.NewClientTLSFromFile("../certifs/ca.crt", "")
        if err != nil {
            log.Fatalf("Echec à la récupération du justificatif : %v", err)
        }
        opts = grpc.WithTransportCredentials(creds)
    }

    cnx, err := grpc.Dial("localhost:50051", opts)

    if err != nil {
        log.Fatalf("Echec à la connexion : %v", err)
    }
    defer cnx.Close()

    client := hellopb.NewHelloSSLServiceClient(cnx)
    rep, err := client.HelloSSL(context.Background(), &hellopb.HelloRequete{
        Hello: &hellopb.Hello{
            Nom:    "Strange",
            Prenom: "Stephen",
        },
    })
    if err != nil {
        log.Panicf("Erreur à l'envoi de la donnée : %v", err)
    }
    fmt.Printf("Réponse : %v\n", rep.GetReponse())
}
```

## 4-gRPC et langages

Un client développé dans un langage sera complètement capable de communiquer avec un serveur développé dans un autre langage à la seule condition que le fichier ```.proto``` soit **absolument** identique. Un client Python et un serveur Go seront parfaitement capable de s'échanger les informations de façon cohérente. Pour la génération des APIs, il faudra simplement se référer à la documentation pour le langage ciblé.

## 5 - Réflexion et CLI

Jusqu'ici nous avons développé nos serveurs et clients en connaissant exactement ce qui était échangé grâce au fichier ```proto```. Mais qu'en est-il si l'on ignore ce qu'effectue le serveur ! C'est le cas lorsque l'on se trouve en situation de développement où le fichier ```proto``` n'est pas disponible.

C'est ici qu'intervient la réflexivité ou réflexion.

Il y a deux cas où la réflexion est utile :

- lorsque le serveur expose les "endpoints" disponibles,
- lorsque l'on veut définir des CLIs pour dialoguer avec un serveur **sans** connaître le fichier ```proto``` préliminaire.

### 1-Serveur

Donc il nous faut implémenter la réflexion au niveau serveur : on va créer un service de réflexion. Nous disposons de la documentation et d'exemples sur [Github](https://github.com/grpc/grpc-go/tree/master/reflection)

#### 1-Mise en place du projet

```sh
mkdir -p ./reflexion/serveur ./reflexion/calculpb
touch ./reflexion/serveur/serveur.go ./reflexion/calculpb/calcul.proto
cd ./reflexion
go mod init reflexion
```

Pour le TP, nous allons réutiliser le code des exercices. Nous avons fusionné tous les protocoles buffer :

```proto
syntax = "proto3";

package calculpb;
option go_package=".;calculpb";

// API Stream Bidirectionnel
message MaxRequete {
    int64 nb = 1;
}

message MaxReponse {
    int64 nb = 1;
}

// API Streaming client
message CalcRequete {
    int64 nb = 1;
}

message CalcReponse {
    float reponse = 1;
}

// API Streaming serveur
message PremierRequete {
    int64 nb = 1;
}

message PremierReponse {
    int64 rslt = 1;
}

// API Unaire
message SommeMessage {
    int64 x = 1;
    int64 y = 2;
}

message SommeRequete {
    SommeMessage somme = 1;
}

message SommeReponse {
    int64 reponse = 1;
}

service CalculService {
    rpc FindMax (stream MaxRequete) returns (stream MaxReponse){};

    rpc Moyenne(stream CalcRequete) returns (CalcReponse){}

    rpc CalculPremiers(PremierRequete) returns (stream PremierReponse){};

    rpc CalculSomme(SommeRequete) returns (SommeReponse) {};
}
```

#### 2-Codage du serveur

Dans un premier temps, il faut importer la dépendance suivante : ```"google.golang.org/grpc/reflection"```. Puis il faudra appeler **APRES l'enregistrement de notre serveur et service** (dans notre code : ```calculpb.RegisterCalculServiceServer()```), la fonction ```reflection.Register()``` à laquelle on passe notre serveur gRPC (dans notre code ```s```). Et c'est tout !

```go
package main

import (
    "context"
    "fmt"
    "io"
    "log"
    "net"
    "reflexion/calculpb"
    "time"

    "google.golang.org/grpc"
    "google.golang.org/grpc/reflection"
)

type service struct{}

func (*service) FindMax(stream calculpb.CalculService_FindMaxServer) error {

    var max int64

    for {
        msg, err := stream.Recv()
        if err == io.EOF {
            log.Println("... Fin de réception et de transmission")
            return nil
        }
        if err != nil {
            log.Printf("Erreur à la réception d'une donnée : %v", err)
            return err
        }
        log.Printf("Réception de la valeur : %d", msg.GetNb())
        if msg.GetNb() > max {
            log.Printf("%d est supérieur à la dernière valeur stockée %d (réponse envoyée)", msg.GetNb(), max)
            max = msg.GetNb()

            if err := stream.Send(&calculpb.MaxReponse{
                Nb: max,
            }); err != nil {
                return err
            }
        }
    }

}

func (*service) Moyenne(stream calculpb.CalculService_MoyenneServer) error {
    log.Println("Appel d'un calcul de moyenne")
    var somme int64 = 0

    i := 0
    for {
        req, err := stream.Recv()
        if err == io.EOF {
            break
        }
        if err != nil {
            log.Panicf("Erreur à la réception d'un message  : %v", err)
        }

        somme += req.GetNb()
        i++
    }

    rep := &calculpb.CalcReponse{
        Reponse: float32(somme) / float32(i),
    }

    return stream.SendAndClose(rep)
}

func (*service) CalculPremiers(req *calculpb.PremierRequete, stream calculpb.CalculService_CalculPremiersServer) error {
    log.Println("Début de la transaction")

    nb := req.GetNb()
    var k int64 = 2

    for nb > 1 {
        if (nb % k) == 0 {
            fmt.Println(k)
            reponse := &calculpb.PremierReponse{
                Rslt: k,
            }
            if err := stream.Send(reponse); err != nil {
                return err
            }
            nb = nb / k
        } else {
            k = k + 1
        }

        time.Sleep(50 * time.Millisecond)
    }
    log.Println("Fin de la transaction")
    return nil
}

func (*service) CalculSomme(ctx context.Context, req *calculpb.SommeRequete) (*calculpb.SommeReponse, error) {
    log.Println("Appel de la méthode CalculSomme")

    rslt := req.Somme.X + req.Somme.Y
    log.Printf("Avec \nX=%d\nY=%d\nRésultat calculé attendu par le serveur = %d",
        rslt,
        req.Somme.X,
        req.Somme.Y,
    )

    return &calculpb.SommeReponse{
        Reponse: rslt,
    }, nil
}

func main() {
    fmt.Println("Lancement du serveur intégrant un service de réflexion")

    l, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur à la création du listener : %v", err)
    }
    defer l.Close()

    s := grpc.NewServer()
    calculpb.RegisterCalculServiceServer(s, &service{})

    reflection.Register(s)

    if err := s.Serve(l); err != nil {
        log.Panicf("Erreur au lancement du serveur : %v", err)
    }
}
```

On installer les dépendances avec ```go mod tidy``` et on lancera notre serveur.

### 2-Client evans

Côté client, nous utiliserons un outil nommé ```evans``` disponible sur [Github](https://github.com/ktr0731/evans). C'est un outil CLI assez populaire et utilisant la réflexion.

Il existe plusieurs solutions pour installer l'outil : on peut utiliser ```go get``` ou une des releases disponibles. L'auteur recommande la récupération d'une [release](https://github.com/ktr0731/evans/releases) : on disposera d'un exécutable prêt à l'emploi (puisque codé en Go).

Comment l'utiliser ? très simplement, pour obtenir de l'aide, on tapera ```./evans -h```. Une fois installé sur son système, utilisera la commande en fournissant le bon port via le flag ```-p```, l'option ```--reflection``` ou ```-r```

```bash
./evans -p 50051 -r
```

Si tout s'est bien déroulé (bien évidemment le serveur sera lancé !), on obtient une ligne de commande interactive. La documentation pour l'utilisation de Evans se trouve sur [Github](https://github.com/ktr0731/evans#usage-cli).

Lançons ```show package``` et on obtient quelque chose comme :

```bash
calculpb.CalculService@127.0.0.1:50051> show package
+-------------------------+
|         PACKAGE         |
+-------------------------+
| calculpb                |
| grpc.reflection.v1alpha |
+-------------------------+
```

On observe le prompt ```calculpb.CalculService``` suivi d'un arobase : cela signifie que nous sommes dans le package ```calculpb``` et le service ```CalculService```.

Pour lister les services fournis par notre serveur :

```bash
calculpb.CalculService@127.0.0.1:50051> show service
+---------------+----------------+----------------+----------------+
|    SERVICE    |      RPC       |  REQUEST TYPE  | RESPONSE TYPE  |
+---------------+----------------+----------------+----------------+
| CalculService | FindMax        | MaxRequete     | MaxReponse     |
| CalculService | Moyenne        | CalcRequete    | CalcReponse    |
| CalculService | CalculPremiers | PremierRequete | PremierReponse |
| CalculService | CalculSomme    | SommeRequete   | SommeReponse   |
+---------------+----------------+----------------+----------------+
```

On obtient l'ensemble des services disponibles avec le type de requête et le type de la réponse associée.

Si l'on utilise ```show message```, on obtient la liste de tous les messages supportés par le serveur et ses services.

```bash
calculpb.CalculService@127.0.0.1:50051> show message
+----------------+
|    MESSAGE     |
+----------------+
| CalcReponse    |
| CalcRequete    |
| MaxReponse     |
| MaxRequete     |
| PremierReponse |
| PremierRequete |
| SommeReponse   |
| SommeRequete   |
+----------------+
```

Et on peut demander une description pour un message particulier avec la commande ```desc``` suivi du nom du message :

```bash
calculpb.CalculService@127.0.0.1:50051> desc SommeMessage
+-------+------------+----------+
| FIELD |    TYPE    | REPEATED |
+-------+------------+----------+
| x     | TYPE_INT64 | false    |
| y     | TYPE_INT64 | false    |
+-------+------------+----------+
```

### 3-Navigation

S'il existe diffférents packages, on peut naviguer comme suit :

On récupère l'une des valeurs retournée par ```show package```. Et au prompt on va appliquer le nouveau package. Ici on récupère le package nommé ```grpc.reflection.v1alpha```

```sh
salutation.SalutationService@127.0.0.1:50051> package grpc.reflection.v1alpha
```

Au retour on aura un changement du prompt :

```sh
grpc.reflection.v1alpha@127.0.0.1:50051>
```

Puis on demande de montrer les services avec ```show service``` :

```sh
grpc.reflection.v1alpha@127.0.0.1:50051> show service
+------------------+----------------------+-------------------------+--------------------------+
|     SERVICE      |         RPC          |      REQUEST TYPE       |      RESPONSE TYPE       |
+------------------+----------------------+-------------------------+--------------------------+
| ServerReflection | ServerReflectionInfo | ServerReflectionRequest | ServerReflectionResponse |
+------------------+----------------------+-------------------------+--------------------------+
```

### 4-Appel d'une procédure RPC

Une fois placé dans le bon package (```package calculpb```) et le bon service (```service CalculService```), on va pouvoir faire des appels RPC avec la commande ```call```suivi du nom de la procédure RPC :

```sh
calculpb.CalculService@127.0.0.1:50051> call CalculSomme
```

On va pouvoir directement interagir avec le serveur directement via la ligne de commande et obtenir une réponse.

Voici un exemple avec un RPC ```CalculSomme``` :

```sh
calculpb.CalculService@127.0.0.1:50051> call CalculSomme
somme::x (TYPE_INT64) => 12
somme::y (TYPE_INT64) => 30
{
  "reponse": "42"
}
```

Nous avons défini une API client en mode streaming. Ici, nous avons souhaitons calculer une moyenne de nombre que l'on envoie au serveur : ```call Moyenne```

```sh
calculpb.CalculService@127.0.0.1:50051> call Moyenne
nb (TYPE_FLOAT) => 34
nb (TYPE_FLOAT) => 23
nb (TYPE_FLOAT) => 21
nb (TYPE_FLOAT) => 545
nb (TYPE_FLOAT) => 43
nb (TYPE_FLOAT) =>
```

**Pour arrêter et attendre le résultat : Ctrl+D !**

```sh
{
  "rslt": 133.2
}
```

Evans permet de gérer tous les types d'API proposé par gRPC.

[<- Précédent](7-Streaming-BiDi.md) || [Suivant ->](9-CRUD.md)
