# Evans, gRPC et bases de données

Avec l'utilitaire [Evans](https://github.com/ktr0731/evans), on peut manipuler les données depuis cette interface sans avoir à créer un client spécifique. Mais pour cela, nous devons la "réflexion" pour le service. **Rappel** : on utilise le package ```reflection``` et on appelle la fonction ```Register()``` à laquelle on passe notre serveur gRPC.

Côté serveur on rajoutera la ligne dans la fonction ```main()``` :

```go
...

  s := grpc.NewServer()
  blogpb.RegisterBlogServiceServer(s, &service{})
// Ajout de la réflexion au service
  reflection.Register(s)

...
```

On lancera le serveur.

Puis on lancera le client Evans comme suit : ```evans -r -p 50051``` ; où l'on détermine l'utilisation de la réflexion gRPC et un port :

```txt
> $ evans -r -p 50051

  ______
 |  ____|
 | |__    __   __   __ _   _ __    ___
 |  __|   \ \ / /  / _. | | '_ \  / __|
 | |____   \ V /  | (_| | | | | | \__ \
 |______|   \_/    \__,_| |_| |_| |___/

 more expressive universal gRPC client


blog.BlogService@127.0.0.1:50051> 

```

Maintenant nous pouvons déterminer les packages : ```show packages``` ; et les services ```show services```.

```txt
blog.BlogService@127.0.0.1:50051> show packages
+-------------------------+
|         PACKAGE         |
+-------------------------+
| blog                    |
| grpc.reflection.v1alpha |
+-------------------------+

blog.BlogService@127.0.0.1:50051> show service
+-------------+------------+-------------------+--------------------+
|   SERVICE   |    RPC     |   REQUEST TYPE    |   RESPONSE TYPE    |
+-------------+------------+-------------------+--------------------+
| BlogService | CreateBlog | CreateBlogRequest | CreateBlogResponse |
| BlogService | ReadBlog   | ReadBlogRequest   | ReadBlogResponse   |
| BlogService | UpdateBlog | UpdateBlogRequest | UpdateBlogResponse |
| BlogService | DeleteBlog | DeleteBlogRequest | DeleteBlogResponse |
| BlogService | ListBlog   | ListBlogRequest   | ListBlogResponse   |
+-------------+------------+-------------------+--------------------+

blog.BlogService@127.0.0.1:50051> 
```

Si l'on dispose de plusiseurs packages et que l'on veut utiliser un package en particulier : ```package monNouveauPackage```. Et on switchera sur le bon package. Ici, nous sommes sur le bon package comme indiqué par le prompt.

On pourra utiliser les services très simplement avec la commande ```call``` suivi du nom du service gRPC à appeler, par exemple ```call CreateBlog``` :

```txt
blog.BlogService@127.0.0.1:50051> call CreateBlog
blog::id (TYPE_STRING) => 
blog::author_id (TYPE_STRING) => Bob
blog::title (TYPE_STRING) => Nouvel article de Bob
blog::content (TYPE_STRING) => Nouveau contenu créé par Bob
{
  "blog": {
    "id": "6037678d951b2fbcfb9a7a5f",
    "authorId": "Bob",
    "title": "Nouvel article de Bob",
    "content": "Nouveau contenu créé par Bob"
  }
}
```

**NB** : pour l'id, nous n'avons pas entré d'identifiant unique (c'est le serveur MongoDB qui s'en charge comme nous l'avons déjà vu). Donc on tape "Entrée" pour laisser le champ vide.

**Attention aux guillemets**, elles seront *de facto*, échappées !

Puis on récupérer nos données avec :

```txt
blog.BlogService@127.0.0.1:50051> call ListBlog
```

Tous nos PRC seront gérables depuis Evans même les erreurs :

```txt
blog.BlogService@127.0.0.1:50051> call DeleteBlog
blog_id (TYPE_STRING) => 5e26c571fd0200c9b522c0b7
{
  "blogId": "5e26c571fd0200c9b522c0b7"
}

blog.BlogService@127.0.0.1:50051> call DeleteBlog
blogID (TYPE_STRING) => ieoiezj
command call: rpc error: code = InvalidArgument desc = Document introuvable : encoding/hex: invalid byte: U+0069 'i'
```

L'ensemble du code se trouve dans ```7-blog``` (pas de partie consacrée au client)

[<- Précédent](14-Liste.md) || [Retour au départ ->](0-cours.md)
