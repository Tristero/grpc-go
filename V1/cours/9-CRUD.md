# gRPC - API CRUD avec MongoDB

Objectif :

- Créer une API dialoguant avec une base de données MongoDB (ou toute autre base : ici, ce sera MongoDB)
- Créer un service "blog" : implémentant les APIs CRUD

Le code complet se trouve dans le répertoire [1-blog](../1-blog)

## 1-Mise en place locale de MongoDB (version 1)

### Sous Linux

Sur le site [officiel de MongoDB](http://mongodb.com/) : dans le menu, on sélectionne "Software" et "Community Server". Dans la section "MongDB Community Server", on renseignera sa plate-forme (OS, CPU). Concernant le package : **attention** il faut sélectionner le package TGZ. Puis on clique sur "Download".

Puis une fois le téléchargement achevé : on "détare" le fichier avec ```tar zxvf mongodb.XXXX.tar.gz``` puis on se place dans le nouveau répertoire et si on liste le contenu, on trouvera le répertoire ```bin```. Dans ce répertoire se trouve le serveur ```mongod```.

### Erreurs, corrections et lancement

Il peut y avoir plusieurs erreurs sous Linux.

- Erreur CURL_OPENSSL_3 : il suffit pour cela d'installer la bibliothèque ```libcurl3```
- le serveur se lance mais s'arrête de suite. Il s'agit d'un problème de répertoire absent : ```/data/db```. Pour cela on va créer son propre répertoire local. Dans le répertoire englobant le ```bin``` et les autres fichiers, on va créer un nouveau répertoire avec ```mkdir``` :

```sh
mkdir -p data/db
```

Pour lancer correctement le serveur daemon ```mongod``` :

```sh
mongod --dbpath ./data/db
```

Le serveur est lancé et attend les connexions entrantes.

### Pour aller plus loin

Pour aller plus loin dans la connaissance de MongoDB, il existe "l'université" de MongoDB qui propose des cours en ligne permettant de bien connaître et maîtriser la base de données. Ce sont des MOOCs disponibles gratuitement. Il existe aussi des formules payantes (notamment pour les attestations et autres certifications).

[Lien vers l'université](https://university.mongodb.com/)
Pour les développeurs, un [cursus](https://university.mongodb.com/learning_paths/developer) de 6 cours est même disponible gratuitement.

## 2-MongoDB Atlas (version 2)

Il est possible de se passer de MongoDB localement et d'utiliser le service Cloud : MongoDB Atlas. On disposera d'une version dite "Free" utilisable pour nos expérimentations.

Lien officiel : [MongoDB Atlas](https://www.mongodb.com/cloud/atlas)

Il suffit de s'enregistrer (et contrairement à AWS, pas besoin de renseigner sa carte bancaire). Il faudra réaliser quelques étapes avant de pouvoir utiliser la base.

### 1-Cluster

Si on dispose d'un compte Atlas on se conntera à l'adresse suivante [https://account.mongodb.com/account](https://account.mongodb.com/account)

Il se peut que MongoDB demande la création d'une organisation : peu importe le nom, il s'agira pour nous de tester les services. Une fois l'organisation créée, on lancera un nouveau projet que l'on nommera "CRUD" (ou autre).

Tutoriel pour créer son serveur Atlas : [Youtube - français](https://www.youtube.com/watch?v=BvZA9Yb_FKE)

On se crée son cluster : on va sélectionner le fournisseur ayant une présence en Europe **ET** avec le support **free tier available** (par exemple: Google Cloud et Belgique). On laissera les options matériel et moteur de base de données par défaut. On changera le nom en, par exemple, "ClusterPerso" (peu importe du moment que l'on se souvienne du nom du cluster). Le déploiement met plusieurs minutes.

Une fois le cluster mis en place, on va créer une connexion : pour cela on clique sur le bouton ```Connect``` su cluster. Puis on sélectionne ```Allow Access from Anywhere``` si l'on ne dispose pas d'une IP statique (sinon on ajoutera l'IP courante ou une autre) puis on clique ```Add IP Address```. Ensuite il faut se créer un administrateur : on se donne un nom d'utilisateur et un mot de passe puis on clique sur ```Create Database User```. Puis on se choisit un mode de connexion : ici on utilisera ```Connect your application```. On va choisir le langage Go et la version ```1.4 or later```. Le système nous fournit un code exemple que l'on copiera (on sélection ```include full driver code example```).

Par exemple :

```go
    import "go.mongodb.org/mongo-driver/mongo"

    ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
    defer cancel()
    client, err := mongo.Connect(ctx, options.Client().ApplyURI(
        "mongodb+srv://<nom>:<password>@<XXXXXX>.<XXXXXX>.mongodb.net/<dbname>?retryWrites=true&w=majority",
    ))
    if err != nil { log.Fatal(err) }
```

L'interface Mongo Atlas dispose d'outils divers pour contrôler les accès au cluster très simple. On pourra ajouter des utilisateurs (pour cela dans le menu à gauche : dans la section ```SECURITY```, on cliquera sur ```Database Access```). On pourra gérer les accès réseau, etc.

## 3-Client spécifique pour MongoDB

Ce qui suit n'est valable que pour les moteurs MongoDB **4.2 et moins** !

On va utiliser un client graphique pour manipuler MongoDB : Robo 3T disponible sur [robomongo.org](https://www.robomongo.org/)

On télécharge seulement Robo 3T [sur la page idoine](https://www.robomongo.org/download). Puis il faut cliquer sur ```Download Robo 3T only```. On renseignera le bon OS et les différents champs.

Sous Linux, on détare le fichier : ```tar xvf robo3T.X.XX.X-ABC-D.gz```

Dans le répertoire nouvellement créé, on trouvera le répertoire bin et dans ce répertoire on trouvera le binaire ```robo3t``` que l'on lancera. On acceptera la licence puis il n'est pas nécessaire de remplir les champs : on cliquera directement sur Finish.

Une double-fenêtre s'ouvre : dans la plus petite, on cliquera sur le lien "create". On changera juste le nom de la connexion pour, par exemple, "Localhost". On clique sur le bouton "Test" pour savoir si tout fonctionne. Si tout est OK dans la fenêtre de résultat, on la fermera (on sera de retour sur la fenêtre de création de connexion) et on cliquera sur "Save". Donc si tout est OK, on pourra observer le comportement du serveur de la base de données.

Dans le client Robo3T on cliquera ensuite sur le bouton "Connect".

### Cas de Visual Studio Code

Si Robo 3T n'est pas utilisable, il existe un outil officiel, supportant les dernières versions de MongoDB : c'est l'extension pour Visual Studio Code. Ainsi MongoDB a officiellement déployé une extension intégrable à Visual Studio code : [MongoDB for VS Code](https://marketplace.visualstudio.com/items?itemName=mongodb.mongodb-vscode). Cette extension très simple permet de se connecter à son serveur local ou à Atlas !

Page officielle de MongoDB pour VSCode (ressources, communauté, ...): [https://www.mongodb.com/products/vs-code](https://www.mongodb.com/products/vs-code)

### Se connecter à son cluster Atlas

Pour se connecter, il suffit de cliquer sur le bouton de connexion. Une page s'ouvre dans VSCode et on cliquera sur ```Connect with Connection string```.

Il suffira de remplir correctement l'URI suivante : ```mongodb+srv://<NOM_UTILISATEUR>:<MOT_DE_PASSE>@<URL_DE_LA_BASE>/admin```

Les informations sont données dans le code Go qui a été générée pour nous connecter à MongoDB Atlas.

## 4-Mise en place du projet

Nous allons créer une nouvelle arborescence de fichiers et répertoires :

```sh
mkdir -p ./blog/client ./blog/serveur ./blog/blogpb 
touch ./blog/client/client.go ./blog/serveur/serveur.go ./blog/blogpb/blog.proto
cd ./blog
go mod init blog
```

## 5-Squelette pour Protocol Buffer

Nous allons créer un package "blog" tout simplement. Et nous ajoutons une option pour nommer le package propre à Go.

Concernant les messages, on en crée un premier message: ```Blog``` qui comportera quatre champs très simples (tous des strings)

- id
- author_id
- title
- content

Puis nous ajoutons un service gRPC nommé ```BlogService```. Pour l'instant, il sera vierge de toute procédure.

Code :

```proto
syntax = "proto3";

package blog;
option go_package=".;blogpb";

message Blog {
    string id = 1;
    string author_id = 2;
    string title = 3;
    string content = 4;
}

service BlogService {
    
}
```

On teste la génération du code : ```protoc blog.proto --go_out=plugins=grpc:.```

Puis on installera les dépendances avec ```go mod tidy```

## 4-Codage du serveur

Nous allons reproduire le code de nos précédents serveurs. Pour cette mise en pratique, nous n'allons pas implémenter un serveur sécurisé.

Il s'agit d'un code traditionnel. Mais jusqu'ici l'arrêt du serveur s'effectuer de façon violente sans explicitement fermer le port.

Pour cela nous devons placer un "crochet" (*hook*) dans le code pour clôturer la connexion. Nous utilisons une goroutine anonyme. Dans cette goroutine, on y place l'appel de la fonction ```Serve()``` avec sa gestion d'erreur.

Puis on va créer un channel (de type ```os.Signal```, de longueur 1) qui va attendre jusqu'à ce que l'usager appuie sur Contrôle-C : on va intercepter le signal puis on informe l'OS que le programme doit s'interrompre. On bloquera le programme jusqu'à ce que le channel soit vidé et on fermera le serveur avec la méthode ```Stop```. S'il n'y a pas d'erreurs, alors la méthode Close du Listener sera appelée.

```go
...
package main

import (
    "blog/blogpb"
    "fmt"
    "log"
    "net"
    "os"
    "os/signal"

    "google.golang.org/grpc"
)

type service struct{}

func main() {

    log.SetFlags(log.LstdFlags | log.Lshortfile)

    l, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur à la création du listener")
    }
    defer func() {
        log.Println("Appel du defer : arrêt du Listener")
        l.Close()
    }()

    s := grpc.NewServer()
    blogpb.RegisterBlogServiceServer(s, &service{})

    // Attente du Ctrl+C pour sortir
    ch := make(chan os.Signal, 1)
    signal.Notify(ch, os.Interrupt)

    // Lancement du serveur
    go func() {
        fmt.Println("Lancement du serveur de blog")
        if err := s.Serve(l); err != nil {
            log.Panicf("Erreur du serveur : %v", err)
        }
        <-ch
    }()

    // Blocage jusqu'à la réception du signal d'arrêt
    <-ch

    // Arrêt du serveur
    log.Println("Arrêt du serveur")
    s.Stop()
    log.Println("Fin du programme")
}
```

Quelques remarques :

**Première remarque** : dans le MOOC, on arrête le serveur PUIS le listener. Ceci va lever une erreur indiquant que l'on ne peut pas fermer un listener sur une connexion déjà fermée ! Il convient de laisser Go fermer la connexion du Listener.

**Seconde remarque** : nous utilisons ```log.Flags()```, en cas de crash du programme nous aurons le nom du fichier impliqué et son numéro de ligne.

## 5-Driver de MongoDB

### 1-Installation du driver

MongoDB dispose d'un driver officiel disponible sur [Github](https://github.com/mongodb/mongo-go-driver).

Pour l'installation, la doc officielle emploie  ```go get``` et ```dep``` ; nous n'emploierons que la méthode traditionnelle du ```go get``` (à noter que le système ```dep``` n'est plus maintenu)  :

```sh
go get go.mongodb.org/mongo-driver/mongo
```

Pour MySQL, il existe une bibliothèque open source considérée comme l'une des meilleures implémentation pour Go d'un driver de base de données : [Github](https://github.com/go-sql-driver/mysql). Son installation procède du ```go get``` habituel :

```sh
go get github.com/go-sql-driver/mysql
```

L'utilisation du driver MongoDB est loin d'être aisée : le dépôt Github dispose d'exemples permettant une meilleure appréhension du pilote. Contrairement à une base SQL, ce driver ne dépend pas du package ```database\sql``` de la bibliothèque standard. On se réfèrera à la documentation du driver sur [pkg.go.dev](https://pkg.go.dev/go.mongodb.org/mongo-driver/mongo).

### 2-Codage côté serveur

#### 1-Bases de la connexion

C'est au niveau du serveur que nous allons introduire un dialogue avec la base MongoDB.

Pour cela on va réutiliser le code de la documentation :

```go
package main

import (
    ...
    "go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/options"
    "go.mongodb.org/mongo-driver/mongo/readpref"
    ...
)
func main(){
    ...
    fmt.Println("Serveur blog en cours de lancement...")

    // Création d'un client MongoDB connecté à un serveur MongoDB local
    ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
    client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
    if err != nil {
        log.Fatalf("Erreur sur la connexion avec MongoDB : %v", err)
    }


    defer func() {
        if err = client.Disconnect(ctx); err != nil {
            log.Panicf("Erreur à la déconnexion : %v",err)
        }
    }()

    listener, err := net.Listen("tcp", "0.0.0.0:50051")
    ...

}
```

**Mongo Atlas** : il faudra adapter son code notamment sur l'URI à passer à la méthode ```ApplyURI()```. Reprise du code fourni par Mongo Atlas :

```go
client, err := mongo.Connect(ctx, options.Client().ApplyURI(
  "mongodb+srv://<NOM>:<MOT_DE_PASSE>@<URL>/?retryWrites=true&w=majority",
))
```

On peut vérifier si l'on a une connexion avec le serveur MongoDB grâce à la méthode ```Ping()``` qui retournera une erreur s'il n'y aucune communication :

```go
    // Vérification de la connexion avec la base
    if err := client.Ping(ctx, readpref.Primary()); err != nil {
        log.Panicf("Erreur à l'accès à MongoDB : %v", err)
    } else {
        log.Println("Connexion avec MongoDB réussie")
    }
```

#### 2-Base et collections

Que faire ? MongoDB gère dans toute base, des collections. Ces collections sont des regroupements de documents. Si, avec les bases SQL on dispose de bases, de tables et d'enregistrements, avec Mongo une collection se rapporterait plus à une table où chaque enregistrement serait un document. Un document est une structure de données dans un format nommé BSON (un forme "étendue" de JSON - [doc officielle](http://bsonspec.org/))

Accéder à une collection est très simple : on se connecte à la base et on appelle la collection. Notre base s'appellera "mydb" et notre collection "blog". Avec MongoDB, si la base et/ou la collection n'existe(nt) pas elle(s) est/sont créée(s) pour nous. Ce qui est retourné c'est un pointeur de ```mongo.Collection```. Nous allons faire en sorte que notre variable stockant le pointeur soit global :

```go
var collection *mongo.Collection

...
func main() {
    ...
    collection := client.Database("mydb").Collection("blog")
    ...
}
```

**NB** si la base de données n'existe pas, Mongo l'a créera pour nous. Il n'existe pas de déclaration équivalente à un "CREATE DATABASE".

#### 3-Schémas inexistants

La caractéristique d'une base comme MongoDB est que la notion de définition de schéma est inexistante. Ce qui fait qu'une collection peut disposer de documents complètement différents (champs et types différents d'un document à un autre).

Ici nous allons définir un schéma ```blogItem``` qui sera une structure reposant sur les types propres à MongoDB :

```go
...
type server struct{}

type blogItem struct{}

var collection *mongo.Collection

func main(){
    ...
}

```

On définit plusieurs champs. Le premier est ```ID``` : il est d'un type propre à MongoDB ```primitive.ObjectID```. On fera suivre le type d'un tag : ``` `bson:"_id,omitempty"` ```. Cela permet de relier, de mapper le champ ```_id``` (qui le nom du champ par défaut pour tout identifiant unique d'un document BSON) au champ ```ID```. L'option ```omitempty``` indique que l'on peut l'ignorer si aucune valeur n'a été affectée à ```ID```. Tous les autres champs devront aussi disposer de tags :

```go
type blogItem struct {
    ID       primitive.ObjectID `bson:"_id,omitempty"`
    AuthorID string             `bson:"author_id"`
    Content  string             `bson:"content"`
    Title    string             `bson:"title"`
}
```

#### 4-Code complet du serveur

```go
package main

import (
    "blog/blogpb"
    "context"
    "fmt"
    "log"
    "net"
    "os"
    "os/signal"
    "time"

    "go.mongodb.org/mongo-driver/bson/primitive"
    "go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/options"
    "go.mongodb.org/mongo-driver/mongo/readpref"
    "google.golang.org/grpc"
)

var coll *mongo.Collection

type blogItem struct {
    ID       primitive.ObjectID `bson:"_id,omitempty"`
    AuthorID string             `bson:"author_id"`
    Content  string             `bson:"content"`
    Title    string             `bson:"title"`
}

type service struct{}

func main() {
    fmt.Println("Lancement du serveur")
    // Connexion avec le serveur MongoDB
    // Création d'un contexte avec un timeout
    ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
    defer cancel()

    // Connexion à la base de données
    // Si le serveur est local :
    // client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
    // Si le serveur est déployé via Atlas :
    client, err := mongo.Connect(ctx,
        options.Client().ApplyURI(
            "mongodb+srv://NOM:MOT_DE_PASSE@URL.mongodb.net/?retryWrites=true&w=majority",
        ))
    if err != nil {
        log.Fatalf("Erreur à la création du client : %v", err)
    }

    // Diffère la déconnexion à l'arrêt du programme
    defer func() {
        if err := client.Disconnect(ctx); err != nil {
            log.Panicf("Erreur à la déconnexion : %v", err)
        }
    }()

    // Vérification de la connexion avec la base
    if err := client.Ping(ctx, readpref.Primary()); err != nil {
        // Pas connexion -> Panic
        log.Panicf("Erreur à l'accès à MongoDB : %v", err)
    } else {
        log.Println("Connexion avec MongoDB réussie")
    }

    // On se connecte à la base et à une collection :
    // si elles n'existent pas alors elles sont créées
    coll = client.Database("mydb").Collection("blog")

    // Si le code crash, on souhaite obtenir le nom de fichier et le numéro de la ligne
    // Cela nous permet de connaître où le crash s'est déroulé
    log.SetFlags(log.LstdFlags | log.Lshortfile)

    l, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur à la création du listener")
    }
    defer func() {
        log.Println("Appel du defer : arrêt du Listener")
        l.Close()
    }()

    s := grpc.NewServer()
    blogpb.RegisterBlogServiceServer(s, &service{})

    // Attente du Ctrl+C pour sortir
    ch := make(chan os.Signal, 1)
    signal.Notify(ch, os.Interrupt)

    // Lancement du serveur
    go func() {
        log.Println("Lancement du serveur gRPC")
        if err := s.Serve(l); err != nil {
            log.Panicf("Erreur du serveur : %v", err)
        }
        <-ch
    }()

    // Blocage jusqu'à la réception du signal d'arrêt
    <-ch

    // Arrêt du serveur
    log.Println("Arrêt du serveur gRPC")
    s.Stop()
    log.Println("Fin du programme")
}

```

[<- Précédent](8-Notions-avancees.md) || [Suivant ->](10-C_RUD.md)
