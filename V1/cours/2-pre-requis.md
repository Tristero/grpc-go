# 2-Pré-requis

Avant d'aller plus loin, il nous faut installer deux dépendances pour Go pour gérer gRPC. Soit on les installe globalement, soit on les installera localement dans un module. Ici on installe globalement les outils.

## 1-VisualStudio Code

En utilisant VSCode, on aura tout intérêt à installer les extensions suivantes :

- Go (de la team Go de Google)
- vscode-proto3 (de zxh404) qui permet d'utiliser des raccourcis clavier
- Clang-Format (de xaver) qui permettra de formater le code Protocol Buffer

## 2-Protocol Buffer

Il faut installer d'abord un compilateur protocol-buffer : [doc officielle](https://grpc.io/docs/protoc-installation/)

Il existe sur Github des binaires pour les trois grands OS : Windows, Linux et le truc à la pomme.

Pour Linux : un simple ```apt install -y protobuf-compiler``` mais attention la version n'est pas la plus récente. On se dirigera vers la documentation pour une installation plus récente : plus longue mais pas très compliquée. Il faudra juste faire attention à la plateforme ciblée !

Pour Windows : on récupèrera le binaire, on le dézippera et placera le contenu à la racine du lecteur C:\ dans un répertoire ```proto3``` (par exemple) puis il conviendra d'ajouter au PATH le chemin vers le répertoire bin : ```C:\proto3\bin```

## 3-Dépendances Go pour gRPC

### Sans les modules

On pourra installer les dépendances pour Go : l'une pour les Protocol Buffers et l'autre pour générer le code Go pour gRPC.

- Gestion des Protocol Buffers avec Go : [Dépôt Github](https://github.com/golang/protobuf)

On utilisera la version de Google : ```go get -u github.com/golang/protobuf/protoc-gen-go```

- gRPC-Go : [détôt Github](https://github.com/grpc/grpc-go)

on installe avec la commande : ```go get -u google.golang.org/grpc```

### Avec les modules

Avec l'utilisation des modules : il suffira d'exécuter la commande ```go mod tidy``` après la génération du code Go depuis le code Protoco Buffer [cf infra](#5-génération-automatique-du-code-go).

## 4-Codage du Protocol Buffer

Dans un premier répertoire (par ex. ```premier-code```), on crée un sous-répertoire ```hellopb``` comportant un fichier ```hello.proto```.

On crée le contenu du fichier ```hello.proto``` comme suit :

```proto
syntax = "proto3";

package hello;
option go_package=".;hellopb";

service HelloService{};
```

C'est un code très simple qui va juste nous permettre de vérifier que la génération fonctionne.

### Explication

On définit le nom du package selon le nom de notre dossier tout simplement. Puis on définit ```option go_package=".;hellopb``` qui va avoir un impact sur la génération du code Go. **Version dépréciée** : le chemin d'accès était ```option go_package="hellopb"```. Dorénavant, convient de préciser le chemin complet de l'import du répertoire dans lequel notre fichier "hello.proto" se trouve. Voici le message d'erreur :

```txt
Deprecated use of 'go_package' option without a full import path in "hello.proto", please specify:
        option go_package = ".;hellopb";
A future release of protoc-gen-go will require the import path be specified.
See https://developers.google.com/protocol-buffers/docs/reference/go-generated#package for more information.
```

On se crée un service vide. Nous souhaitons seulement vérifier que le service est bien compilé, que l'on dispose du code généré en Go.

## 5-Génération automatique du code Go

A la racine (```premier-code```), on créera notre module avec ```go mod init premier-code```. Puis on installera les deux dépendances avec ```go mod tidy``` et on va pouvoir lancer le compilateur ```protoc``` :

```sh
protoc ./hellopb/hello.proto --go_out=plugins=grpc:.
```

### Quelques Explications

Le premier argument indique là où doit chercher le compilateur tous les fichiers Proto  puis on indique où l'on veut générer le code Go. Mais on utilise un mot-clé ```plugins``` suivi du nom du plug à employer ```plugins=grpc``` pour indiquer qu'il est nécessaire de recourir aux éléments propres à gRPC qui seront rajoutés au code source généré. Puis on a indiqué où l'on souhaite générer le code Go pour notre service gRPC à l'aide du ":" et du point (pour indiquer le répertoire dans lequel on veut nos codes générés : ici le répertoire courant) :  ```plugins=grpc:.```.

Pour éviter de taper cette ligne de code : on créera un petit fichier ```.sh``` à la racine du projet et où l'on placera notre commande ```protoc ./hellopb/...```. Ne pas oublier : ```chmod +x``` pour le rendre exécutable !

Si tout c'est bien déroulé un nouveau fichier a été créé : ```.../premier-code/hellopb/hello.pb.go```

[<- Précédent](1-introduction.md) || [Suivant ->](3-boilerplate-codes.md)
