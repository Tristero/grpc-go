# Lister tous les documents

Objectif : utiliser un streaming côté serveur pour envoyer tous les documents au client

## 1-Protocol Buffer

Ici aucune difficulté : le service RPC prendra une requête simple et retournera un *flux* de réponses (les documents retournés par la base MongoDB). La requête sera un **message vide** (rien n'empêche de transmettre un filtre de recherche par exemple).

Code :

```proto
// Messages pour récupérer la liste complète des documents stockés dans MongoDB
message ListBlogRequest{}

message ListBlogResponse{
  Blog blog = 1;
}

service BlogService {
  ...
  rpc ListBlog(ListBlogRequest) returns (stream ListBlogResponse);
}
```

On génère notre code source.

## 2-Implémentation du serveur

Le code de ```blog.pb.go``` a été modifié donc le code du serveur n'est plus valide : l'interface ```BlogServiceServer``` a intégré une nouvelle fonction ```ListBlog(*ListBlogRequest, BlogService_ListBlogServer) error``` :

```go
type BlogServiceServer interface {
    CreateBlog(context.Context, *CreateBlogRequest) (*CreateBlogResponse, error)
    ReadBlog(context.Context, *ReadBlogRequest) (*ReadBlogResponse, error)
    UpdateBlog(context.Context, *UpdateBlogRequest) (*UpdateBlogResponse, error)
    DeleteBlog(context.Context, *DeleteBlogRequest) (*DeleteBlogResponse, error)
    ListBlog(*ListBlogRequest, BlogService_ListBlogServer) error
}
```

Il faut garder à l'esprit que l'on implémente un **flux** de données. Comme on peut le voir dans la signature, la méthode/fonction demande deux arguments :

- la requête envoyée par le client
- et le stream de type ```BlogService_ListBlogServer```

... et retourne une erreur.

Voici notre squelette :

```go
func (*service) ListBlog(req *blogpb.ListBlogRequest, stream blogpb.BlogService_ListBlogServer) error {
    return nil
}
```

### 1-Récupération des documents

Nous allons très simplement récupérer tous les documents de la base de données à l'aide de la méthode ```Find()``` appelée depuis notre objet ```coll```. Cette méthode demande deux arguments (*a minima*):

- un contexte
- un filtre : puisque l'on veut tout récupérer, on passera ```nil``` ou un document vide
- des options (que nous n'utiliserons pas) pour affiner la recherche

Et retourne deux éléments :

- un pointeur de curseur : ```*mongo.Cursor```
- une erreur

Code :

```go
func (*server) ListBlog(req *blogpb.ListBlogRequest, stream blogpb.BlogService_ListBlogServer) error {
    fmt.Println("Demande de liste des articles.")
    ctx := context.Background()
    cur, err := collection.Find(
        ctx,
        bson.D{},
    )

    if err != nil {
        return status.Errorf(
            codes.Internal,
            fmt.Sprintf("Erreur avec la demande Find() : %v", err),
        )
    }
}
```

### 2-Gestion du curseur

Puis on va s'occuper de fermer le curseur avec ```defer```. La méthode associée au curseur demande en argument un contexte (on peut utiliser ```context.Background()``` ou un contexte pré-établi et partagé comme nous allons faire) :

```go
    ...
    defer cur.Close(ctx)
    ...
```

Puis on va itérer sur le curseur avec une méthode ```Next()``` (qui demande un argument de type ```context```) et retourne un booléen.

*Remarque* : il existe la méthode ```TryNext()``` qui prend le même argument et retourne aussi un booléen mais il se différencie sur le comportement. ```Next()``` est bloquant alors que ```TryNext()``` ne bloque pas. De plus ```TryNext()``` sera utile pour des curseurs dit "tailables" (qui fonctionne comme la commande Unix ```tail -f```) : la communication n'est pas coupée après le dernier document de la collection. Si un nouveau document arrive alors il sera retourné par le curseur. **Mais** ceci n'est possible que pour les collections ayant été créées avec l'option ```capped``` [doc](https://docs.mongodb.com/manual/core/capped-collections/).

**NB** : l'utilisation du contexte est pertinent pour gérer les problèmes de timeout ou de temps limite/deadline

A chaque itération, il faudra :

- utiliser un pointeur sur une structure vide de type ```*blogItem```. Ce pointeur va stocker un document retourné par le curseur
- appeler sur l'objet ```cur``` la méthode ```Decode()``` qui prendra en paramètre notre pointeur. Cette méthode décode le document reçu et le passe dans la structure pointée.

```Decode()``` retourne une erreur que l'on traitera tout de suite :

```go
    ...

    for cur.Next(context.Background()) {
        data := &blogItem{}
        if err := cur.Decode(data); err != nil {
            return status.Errorf(
                codes.Internal,
                fmt.Sprintf("Erreur de décodage : %v", err),
            )
        }
    }
```

Toujours dans notre boucle, nous pouvons retourner les valeurs dans le flux ```stream```. Or ```stream``` dispose d'une méthode ```Send()```. Cette méthode retourne nos données sous forme de flux de pointeur ```blogpb.ListBlogResponse```. Nous allons mapper les données stockées dans la structure ```b``` (rappel : de type ```blogItem```) avec un pointeur ```&blogpb.Blog{}```.

```Send()``` retourne une erreur que nous gèrerons de façon traditionnelle :

```go
...

    for cur.Next(ctx) {
        b := &blogItem{}
        if err := cur.Decode(b); err != nil {
            return status.Errorf(
                codes.Internal,
                fmt.Sprintf("Erreur de décodage : %v", err),
            )
        }
        // Envoi en flux des données
        if err := stream.Send(&blogpb.ListBlogResponse{
            Blog: &blogpb.Blog{
                Id:       b.ID.Hex(),
                AuthorId: b.AuthorID,
                Title:    b.Title,
                Content:  b.Content,
            },
        }); err != nil {
            return status.Errorf(
                codes.Internal,
                fmt.Sprintf("Erreur dans l'envoi d'un document : %v", err),
            )
        }
    }

...
```

Et nous allons gérer une dernière fois toute erreur sur le curseur. Ce type dispose d'une fonction/méthode ```Err()``` :

```go
    ...
    if err := cur.Err(); err != nil {
        status.Errorf(
            codes.Internal,
            fmt.Sprintf("Erreur sur le curseur (fin) : %v", err),
        )
    }
```

Finalement si tout s'est bien déroulé, on retourne un ```nil```.

### 3-Code complet

**NB** le code comprend celui du cours

```go
func (*service) ListBlog(req *blogpb.ListBlogRequest, stream blogpb.BlogService_ListBlogServer) error {
    log.Println("Demande de récupération de l'ensemble des entrées du blog")

    // 1-Récupération des documents
    ctx := context.Background()
    cur, err := coll.Find(ctx, bson.D{})
    if err != nil {
        return status.Errorf(
            codes.Internal,
            fmt.Sprintf("Erreur avec la demande Find() : %v", err),
        )
    }

    // 2-Gestion du curseur
    defer cur.Close(ctx)

    for cur.Next(ctx) {
        b := &blogItem{}
        if err := cur.Decode(b); err != nil {
            return status.Errorf(
                codes.Internal,
                fmt.Sprintf("Erreur de décodage : %v", err),
            )
        }
        // Envoi en flux des données
        if err := stream.Send(&blogpb.ListBlogResponse{
            Blog: &blogpb.Blog{
                Id:       b.ID.Hex(),
                AuthorId: b.AuthorID,
                Title:    b.Title,
                Content:  b.Content,
            },
        }); err != nil {
            return status.Errorf(
                codes.Internal,
                fmt.Sprintf("Erreur dans l'envoi d'un document : %v", err),
            )
        }
    }
    return nil
}
```

## 3-Implémentation du client

Nous allons devoir traiter la réception du flux de données provenant du serveur. Mais il faut d'abord envoyer une requête vide. Puis on traitera dans une boucle la réception de ces données. Le tout sera "empaqueté" dans une fonction-helper.

### 1-fonction-helper : création et envoi de la requête

```protoc``` nous a généré une méthode associée à notre client : ```ListBlog()```. Cette fonction demande un contexte (```Background()```) et une requête (un pointeur vide de type ```ListBlogRequest```). En retour nous obtenons un flux et une erreur.

```go
func listBlog(client blogpb.BlogServiceClient) {
    flux, err := client.ListBlog(
        context.Background(),
        &blogpb.ListBlogRequest{},
    )
    if err != nil {
        log.Printf("Erreur aucun flux disponible : %v", err)
    }
}
```

### 2-Traitement du flux

Nous utiliserons une boucle **infinie** et nous appellerons la méthode ```Recv()``` depuis notre objet ```flux```. La méthode retournera un résultat et une erreur.

Pour les erreurs, nous **devons** gérer deux types d'erreur :

- lorsqu'il n'y a plus de données : ```io.EOF```. On sort de la boucle.
- l'erreur à la réception des données.

**ATTENTION / RAPPEL** : l'ordre de récupération des erreurs est important ! Donc d'abord on vérifie si cela n'est pas une fin de réception de données puis on vérifie si l'erreur n'est pas ```nil``` (gestion *générique* de l'erreur qui survient)

Puis on affiche le contenu de l'article et nous fermerons la connexion du flux avec ```CloseSend()``` :

```go
func listBlog(client blogpb.BlogServiceClient) {
    log.Println("Demande de tous les documents")

    // Envoi de la requête
    flux, err := client.ListBlog(
        context.Background(),
        &blogpb.ListBlogRequest{},
    )
    if err != nil {
        log.Printf("\nErreur aucun flux disponible : %v", err)
    }

    // Traitement des documents
    for {
        rep, err := flux.Recv()
        if err == io.EOF {
            log.Println("Fin de transmission...")
            break
        }
        if err != nil {
            log.Printf("Erreur à la réception d'un document : %v", err)
            break
        }
        log.Printf("Réception d'un document :\n\t%v", rep.GetBlog())
    }

    // Fermeture du flux
    if err := flux.CloseSend(); err != nil {
        log.Printf("Erreur à la fermeture du flux : %v", err)
    }
}
```

Code complet (client+serveur) se trouve dans le répertoire ```6-blog```.

[<- Précédent](13-CRU_D.md) || [Suivant ->](15-Evans.md)
