# Création d'un document

## 1-Création pour le Protocol Buffer

Dans le fichier ```blog.proto```, on crée un premier service de création de blog très simple :

```proto
service BlogService {
    rpc CreateBlog (CreateBlogRequest) returns (CreateBlogResponse);
}
```

Reste à définir nos messages :```CreateBlogRequest``` et ```CreateBlogResponse```.

```proto
message CreateBlogRequest {
    Blog blog = 1;
}

message CreateBlogResponse {
    Blog blog = 1; // Avec un "id" retourné
}
```

A la requête nous ne définissons pas d'ID, celui-ci sera généré par MongoDB et nous sera retourné en réponse.

### Code Protocol Buffer complet

```proto
syntax = "proto3";

package blog;
option go_package=".;blogpb";

message Blog {
    string id = 1;
    string author_id = 2;
    string title = 3;
    string content = 4;
}

message CreateBlogRequest {
    Blog blog = 1;
}

message CreateBlogResponse {
    Blog blog = 1; // Avec un "id" retourné
}

service BlogService {
    rpc CreateBlog(CreateBlogRequest) returns (CreateBlogResponse);
}
```

Générons le fichier ```go``` associé à ```blog.proto```. Et l'on peut observer les nouvelles structures générées comme la structure ```CreateBlogRequest``` :

```go
type CreateBlogResponse struct {
    Blog                 *Blog    `protobuf:"bytes,1,opt,name=blog,proto3" json:"blog,omitempty"`
    XXX_NoUnkeyedLiteral struct{} `json:"-"`
    XXX_unrecognized     []byte   `json:"-"`
    XXX_sizecache        int32    `json:"-"`
}
```

## 2-Serveur : gestion de la création d'un blog

La génération du nouveau code Go dans ```blog.pb.go``` fait que notre code ```serveur.go``` n'est plus valide : nous devons définir une fonction associée à notre "objet" ```service```. La signature attendue nous est donnée dans le fichier généré par ```protoc``` :

```go
type BlogServiceServer interface {
    CreateBlog(context.Context, *CreateBlogRequest) (*CreateBlogResponse, error)
}
```

Donc dans ```serveur.go```, nous définissons notre fonction comme suit :

```go
func (*server) CreateBlog(ctx context.Context, req *blogpb.CreateBlogRequest) (*blogpb.CreateBlogResponse, error) {}
```

Dans un premier temps nous allons récupérer la requête, et plus particulièrement, l'entrée du blog (envoyée par le client) avec ```GetBlog()```. Puis nous allons associer chaque champ reçu à son correspond dans le type ```blogItem``` à l'aide des fonctions associées au type ```Blog```. A l'exclusion du champ ```ID``` qui sera géré par la base de données (rappel : on a défini le tag ```omitempty```, et c'est à ce niveau qu'il intervient).

```go
func (*server) CreateBlog(ctx context.Context, req *blogpb.CreateBlogRequest) (*blogpb.CreateBlogResponse, error) {
    blog := req.GetBlog()

    data := blogItem{
        AuthorID: blog.GetAuthorId(),
        Content:  blog.GetContent(),
        Title:    blog.GetTitle(),
    }
}
```

```data``` va être transmis à la base de données. Pour cela on va réutiliser notre variable globale de type ```*mongo.Collection```. Avec celle-ci, on va pouvoir appeler la fonction ```InsertOne()```. Si on connaît MongoDB : aucune différence avec l'API Python ou JS ou même le shell Mongo.

```InsertOne()``` requiert au moins deux arguments :

- un type ```context.Context``` : la documentation nous indique que l'on peut utiliser un contexte particulier ou simplement ```Background()``` si l'on ne veut pas en proposer
- un document de type ```interface{}```
- et des options propres à la gestion de MongoDB pour une insertion unique (ici nous ne les utiliserons pas)

Et elle retourne deux éléments :

- un pointeur de type ```mongo.InsertOneResult``` dont l'unique élément utilisable est une variable ```InsertedID``` (de type générique ```interface{}```)
- et une erreur.

Si erreur il y a, nous retourneront une erreur à l'aide de ```status.Errorf``` issu du package ```status``` fourni par Google au sein de sa bibliothèque ```grpc```. Rappel : cette fonction requiert un code d'erreur (nous ferons le choix de ```codes.Internal```) et d'un message sous forme de chaîne de caractères.

Premiers éléments de codage du serveur :

```go
func (*service) CreateBlog(ctx context.Context, req *blogpb.CreateBlogRequest) (*blogpb.CreateBlogResponse, error) {

    blog := req.GetBlog()

    // mappage des champs de la requête avec un objet blogItem nouveau
    data := blogItem{
        AuthorID: blog.GetAuthorId(),
        Content:  blog.GetContent(),
        Title:    blog.GetTitle(),
    }

    // enregistrement de la nouvelle entrée dans la base à l'aide de InsertOne
    rslt, err := coll.InsertOne(ctx, data)
    if err != nil {
        return nil, status.Errorf(
            codes.Internal,
            fmt.Sprintf("Erreur interne : %v", err),
        )
    }

    return nil, nil
}
```

Maintenant nous allons extraire l'ObjectID du résultat retourné : ```rslt``` est un pointeur de type ```*mongo.InsertOneResult```.

Notre variable peut accéder à un champ unique ```InsertedID``` de type ```interface{}``` ce qui va nécessiter d'inférer le type de cette interface. A partir de là, il va falloir caster cette interface : pour cela, on *caste* avec le type ```primitive.ObjectID``` tout simplement :

```go
oid, ok := rslt.InsertedID.(primitive.ObjectID)
```

Le résultat de cette opération nous retourne deux choses :

- ```oid``` : notre ```primitive.ObjectID```
- ```ok``` : un booléen que l'on va manipuler pour gérer les problèmes de casting.

Si ```ok``` est ```false```, alors on retourne un ```nil``` et une erreur créée avec le package ```status``` comme précédemment :

```go
oid, ok := rslt.InsertedID.(primitive.ObjectID)
if !ok {
    return nil, status.Errorf(
        codes.Internal,
        fmt.Sprintf("Erreur de casting sur ObjectID."),
    )
}
```

Enfin nous allons retourner notre réponse ! Comme la réponse est un pointeur de type ```blog.CreateBlogResponse```, il faut créer l'ensemble des champs demandés par le type. Pour cela on va récupérer tous les champs de la variable ```blog``` et les mapper. Sauf pour ```Id``` car notre variable ```oid``` est d'un type spécial : ```primitive.ObjectID``` ! A cet objet sont associés des méthodes particulières dont ```String()``` retournant une chaine formatée (```"ObjectID(...)"```) ou ```Hex``` qui ne retourne *que* l'ID. Donc on se retourne vers cette méthode pour récupérer l'ID :

### Code complet de la fonction ```CreateBlog```

```go
func (*service) CreateBlog(ctx context.Context, req *blogpb.CreateBlogRequest) (*blogpb.CreateBlogResponse, error) {

    blog := req.GetBlog()

    // mappage des champs de la requête avec un objet blogItem nouveau
    data := blogItem{
        AuthorID: blog.GetAuthorId(),
        Content:  blog.GetContent(),
        Title:    blog.GetTitle(),
    }

    // enregistrement de la nouvelle entrée dans la base à l'aide de InsertOne
    log.Println("Demande d'insertion d'une nouvelle entrée pour le blog")
    rslt, err := coll.InsertOne(ctx, data)
    if err != nil {
        return nil, status.Errorf(
            codes.Internal,
            fmt.Sprintf("Erreur interne : %v", err),
        )
    }

    if oid, ok := rslt.InsertedID.(primitive.ObjectID); ok {
        log.Printf("Enregistrement réussi : ID n°%s", oid.Hex())
        return &blogpb.CreateBlogResponse{
            Blog: &blogpb.Blog{
                Id:       oid.Hex(),
                AuthorId: blog.GetAuthorId(),
                Title:    blog.GetTitle(),
                Content:  blog.GetContent(),
            },
        }, nil
    }
    return nil, status.Errorf(
        codes.Internal,
        "Erreur de casting sur l'identifiant unique",
    )

}
```

## 3-Création du client

### 1-Design

Comment allons-nous concevoir notre client : très simplement. Notre client devra gérer une API Unaire avec :

- l'absence de connexion sécurisée
- la création d'un nouveau client via ```NewBlogServiceClient()```
- la création et la transmission d'un "blog" à l'aide de la méthode ```CreateBlog()```. On lui passera deux arguments : un contexte et un objet ```CreateBlogRequest``` (on ne se préoccupera pas de ID puisque c'est MongoDB qui va le générer pour nous)
- récupération de la réponse du serveur : un objet de type ```CreateBlogResponse```.

*NB* : nous créerons une fonction-helper.

### 2-Code source du client

```go
package main

import (
    "blog/blogpb"
    "context"
    "fmt"
    "log"

    "google.golang.org/grpc"
)

func main() {

    fmt.Println("Lancement du client")
    cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
    if err != nil {
        log.Fatalf("Connexion impossible : %v", err)
    }
    defer cnx.Close()

    client := blogpb.NewBlogServiceClient(cnx)

    createBlog(client)
}

// création d'une entrée pour le blog
func createBlog(client blogpb.BlogServiceClient) {
    log.Println("Création d'une nouvelle entrée pour le blog")

    rep, err := client.CreateBlog(
        context.Background(),
        &blogpb.CreateBlogRequest{
            Blog: &blogpb.Blog{
                AuthorId: "Alice",
                Content:  "Contenu du premier article",
                Title:    "Premier article",
            },
        })
    if err != nil {
        log.Panicf("Erreur à l'envoi de la requête : %v", err)
    }

    fmt.Printf("Article enregistré : %v", rep.GetBlog())
}
```

Avec Mongo Atlas on pourra vérifier si les données ont été stockées : on cliquera sur ```collections``` du panneau Cluster. Dans la collection ```blog```, on verra le document enregistré comme suit :

```txt
    _id:ObjectID(602d1308e39d4ac9942badfc)
    author_id:"Alice"
    content:"Contenu du premier article"
    title:"Premier article"
```

L'ensemble des codes sources est placé dans le répertoire ```2-blog```.

[<- Précédent](9-CRUD.md) || [Suivant ->](11-C_R_UD.md)
