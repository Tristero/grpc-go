# Update du blog

## 1-Protocol Buffer

Nous allons créer un service de mise à jour d'une entrée du blog ```UpdateBlog``` avec deux messages associés ```UpdateBlogRequest``` et ```UpdateBlogResponse```.

Le message ```UpdateBlogRequest``` sera constitué d'une structure de type ```Blog``` et le message ```UpdateBlogResponse``` sera constitué de la même façon :

```proto
syntax = "proto3";

package blog;
option go_package=".;blogpb";

message Blog {
    string id = 1;
    string author_id = 2;
    string title = 3;
    string content = 4;
}

// Messages pour la création d'une entrée dans le blog
message CreateBlogRequest {
    Blog blog = 1;
}

message CreateBlogResponse {
    Blog blog = 1; // Avec un "id" retourné
}

// Messages pour la lecture d'une entrée dans le blog
message ReadBlogRequest {
    string blogID = 1;
}

message ReadBlogResponse {
    Blog blog = 1;
}

// Messages pour la mise à jour d'une entrée du blog
message UpdateBlogRequest {
    Blog blog = 1;
}

message UpdateBlogResponse {
    Blog blog = 1;
}

service BlogService {
    rpc CreateBlog(CreateBlogRequest) returns (CreateBlogResponse);
    rpc ReadBlog(ReadBlogRequest) returns (ReadBlogResponse); // Retourne NOT_FOUND si rien n'a été trouvé
    rpc UpdateBlog(UpdateBlogRequest) returns (UpdateBlogResponse);// Retourne NOT_FOUND si rien n'a été trouvé
}
```

On compile notre fichier ```proto``` avec protoc. Rappel : ```protoc blog.proto --go_out=plugins=grpc:.```

## 2-Modification du serveur

En regardant l'interface ```BlogServiceClient``` dans ```blog.pb.go```, on doit implémenter la méthode associée à ```service``` comme suit :

```go
func (*service) UpdateBlog(ctx context.Context, req *blogpb.UpdateBlogRequest) (*blogpb.UpdateBlogResponse, error) {
    return nil, nil
}
```

### 1-récupération de l'ID

Tout d'abord nous allons récupérer l'ID stocké de la requête reçue```req``` et cet ID nous permettra de mettre à jour le document dans MongoDB. Donc on va, dans un premier temps, "extraire" la structure avec ```GetBlog()``` puis récupérer l'ID sous forme d'un ```ObjectID``` avec ```primitive.ObjectIDFromHex()``` :

```go
func (*server) UpdateBlog(ctx context.Context, req *blogpb.UpdateBlogRequest) (*blogpb.UpdateBlogResponse, error) {
    fmt.Println("Demande de mise à jour d'un document")
    blog := req.GetBlog()
    oid, err := primitive.ObjectIDFromHex(blog.GetId())
}
```

Nous gérons en premier l'erreur : elle est identique à ce que nous avons déjà vu jusqu'ici. On retourne ```nil``` et une ```status.Error``` avec un code ```InvalidArgument``` :

```go
...
func (*service) UpdateBlog(ctx context.Context, req *blogpb.UpdateBlogRequest) (*blogpb.UpdateBlogResponse, error) {
    log.Println("Demande de modification d'un document/article")

    blog := req.GetBlog()
    oid, err := primitive.ObjectIDFromHex(blog.GetId())
    if err != nil {
        return nil, status.Errorf(
            codes.InvalidArgument,
            "Le document demandé n'existe pas",
        )
    }

    return nil, nil
}
```

### 2-Mise à jour du document

Pour mettre à jour un document avec MongoDB, nous allons recourir à la méthode ```ReplaceOne()``` qui demande trois éléments :

- un contexte : on utilisera ```Background()```
- un filtre : on filtrera avec l'ID du document à modifier
- un document à renvoyer à la base

Cette méthode retournera deux valeurs:

- un pointeur de type ```*mongo.UpdateResult```
- une erreur

On va créer une variable ```data``` de type ```*blogItem{}```
Pour le filtre on va réutiliser ```oid``` et créer un document à l'aide de ```bson.M``` :

```go
...
    //  Mise à jour de notre structure interne
    data := &blogItem{
        AuthorID: blog.GetAuthorId(),
        Content:  blog.GetContent(),
        Title:    blog.GetTitle(),
    }

    filtre := bson.D{
        primitive.E{
            Key:   "_id",
            Value: oid,
        },
    }

    // Version alternative avec bson.M
    //filter := bson.M{"_id": oid}
```

Rappel : la variable blog est de type ```*blogpb.Blog```.

**NB** : on dispose avec le driver de deux méthodes ```UpdateOne``` et ```ReplaceOne```. Nous choisirons le second parce que nous allons écraser complètement le document d'origine par le nouveau !

La méthode ```ReplaceOne()``` requiert aux moins arguments :

- un contexte
- le filtre (quel document allons-nous choisir ?)
- la donnée (structure avec laquelle on remplacera les données du document stocké dans MongoDB)

Code partiel :

```go
...
    rslt, err := collection.ReplaceOne(
        ctx,
        filtre,
        data,
    )
```

Pour la gestion de l'erreur, on réutilise à nouveau ```status.Error()``` : on utilise le code ```Internal``` et un message simple.

```go
    ...
    if err != nil {
        return nil, status.Errorf(
            codes.Internal,
            fmt.Sprintf("Le document demandé ne peut pas être mis à jour : %v", err),
        )
    }
```

Si tout est OK, on peut gérer le retour ```UpdateResult``` qui est une structure :

```go
type UpdateResult struct {
    MatchedCount  int64       // The number of documents matched by the filter.
    ModifiedCount int64       // The number of documents modified by the operation.
    UpsertedCount int64       // The number of documents upserted by the operation.
    UpsertedID    interface{} // The _id field of the upserted document, or nil if no upsert was done.
}
```

Ici nous ne nous intéresserons pas à cette donnée. Nous retournerons seulement le résultat.

### 3-Code complet de UpdateBlog

```go
func (*service) UpdateBlog(ctx context.Context, req *blogpb.UpdateBlogRequest) (*blogpb.UpdateBlogResponse, error) {
    log.Println("Demande de modification d'un document/article")

    // Récupération de la structure Blog
    blog := req.GetBlog()
    oid, err := primitive.ObjectIDFromHex(blog.GetId())
    if err != nil {
        return nil, status.Errorf(
            codes.InvalidArgument,
            "Le document demandé n'existe pas",
        )
    }

    // Mise à jour de notre structure interne
    data := &blogItem{
        AuthorID: blog.GetAuthorId(),
        Content:  blog.GetContent(),
        Title:    blog.GetTitle(),
    }
    filtre := bson.D{
        primitive.E{
            Key:   "_id",
            Value: oid,
        },
    }
    // Version alternative avec bson.M
    //filter := bson.M{"_id": oid}

    // Mise à jour du document dans la base de données
    _, err = coll.ReplaceOne(
        ctx,
        filtre,
        data,
    )
    if err != nil {
        return nil, status.Errorf(
            codes.Internal,
            fmt.Sprintf("Le document demandé ne peut pas être mis à jour : %v", err),
        )
    }

    // Retour en mappant les champs de data avec le type Blog
    return &blogpb.UpdateBlogResponse{
        Blog: &blogpb.Blog{
            Id:       oid.Hex(),
            AuthorId: data.AuthorID,
            Content:  data.Content,
            Title:    data.Title,
        },
    }, nil
}
```

**NB** : on utilise ```oid.Hex()``` car on récupère l'ObjectID du document à modifier et non ```data.ID.Hex()``` car on aurait en retour une série de zéro (ce qui est logique).

**Remarque** : Dans les fonctions consacrées à la lecture et à la mise à jour, nous avons le même code source pour le retour :

```go
    return &blogpb.UpdateBlogResponse{
        Blog: &blogpb.Blog{
            Id:       oid.Hex(),
            AuthorId: data.AuthorID,
            Content:  data.Content,
            Title:    data.Title,
        },
    }, nil
```

On pourra refactoriser ce code dans une fonction-helper idoine (nous ne le ferons pas ici)qui prendra en paramètre la donnée et un type ```primitive.ObjectID``` et retournera un pointeur de type ```*blogpb.Blog```.

```go
func dataToBlog (data *blogItem, oid primitive.ObjectID) *blogpb.Blog {
    return &blogpb.UpdateBlogResponse{
        Blog: &blogpb.Blog{
            Id:       ,
            AuthorId: data.AuthorID,
            Content:  data.Content,
            Title:    data.Title,
        },
    }, nil
}
```

**NB** : dans le MOOC original, il y a un appel de ```FindOne()``` : cela ne sert à rien puisque MongoDB vérifiera si le document existe avec ```ReplaceOne()```. Donc on a une opération redondante et inutile.

On pourra tester le serveur avec ```go run```.

## 3-Modification du client

Objectif : mettre à jour le blog (via une fonction-helper) en modifiant différents champs.

```go
...

func main(){

    ...

    // Fonctions helpers
    //        Création du blog
    createBlog(client)

    //        Lecture du blog
    readBlog(client)

    //        Mise à jour du blog
    updateBlog(client)
}
```

Dans notre fonction helper ```UpdateBlog```, on crée une nouvelle struct de type pointeur ```blogpb.Blog```. Cette structure va récupérer l'ID du document Mongo à modifier : cet ID est stocké dans notre variable globale ```blogID```:

```go
// Mise à jour d'une entrée
func updateBlog(client blogpb.BlogServiceClient) {
    log.Println("Mise à jour d'une entrée du blog")

    // Mise à jour de l'entrée
    updtBlog := &blogpb.Blog{
        Id:       blogID,
        AuthorId: "Nouvel auteur",
        Title:    "Titre modifié",
        Content:  "Nouveau contenu. Ou plutôt contenu modifié",
    }
}
```

Avec notre objet ```client```, on appelle la méthode ```UpdateBlog()``` qui prend (au moins) deux arguments :

- un contexte
- et un pointeur ```blogpb.UpdateBlogRequest```
- en option : des appels de grpc.CallOption (que nous n'utiliserons pas)

... et retourne deux valeurs :

- une réponse de type ```blogpb.UpdateBlogResponse```
- et une erreur

Code complet de la fonction ```updateBlog()```

```go
// Mise à jour d'une entrée
func updateBlog(client blogpb.BlogServiceClient) {
    log.Println("Mise à jour d'une entrée du blog")

    // Mise à jour de l'entrée
    updtBlog := &blogpb.Blog{
        Id:       blogID,
        AuthorId: "Nouvel auteur",
        Title:    "Titre modifié",
        Content:  "Nouveau contenu. Ou plutôt contenu modifié",
    }

    rep, err := client.UpdateBlog(
        context.Background(),
        &blogpb.UpdateBlogRequest{
            Blog: updtBlog,
        })
    if err != nil {
        log.Printf("Erreur sur la mise à jour de l'entrée : %v", err)
    }

    log.Printf("Mise à jour de l'entrée : %v", rep.GetBlog())
}

```

Le code complet se trouve dans le répertoire ```4-blog```.

[<- Précédent](11-C_R_UD.md) || [Suivant ->](13-CRU_D.md)
