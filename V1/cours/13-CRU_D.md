# Suppression d'un article

## 1-Protocol Buffer

On va créer un service RPC et des messages pour gérer la suppression d'un article de blog.

Le client se chargera d'envoyer un simple ID de type ```string``` et le serveur fera de même.

Code source complet :

```proto
syntax = "proto3";

package blog;
option go_package=".;blogpb";

message Blog {
    string id = 1;
    string author_id = 2;
    string title = 3;
    string content = 4;
}

// Messages pour la création d'une entrée dans le blog
message CreateBlogRequest {
    Blog blog = 1;
}

message CreateBlogResponse {
    Blog blog = 1; // Avec un "id" retourné
}

// Messages pour la lecture d'une entrée dans le blog
message ReadBlogRequest {
    string blogID = 1;
}

message ReadBlogResponse {
    Blog blog = 1;
}

// Messages pour la mise à jour d'une entrée du blog
message UpdateBlogRequest {
    Blog blog = 1;
}

message UpdateBlogResponse {
    Blog blog = 1;
}

// Messages pour la suppression d'une entrée du blog
message DeleteBlogRequest{
    string blogID = 1;
}

message DeleteBlogResponse {
    string blogID = 1;
}

service BlogService {
    rpc CreateBlog(CreateBlogRequest) returns (CreateBlogResponse);
    rpc ReadBlog(ReadBlogRequest) returns (ReadBlogResponse); // Retourne NOT_FOUND si rien n'a été trouvé
    rpc UpdateBlog(UpdateBlogRequest) returns (UpdateBlogResponse);// Retourne NOT_FOUND si rien n'a été trouvé
    rpc DeleteBlog(DeleteBlogRequest) returns (DeleteBlogResponse); // Retourne NOT_FOUND si rien n'a été trouvé
}
```

puis on génère notre code Go.

## 2-Modification du serveur

### 1-Implémentation de DeleteBlog

Si on regarde ```blog.pb.go```, l'interface ```BlogServiceServer``` nécessite l'implémentation d'une fonction associée ```DeleteBlog(context.Context, *DeleteBlogRequest) (*DeleteBlogResponse, error)``` :

```go
type BlogServiceServer interface {
    CreateBlog(context.Context, *CreateBlogRequest) (*CreateBlogResponse, error)
    ReadBlog(context.Context, *ReadBlogRequest) (*ReadBlogResponse, error)
    UpdateBlog(context.Context, *UpdateBlogRequest) (*UpdateBlogResponse, error)
    DeleteBlog(context.Context, *DeleteBlogRequest) (*DeleteBlogResponse, error)
}
```

Donc dans ```serveur.go```, on implémente avec la signature suivante :

```go
func (*service) DeleteBlog(ctx context.Context, req *blogpb.DeleteBlogRequest) (*blogpb.DeleteBlogResponse, error) {
    return nil, nil
}
```

Objectifs : il nous faut

- récupérer l'ID envoyé par le client
- supprimer le document dans la base correspondant à l'Id

### 2-Récupération de l'ID

On utilise l'objet requête ```req``` et on appelle la méthode ```GetBlogId()```. Puis on appelle la fonction ```primitive.ObjectIDFromHex()``` pour créer un type ```ObjectID```. ```GetBlogId()``` retourne deux choses :

- non seulement un type ```primitive.ObjectID```
- mais aussi une erreur que nous allons gérer comme précédemment avec le package ```status```.

Code partiel :

```go
func (*service) DeleteBlog(ctx context.Context, req *blogpb.DeleteBlogRequest) (*blogpb.DeleteBlogResponse, error) {
    log.Println("Demande de suppression d'une entrée dans le blog")
    // 1-récupération de l'ID envoyé par le client
    id := req.GetBlogID()
    // 2-transformation en un objet reconnaissable par MongoDB
    oid, err := primitive.ObjectIDFromHex(id)
    if err != nil {
        return nil, status.Errorf(
            codes.InvalidArgument,
            fmt.Sprintf("Document introuvable : %v", err),
        )
    }
    return nil, nil
}
```

### 3-Suppression d'un document

Pour supprimer un document, MongoDB propose la méthode ```DeleteOne()``` qui nécessite (au moins) deux arguments :

- un contexte (qui requiert ```Background()```)
- un filtre : ici on utilisera la même procédure que précédemment. A savoir, le package ```bson``` en appelant la *primitive* ```D``` (ou ```M```, c'est au choix du développeur)

Cette méthode retourne deux éléments :

- un pointeur de type ```mongo.DeleteResult```
- et une erreur

Et on traitera en premier l'erreur :

```go
func (*service) DeleteBlog(ctx context.Context, req *blogpb.DeleteBlogRequest) (*blogpb.DeleteBlogResponse, error) {
    log.Println("Demande de suppression d'une entrée dans le blog")

    // 1-récupération de l'ID envoyé par le client
    id := req.GetBlogID()

    // 2-transformation en un objet reconnaissable par MongoDB
    oid, err := primitive.ObjectIDFromHex(id)
    if err != nil {
        return nil, status.Errorf(
            codes.InvalidArgument,
            fmt.Sprintf("Document introuvable : %v", err),
        )
    }

    // 3-suppression
    rslt, err := coll.DeleteOne(
        ctx,
        bson.D{primitive.E{
            Key:   "_id",
            Value: oid,
        }},
    )
    if err != nil {
        return nil, status.Errorf(
            codes.Internal,
            fmt.Sprintf("Erreur inattendue est survenue au moment de supprimer le document : %v", err),
        )
    }
    return nil, nil
}
```

L'objet stocké dans ```rslt``` est un pointeur de type  ```mongo.DeleteResult```. Ce dernier permet d'accéder à une valeur entière qui représente le nombre de suppression réalisée : nous savons qu'il n'y aura qu'une seule suppression. Donc si le nombre est égal à 0, alors on retourne une erreur !

```go
...
    if rslt.DeletedCount < 1 {
        return nil, status.Errorf(
            codes.NotFound,
            fmt.Sprintf("Erreur - le document n'a pas été supprimé : %v", err),
        )
    }
...
```

Et enfin, nous retournons un résultat : on a deux possibilités. Soit on utilise ```req.GetBlogId()``` soit ```oid.Hex()```. Si l'on choisit la seconde solution il ne faut **PAS** utiliser le code comme suit :

```go
    return &blogpb.DeleteBlogResponse{
        BlogId: oid.Hex(),
    }, nil
```

car à l'arrêt du serveur on fera face à une erreur !

**MAIS** il faut d'abord extraire la chaîne depuis ```oid``` et ensuite le passer à ```BlogId``` :

```go
    id := oid.Hex()
    return &blogpb.DeleteBlogResponse{
        BlogId: id,
    }, nil
```

(nous avons fait le choix du numéro un... cela finalement importe peu)

#### 4-Code complet

```go
func (*service) DeleteBlog(ctx context.Context, req *blogpb.DeleteBlogRequest) (*blogpb.DeleteBlogResponse, error) {
    log.Println("Demande de suppression d'une entrée dans le blog")

    // 1-récupération de l'ID envoyé par le client
    id := req.GetBlogID()

    // 2-transformation en un objet reconnaissable par MongoDB
    oid, err := primitive.ObjectIDFromHex(id)
    if err != nil {
        return nil, status.Errorf(
            codes.InvalidArgument,
            fmt.Sprintf("Document introuvable : %v", err),
        )
    }

    // 3-suppression
    rslt, err := coll.DeleteOne(
        ctx,
        bson.D{primitive.E{
            Key:   "_id",
            Value: oid,
        }},
    )
    if err != nil {
        return nil, status.Errorf(
            codes.Internal,
            fmt.Sprintf("Erreur inattendue est survenue au moment de supprimer le document : %v", err),
        )
    }
    // 4-Erreur si le nombre de suppression est inférieur strict à 1
    if rslt.DeletedCount < 1 {
        return nil, status.Errorf(
            codes.NotFound,
            fmt.Sprintf("Erreur - le document n'a pas été supprimé : %v", err),
        )
    }

    // 5-Si tout s'est bien déroulé on retourne l'ID envoyé par le client
    return &blogpb.DeleteBlogResponse{
        BlogID: req.GetBlogID(),
    }, nil
}
```

## 3-Implémentation du client

Objectif : transmettre un ID au serveur et recevoir le même ID si tout est OK.

Nous créerons une fonction-helper pour plus de facilité de maintenance et de lisibilité ```deleteBlog()``` :

```go
...

func main(){

    ...

    // Fonctions helpers
    //        Création du blog
    createBlog(client)

    //        Lecture du blog
    readBlog(client)

    //        Mise à jour du blog
    updateBlog(client)

    //        Suppression d'une entrée du blog
    deleteBlog(client)
}
```

Pour envoyer un ID, nous allons utiliser l'ID stocké dans la variable globale ```blogId```. Puis la passer à la méthode ```DeleteBlog()``` de l'objet ```blogpb.BlogServiceClient```. Cette méthode associée au client prend au moins deux arguments :

- un contexte (qui sera ```Background()```)
- et un pointeur de type ```DeleteBlogRequest{}``` dans lequel on passera notre variable globale ```blogId```.

Elle nous retournera deux valeurs :

- une réponse de type ```DeleteBlogResponse```
- et une erreur

Nous traiterons d'abord l'erreur puis le résultat.

### Code complet

```go
func deleteBlog(client blogpb.BlogServiceClient) {
    log.Println("Demande de suppression d'un article")

    // 1-envoi de la demande de suppression
    rep, err := client.DeleteBlog(
        context.Background(),
        &blogpb.DeleteBlogRequest{
            BlogID: blogID,
        },
    )
    // 2-gestion des erreurs
    if err != nil {
        log.Printf("Erreur est survenue au moment de la suppression : %v", err)
    }

    // 3-si tout va bien on affiche le résultat
    log.Printf("Document %s a été supprimé avec succès", rep.GetBlogID())
}
```

Le code source complet se trouve dans le répertoire ```5-blog```

[<- Précédent](12-CR_U_D.md) || [Suivant ->](14-Liste.md)
