# 5-Streaming Server

Ceci est une nouveauté supportée uniquement avec HTTP/2 : le client envoie une seule requête et le serveur renvoie un flux de message.

Quels usages ? pour expédier des données très lourdes, on pourra expédier depuis le serveur des paquets plus petits, fragmentés ; pour des actions de type "push" où le client n'a pas à effectuer de requêtes supplémentaires comme pour du chat, des live feeds, ...

Il s'agit d'un <u>nouveau paradigme</u>.

Avec le Protocol Buffer, **tout appel de flux nécessitera l'emploi du mot-clé ```stream```**

## 1-Implémentation du serveur

### 1-Mise en place du projet

On crée l'arborescence d'un projet nommé ```api-stream-serveur```, avec les différents fichiers. On se crée un module Go et on installe les dépendances :

```sh
mkdir -p ./api-stream-serveur/hellopb ./api-stream-serveur/serveur ./api-stream-serveur/client
touch ./api-stream-serveur/hellopb/hello.proto ./api-stream-serveur/serveur/serveur.go ./api-stream-serveur/client/client.go
cd ./api-stream-serveur
go mod init api-stream-serveur
```

### 2-Mise en place avec le Protocol Buffer

Nous allons définir une API *streaming server* qui prend en entrée une requête unique et retournera plusieurs fois une chaîne de caractères.

On édite le fichier ```hello.proto``` et on ajoute dans le ```service``` une nouvelle définition ```rpc``` qui devra retourner un type préfixé avec ```stream```:

```proto
// Définition du service
service HelloService {
    rpc HelloStreamServeur(HelloRequete) returns (stream HelloReponse){};
}
```

L'écriture de la requête est basique : seul l'ajout de ```stream``` marque la différence avec une API unaire.

Code du fichier ```hello.proto``` :

```proto
syntax = "proto3";

package apiStreamServer;
option go_package=".;hellopb";

message HelloMessage {
    string prenom = 1;
    string nom = 2;
}

message HelloRequete {
    HelloMessage message = 1;
}


message HelloReponse {
    string reponse = 1;
}

// Définition du service
service HelloService {
    rpc HelloStreamServeur(HelloRequete) returns (stream HelloReponse){};
}
```

Générons le code avec notre commande shell (dans le répertoire ```hellopb```) : ```protoc *.proto --go_out=plugins=grpc:.```. Et si tout s'est déroulé correctement, le code s'est alourdi de nouvelles méthodes, fonctions et variables. Puis on appelle les commandes ```go mod tidy``` et (c'est optionnel : ) ```go mod vendor```

## 2-Codage du serveur

### 1-Rappel des étapes de conception du serveur

- Création d'un type struct vide pour le serveur associé au service
- Création d'un serveur sur le port avec ```net.Listen()``` (deux arguments en paramètre: une chaîne indiquant le protocole et une chaîne précisant l'adresse et le port; deux types de retour : une instance de type ```net.Listener``` et une erreur)
- Gestion de l'erreur
- Différer la fermeture du ```net.Listener```
- Création d'une nouvelle instance de serveur gRPC avec ```grpc.NewServer()```
- Avec la bibliothèque générée ```hellopb```(in ```hello.pb.go```), enregistrement du serveur gRPC et du service à associer
- Lancement du serveur avec ```grpc.Serve()```

Dans ```hello.pb.pro```, ```protoc``` nous a défini l'interface pour la gestion des données côté serveur en rajoutant notre nouvelle "fonction" rpc :

```go
// HelloServiceServer is the server API for HelloService service.
type HelloServiceServer interface {
    HelloStreamServeur(*HelloRequete, HelloService_HelloStreamServeurServer) error
}
```

### 2-Implémentation de la réponse du serveur

Il nous faut définir et implémenter la méthode ```HelloStreamServeur()```. Donc on crée une nouvelle méthode dans ```serveur.go``` pour notre structure ```serveur``` en faisant attention à bien réécrire (pointer) la bonne bibliothèque (ici notre alias est ```pb```)

```go
func (*srvService) HelloStreamServeur(req *hellopb.HelloRequete, stream hellopb.HelloService_HelloStreamServeurServer) error {
    return nil
}
```

Le premier argument est la requête envoyée par le client. Le second est la réponse, à savoir, le flux à retourner.

Dans un premier temps nous allons extraire les nom et prénom.

```go
nom, prenom := req.GetMessage().GetNom(), req.GetMessage().GetPrenom()
// version alternative
// nom, prenom := req.Message.Nom, req.Message.Prenom
```

Puis nous allons créer une boucle pour expédier les messages de réponse. Dans cette boucle nous allons récupérer notre objet réponse définie comme étant ```hellopb.HelloReponse``` : ici on va créer une référence. Cette structure nécessite que nous définissiions une chaîne dans le champ ```Reponse```. Notre réponse retournera non seulement le prénom et le nom mais aussi le nombre d'itération de la boucle :

```go
for i := 0; i < 10; i++ {
    rep := &hellopb.HelloReponse{
        Reponse: fmt.Sprintf("[Msg %d]Hello, %s %s", i, prenom, nom),
    }
    stream.Send(rep)
}
```

Toujours dans la boucle, à chaque itération, nous passerons au flux la réponse via la méthode ```Send()``` qui prend un message un pointeur de type structure ```hellopb.HelloReponse``` : autrement dit notre réponse. Cette méthode retourne une erreur que nous gèrerons comme suit :

```go
    for i := 0; i < 10; i++ {
        rep := &hellopb.HelloReponse{
            Reponse: fmt.Sprintf("[Msg %d]Hello, %s %s", i, prenom, nom),
        }

        if err := stream.Send(rep); err != nil {
            return err
        }
    }
```

Pour simuler une connexion lente on va mettre en pause le programme avec ```time.Sleep``` toutes les secondes.

### 3-Code du serveur

```go
package main

import (
    "api-stream-serveur/hellopb"
    "fmt"
    "log"
    "net"
    "time"

    "google.golang.org/grpc"
)

type srvService struct{}

func (*srvService) HelloStreamServeur(req *hellopb.HelloRequete, stream hellopb.HelloService_HelloStreamServeurServer) error {
    log.Println("Appel de la méthode HelloStreamServer")

    nom, prenom := req.GetMessage().GetNom(), req.GetMessage().GetPrenom()
    // version alternative
    // nom, prenom := req.Message.Nom, req.Message.Prenom

    for i := 0; i < 10; i++ {
        rep := &hellopb.HelloReponse{
            Reponse: fmt.Sprintf("[Msg %d]Hello, %s %s", i, prenom, nom),
        }

        if err := stream.Send(rep); err != nil {
            return err
        }

        time.Sleep(1 * time.Second)
    }
    return nil
}

func main() {
    fmt.Println("Lancement du serveur - API stream server")

    lst, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Impossible de mettre à la disposition le port 50051 : %v", err)
    }

    defer lst.Close()

    srvGRPC := grpc.NewServer()
    hellopb.RegisterHelloServiceServer(srvGRPC, &srvService{})
    log.Panic(srvGRPC.Serve(lst))
}
```

## 3- Implémentation du client

### 1-Rappel sur le codage du client

- Création d'une connexion avec ```grpc.Dial()``` : nécessite deux arguments (une chaîne et de options : adresse avec son port à écouter et des options sous forme d'appel de fonctions ici ```WithInsecure()```) et retourne une connexion et une erreur
- Gestion de l'erreur
- Différer la fermeture de la connexion
- Création du client avec appel de la fonction ```hellopb.NewHelloServiceClient()``` auquel on passe notre connexion

Code :

```go
package main

import (
    "api-stream-serveur/hellopb"
    "fmt"
    "log"

    "google.golang.org/grpc"
)

func main() {
    fmt.Println("Client - API Stream Server")
    cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
    if err != nil {
        log.Fatalf("Erreur au lancement du client : %v", err)
    }
    defer cnx.Close()

    client := hellopb.NewHelloServiceClient(cnx)
}
```

### 2-Codage de la requête

Nous disposons que d'une méthode accessible pour le client : ```HelloStreamServer()```. Cette méthode demande deux arguments au moins :

- Premier: un contexte
- Second : une requête de type pointeur de ```hellpb.HelloRequete```
- Troisième : des éléments optionnels de type ```gprc.CallOption```

1. Le contexte passé sera le contexte *racine* : ```context.Background()```
2. Créons un pointeur ```req``` de type ```hellopb.HelloRequete``` : un pointeur d'une structure à construire :

```go
req := &hellopb.HelloRequete{
    Message: &hellopb.HelloMessage{
        Prenom: "John",
        Nom:    "Doe",
    },
}
```

Et l'on passe ce pointeur ```req``` à la méthode ```HelloStreamServeur```.

```go
rsltStream, err := client.HelloStreamServeur(
    context.Background(),
    req,
)
```

Que retourne la méthode ```HelloStreamServeur``` ? un objet de type ```hellopb.HelloService_HelloStreamServeurClient``` qui représente le flux renvoyé par le serveur, et une erreur.

On gère d'abord l'erreur ...

```go
if err != nil {
    log.Panicf("Erreur sur le streaming serveur : %v", err)
}
```

... puis nous devons gérer le flux reçu avec la méthode ```Recv()```. Celle-ci ne prend aucun argument mais retourne deux valeurs : un pointeur de type ```*hellopb.HelloReponse``` et une erreur. Comme nous avons affaire à un flux, il nous faut itérer. L'arrêt de la boucle ne sera possible que si la méthode ```Recv()``` retourne une erreur particulière.

**Le point à bien se remémorer est la gestion de l'erreur** : la méthode ```Recv()``` récupèrera les données jusqu'à ce que le serveur n'envoie plus rien ; à ce moment-là, une erreur de type ```io.EOF``` sera retournée. Il faudra néanmoins une autre gestion d'erreur pour gérer toutes les autres sources d'erreur possibles.

Pour récupérer toutes les données, on utilisera une boucle for infinie :

```go
for {
    msg, err := rsltStream.Recv()
    if err == io.EOF {
        log.Println("** Fin d'envoi des données **")
        break
    }
    if err != nil {
        log.Printf("Une erreur a eu lieu à la récupération d'une donnée : %v", err)
        break
    }

    fmt.Printf("[Réception donnée] : %v\n", msg.Reponse)
}
```

### 3-Code complet du client

```go
package main

import (
    "api-stream-serveur/hellopb"
    "context"
    "fmt"
    "io"
    "log"

    "google.golang.org/grpc"
)

func main() {
    fmt.Println("Client - API Stream Server")
    cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
    if err != nil {
        log.Fatalf("Erreur au lancement du client : %v", err)
    }
    defer cnx.Close()

    client := hellopb.NewHelloServiceClient(cnx)

    req := &hellopb.HelloRequete{
        Message: &hellopb.HelloMessage{
            Prenom: "John",
            Nom:    "Doe",
        },
    }

    rsltStream, err := client.HelloStreamServeur(
        context.Background(),
        req,
    )
    if err != nil {
        log.Panicf("Erreur sur l'envoi de la requête : %v", err)
    }

    for {
        msg, err := rsltStream.Recv()
        if err == io.EOF {
            log.Println("Fin d'envoi des données")
            break
        }
        if err != nil {
            log.Printf("Une erreur a eu lieu à la récupération d'une donnée : %v", err)
            break
        }

        fmt.Printf("[Réception donnée] : %v\n", msg.Reponse)
    }

}

```

[<- Précédent](4-API-unaire.md) || [Suivant ->](6-Streaming-client.md)
