# 4-API unaire

On rappelle qu'il s'agit d'un simple échange entre un client et un serveur : le client envoie une requête et le serveur enverra sa réponse.

Cette implémentation est la plus courante, la plus utilisée.

Conseils :

- on commencera par développer une API unaire et si l'on a des problèmes de performances alors on se dirigera vers une API streaming.
- on utilisera l'API unaire si les données envoyées sont légères.

## 1-Protocol Buffer et API unaire

### 1-Projet

On se crée un nouveau répertoire avec initialisation d'un module ainsi que les sous-répertoires et fichiers suivants :

```sh
mkdir -p ./api-unaire/serveur ./api-unaire/client ./api-unaire/hellopb
touch ./api-unaire/serveur/serveur.go ./api-unaire/client/client.go ./api-unaire/hellopb/hello.proto
cd api-unaire
go mod init api-unaire
```

### 2-Définition du hello.proto

Notre fichier ```.api-unaire/hellopb/hello.proto``` va contenir trois nouveaux messages qui seront la requête et la réponse et un ```service```.

Le premier message sera ```HelloMessage```  et devra inclure deux champs de type ```string``` : ```prenom``` et ```nom```.

```proto
message HelloMessage {
    string prenom = 1;
    string nom = 2;
}
```

Ce message sera embarqué dans un message ```HelloRequete``` incluant un seul champ ```hello``` de tytpe ```HelloMessage```.

```proto
message HelloRequete {
    HelloMessage hello = 1;
}
```

Le message retourné en réponse sera ```HelloReponse``` et incluera un seul champ : ```reponse``` de type ```string```.

```proto
message HelloReponse {
    string reponse = 1;
}
```

Et on créera un service ```HelloService``` définissant une méthode RPC ```Hello``` prenant en paramètre un message de type ```HelloRequete``` et retournera un message ```HelloReponse```  :

```proto
service HelloService {
    rpc Hello(HelloRequete) returns (HelloReponse) {};
}
```

#### Code

On n'oubliera pas de définir la syntaxe, un nom pour le package et l'option ```go_package``` (qui indique où l'on doit placer le code Go généré par le compilateur ```protoc```) :

```proto
syntax = "proto3";

package helloApiUnaire;
option go_package=".;hellopb";

message HelloMessage {
    string prenom = 1;
    string nom = 2;
}

message HelloRequete {
    HelloMessage hello = 1;
}

message HelloReponse {
    string reponse = 1;
}

service HelloService {
  rpc Hello(HelloRequete) returns (HelloReponse) {}
}
```

Observez les conventions de nommages : on a service NomService, une requête NomRequete (ou *NomRequest*, pour les anglo-saxons), une réponse NomReponse (*NomResponse*)

Maintenant compilons le code Proto : (si on se trouve dans le répertoire ./hellopb, sinon on adaptera le chemin correctement en fonction du répertoire courant du shell)

```sh
protoc ./hello.pb --go_out=plugins=grpc:.
```

Si on se trouve à la racine du projet (soit ```./api-unaire```) et que l'on a oublié le nom de son fichier Proto :

```sh
protoc ./hellopb/*.proto --go_out=plugins=grpc:./hellopb  
```

Si tout s'est bien déroulé, le fichier ```hello.pb.go``` aura été créé avec des interfaces, fonctions, méthodes, variables nouvelles prêtes à l'emploi.

Pour les dépendances, on se place à la racine du projet et on tape les commandes :

```sh
go mod tidy
go mod vendor
```

## 2-Serveur et API unaire

Si l'on regarde ```hello.pb.go```, on dispose d'une interface pour le service Protocol Buffer ```HelloService``` nommée ```HelloServiceServer``` ! Comme nous avons affaire à une interface, il faudra **que notre serveur associé au service implémente la méthode demandée** :

```go
// HelloServiceServer is the server API for HelloService service.
type HelloServiceServer interface {
    Hello(context.Context, *HelloRequete) (*HelloReponse, error)
}
```

Il nous faut implémenter cette interface qui demande une méthode ```Hello()```. C'est  cette méthode qui va récupérer un contexte et la requête du client, et retourner une réponse et une erreur. C'est pour cela que nous avions créer une structure ```serveur``` dans ```serveur.go``` dans la section serveur du chapitre 3 [boilerplate code](3-boilerplate-codes.md).

### 1-Mise en place du code boilerplate du serveur

Etapes :

- Création d'un type struct vide pour le serveur associé au service côté serveur de Hello
- Création d'une instance de ```net.Listen(protocole, adresseEtPort)```
- Gestion des erreurs
- Création d'une nouvelle instance de serveur gRPC avec ```grpc.NewServer()```
- Enregistrement du serveur gRPC et du serveur associé au service à l'aide de ```hellopb.RegisterHelloServiceServer()```
- Lancer le serveur gRPC avec ```Serve()``` en lui passant l'instance du port ouvert avec ```net.Listen```
- Gestion des erreurs

Code :

```go
package main

import (
    "api-unaire/hellopb"
    "fmt"
    "log"
    "net"

    "google.golang.org/grpc"
)

type serveurService struct{}

func main() {
    fmt.Println("Serveur - API unaire")

    lis, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur à l'écoute sur le port 50051 : %v", err)
    }

    srvGRPC := grpc.NewServer()
    hellopb.RegisterHelloServiceServer(srvGRPC, &serveurService{})

    if err := srvGRPC.Serve(lis); err != nil {
        log.Fatalf("Erreur au lancement du serveur gRPC : %v", err)
    }

}
```

### 2-Implémentation du service Hello

Le code ci-dessus ne compilera pas car notre serveur associé aux services doit implémenter la méthode suivante :

```go
Hello(context.Context, *HelloRequete) (*HelloReponse, error)
```

Donc au type struct qui joue le rôle d'objet, on lui adjoint la fonction *receiver* ou méthode :

```go
type serveurService struct{}

func (*serveurService) Hello(ctx context.Context, in *hellopb.HelloRequete) (*hellopb.HelloReponse, error) {
    return nil, nil
}
```

**Remarque** : il faut faire attention aux imports. Ici nous importons depuis notre package ```hellopb```.

Maintenant qu'allons-nous implémenter dans cette méthode ?

**Objectif** : on récupère les nom et prénoms et on retourne une chaîne : "Salut, prénom nom".

Le code suivant est très simple et facile à comprendre (les explications sont dans les commentaires). Il y a une seule chose à laquelle il faut bien prêter attention c'est au retour : on retourne l'adresse de notre pointeur !

```go
func (*serveurService) Hello(ctx context.Context, in *hellopb.HelloRequete) (*hellopb.HelloReponse, error) {
    log.Printf("La fonction HelloRequete a été invoquée avec %v", in)
    // nous récupérons les valeurs associée à Prenom et Nom
    msg := fmt.Sprintf("Hello, %s %s", in.GetHello().Prenom, in.GetHello().Nom)
    // Version alternative
    //msg := fmt.Sprintf("Hello, %s %s", in.GetHello().GetPrenom, in.GetHello().GetNom)

    // Pour créer une réponse, on appelle simplement la structure publique
    // hellopb.Reponse{} et on affecte la chaîne au champ Reponse
    rep := hellopb.HelloReponse{
        Reponse: msg,
    }
    // Attention on retourne une référence !!!
    return &rep, nil
}
```

```GetHello()``` retourne un objet ```hellopb.HelloMessage``` et de cet objet on dispose de méthodes générées par ```protoc```. On va notamment pouvoir extraire les données ```nom``` et ```prenom``` avec les méthodes ```GetNom()``` retournant une chaîne de caractère et ```GetPrenom()``` (retournant aussi une string). On peut aussi utiliser les variables publiques : ```Prenom``` et ```Nom```.

La réponse que l'on doit retourner est aussi une chaîne mais **attention**, ne devons retourner une structure de type ```hellopb.HelloReponse``` avec pour seul champ ```Reponse```. Ne pas oublier le symbole ```&``` à ```hellopb.HelloReponse``` car la signature demande un pointeur.

Le serveur est normalement fonctionnel : nous avons déjà un serveur gRPC et nous avons enregistrer les services à notre structure serveur.

### 3-Code du serveur

```go
package main

import (
    "api-unaire/hellopb"
    "context"
    "fmt"
    "log"
    "net"

    "google.golang.org/grpc"
)

type serveurService struct{}

func (*serveurService) Hello(ctx context.Context, in *hellopb.HelloRequete) (*hellopb.HelloReponse, error) {
    log.Printf("La fonction HelloRequete a été invoquée avec %v", in)
    // nous récupérons les valeurs associée à Prenom et Nom
    msg := fmt.Sprintf("Hello, %s %s", in.GetHello().Prenom, in.GetHello().Nom)
    // Version alternative
    //msg := fmt.Sprintf("Hello, %s %s", in.GetHello().GetPrenom, in.GetHello().GetNom)

    // Pour créer une réponse, on appelle simplement la structure publique
    // hellopb.Reponse{} et on affecte la chaîne au champ Reponse
    rep := hellopb.HelloReponse{
        Reponse: msg,
    }
    // Attention on retourne une référence !!!
    return &rep, nil
}

func main() {
    fmt.Println("Serveur - API unaire")

    lis, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur à l'écoute sur le port 50051 : %v", err)
    }

    srvGRPC := grpc.NewServer()
    hellopb.RegisterHelloServiceServer(srvGRPC, &serveurService{})

    if err := srvGRPC.Serve(lis); err != nil {
        log.Fatalf("Erreur au lancement du serveur gRPC : %v", err)
    }

}
```

## 3-Client et API unaire

### 1-Mise en place du code de base du client

Notre client demande une connexion à un serveur local sur un port TCP précis sans sécurisation SSL.

Rappel :

- Création d'une connexion avec ```grpc.Dial()``` : nécessite deux arguments (une chaîne et de options : adresse avec son port à écouter et des options sous forme d'appel de fonctions) et retourne une connexion et une erreur
- Gestion de l'erreur
- Différer la fermeture de la connexion
- Passer la connexion au client du service créé à l'aide de la fonction ```hellopb.NewHelloServiceClient(cnx)```

Code :

```go
package main

import (
    "api-unaire/hellopb"
    "fmt"
    "log"

    "google.golang.org/grpc"
)

func main() {
    fmt.Println("Client - API unaire")

    cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
    if err != nil {
        log.Fatalf("Erreur à l'ouverture de la connexion vers le serveur : %v", err)
    }
    defer cnx.Close()

    client := hellopb.NewHelloServiceClient(cnx)
    log.Printf("Client créé : %f", client)

}
```

### 2-Création du service Hello

L'objet ```client``` dispose d'un accès à une seul méthode : ```Hello()```. Cette méthode doit être implémentée : elle fait partie de l'interface ```HelloServiceClient```. Et elle a été générée pour nous par ```protoc```. Observons sa signature :

```go
type HelloServiceClient interface {
    Hello(ctx context.Context, in *HelloRequete, opts ...grpc.CallOption) (*HelloReponse, error)
}
```

Les différents arguments sont :

- pour le contexte on va appeler la méthode ```context.Background()```. Pour plus d'information sur ce package provenant de la bibliothèque standard on se réfèrera sur la [doc officielle](https://golang.org/pkg/context/) ainsi que pour la fonction [Background()](https://golang.org/pkg/context/#Background).
- pour l'argument ```in``` on doit créer notre requête c'est-à-dire ce que l'on souhaite envoyer ! Pour cela on crée une référence vers ```hellopb.RequeteHello``` qui devra définir un nouveau message de type ```hellopb.HelloMessage```.
- aucun argument optionnel ne sera fourni.

### 3-Création de la requête

Attention au type ```hellopb.RequeteHello``` ! Il demande un champ ```Hello``` qui correspond au type ```*hellopb.HelloMessage``` : conformément à notre schéma défini avec Protocol Buffer. Ce qui donne l'objet ```req``` suivant :

```go
    req := &hellopb.RequeteHello{
        Hello: &hellopb.HelloMessage{
            Prenom: "John",
            Nom:    "Doe",
        },
    }
```

Maintenant on peut passer ```req``` à notre méthode ```HelloRequete```. Nous pouvons passer des options mais cela ne nous intéresse pas pour le moment.

Il nous faut ensuite récupérer les valeurs de retour de cette méthode : un pointeur de type ```hellopb.HelloReponse``` et une erreur. On va pouvoir récupérer la valeur de la réponse retournée par le serveur via la méthode ```GetReponse()```. Il existe une version alternative en récupérant la valeur directement à partir du champ ```Reponse``` : ```rep.Reponse```.

```go
    // Envoi de la requête
    rep, err := client.Hello(
        context.Background(),
        req,
    )

    if err != nil {
        log.Printf("Erreur sur le retour serveur : %v", err)
    }
    log.Printf("Message du serveur : %v", rep.Reponse)
```

### 4-Code complet du client

```go
package main

import (
    "api-unaire/hellopb"
    "context"
    "fmt"
    "log"
    "os"

    "google.golang.org/grpc"
)

func main() {
    fmt.Println("Client - API unaire")

    cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
    if err != nil {
        log.Fatalf("Erreur à l'ouverture de la connexion vers le serveur : %v", err)
    }
    defer cnx.Close()

    client := hellopb.NewHelloServiceClient(cnx)
    log.Printf("Client créé : %f", client)

    // Création de la requête et du message HelloMessage
    req := &hellopb.HelloRequete{
        Hello: &hellopb.HelloMessage{
            Prenom: "John",
            Nom:    "Doe",
        },
    }

    // Envoi de la requête
    rep, err := client.Hello(
        context.Background(),
        req,
    )

    if err != nil {
        // Attention au log.Fatal ! cf. Doc officielle : les defer ne sont pas pris en compte car os.Exit() termine immédiatement le programme.
        log.Panicf("Erreur sur le retour serveur : %v", err)
    }
    log.Printf("Message du serveur : %v", rep.Reponse)
}

```

[<- Précédent](3-boilerplate-codes.md) || [Suivant ->](5-Streaming-server.md)
