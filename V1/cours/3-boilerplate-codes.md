# 3-Boilerplate codes

Pour l'instant nous créons de fichiers de test pour vérifier que notre code et éditeur sont tous fonctionnels.

## 1-Serveur

**objectifs** :

- créer un serveur sans service.
- lancer et arrêter le serveur sur un port défini.
- utiliser un code que l'on réutilisera à chaque création de serveur (boilerplate code).

### 1-Arborescence du projet

On se crée un répertoire : ```boilerplate-code```. On se créera un module avec ```go mod init``` puis ```go mod tidy``` et enfin, si l'on souhaite, un ```go mod vendor```. Comme précédemment on se crée un répertoire ```hellopb``` et on copiera le contenu de ```premier-code/hellopb``` dans ce nouveau répertoire.

On se créera un autre répertoire : ```serveur``` dans lequel on crée un fichier ```serveur.go```.

Notre arborescence ressemblera à :

```txt
boilerplate-code/
        |__serveur/
                |__serveur.go
        |__hellopb/
                |__hello.pb.go
                |__hello.proto
```

### 2-Codage du serveur

Le serveur est un exécutable donc on créera un package ```main``` et une fonction ```main```.

#### 1-Ecoute du port TCP

La toute première opération est de créer un listener dans notre fichier ```serveur.go``` : il sera en charge d'écouter un port TCP particulier avec la fonction ```Listen``` du package ```net```. Cette fonction prend deux arguments : le type de réseau (string) et une adresse avec le port à écouter (string). Le port par défaut pour le gRPC est le numéro 50051 :

```go
listener, err := net.Listen("tcp", "0.0.0.0:50051")
if err != nil {
    log.Fatalf("Echec à l'écoute du port 50051 : %v",err)
}
```

#### 2-Création du serveur gRPC

Pour utiliser notre listener, nous allons utiliser un serveur gRPC disponible dans la bibliothèque ```grpc``` avec la fonction ```NewServer```.

```go
s := grpc.NewServer()
```

**NB** : VSCode peut proposer plusieurs imports possibles. Il convient de sélectionner la version ```google.golang.org/grpc```

On obtient le code suivant :

```go
package main

import (
    "log"
    "net"

    "google.golang.org/grpc"
)

func main() {
    fmt.Println("Premier serveur - boilerplate code")
    listener, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur à l'écoute sur le port 50051 : %v", err)
    }

    s := grpc.NewServer()

}
```

#### 3-Création d'un type struct pour les services

Avant de pouvoir utiliser notre serveur gRPC, on va créer un type structure :

```go
import (
    ...
)
// on crée une structure vide
type srv struct{}

func main() {
    ...
}
```

Ce nouveau type représentera un serveur particulier pour la gestion des services à implémenter. Il s'agit plus précisément d'une future interface à implémenter pour les services. Mais pour l'instant nous n'avons pas à nous préoccuper.

#### 4-Enregistrement des serveur et services

Maintenant on doit enregistrer les services afin de les manipuler dans notre serveur.

Pour cela on va recourir à notre bibliothèque générée par ```protoc``` à savoir ```hello.pb.go```.

Cette bibliothèque dispose de la fonction ```RegisterHelloServiceServer``` permettant de créer un serveur gRPC. Cette prend deux paramètres :

- un serveur gRPC (ici ```s```)
- un serveur pour les services (ici notre nouveau type ```srv``` sous la forme d'une référence vers une structure ```srv``` vide) :

```go
package main

import (
    "boilerplate-code/hellopb/hellopb"
    "log"
    "net"

    "google.golang.org/grpc"
)

type srv struct{}

func main() {
    fmt.Println("Premier serveur - boilerplate code")
    listener, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur à l'écoute sur le port 50051 : %v", err)
    }

    s := grpc.NewServer()

    hellopb.RegisterHelloServiceServer(s, &srv{})
}
```

On a interconnecté le serveur gRPC et nos services.

#### 5-Mise en place effective du serveur gRPC

Maintenant on va pouvoir lancer notre serveur. Pour cela, on appelle la méthode ```Serve()``` de notre objet ```s```. Cette fonction a pour objectif d'écouter les connexions entrantes donc elle demande notre listener en paramètre et retourne une erreur :

```go
...
func main() {

    ...

    if err := s.Serve(listener); err != nil {
        log.Fatalf("Echec serveur : %v", err)
    }
}
```

### 3-Code complet du serveur

```go
package main

import (
    "boilerplate-code/hellopb/hellopb"
    "fmt"
    "log"
    "net"

    "google.golang.org/grpc"
)

type srv struct{}

func main() {
    fmt.Println("Premier serveur - boilerplate code")
    listener, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur à l'écoute sur le port 50051 : %v", err)
    }

    s := grpc.NewServer()

    hellopb.RegisterHelloServiceServer(s, &srv{})

    if err := s.Serve(listener); err != nil {
        log.Fatalf("Echec serveur : %v", err)
    }
}
```

On teste avec ```go run``` comme précédemment. Et si tout s'est bien déroulé : notre message s'affiche et le serveur attend une connexion !

contrôle-C pour terminer le programme.

## 2-Client

**objectifs** :

- connecter un client gRPC à un serveur gRPC
- lancer et arrêter le client
- créer un code boilerplate

### 1-Arborescence mise à jour

Dans le répertoire ```boilerplate-code```, créons un nouveau répertoire ```client``` et à l'intérieur de ce dernier, on crée un fichier ```client.go```.

L'arborescence de notre module ressemblera à :

```txt
boilerplate-code/
        |__client/
                |__client.go
        |__serveur/
                |__serveur.go
        |__hellopb/
                |__hello.pb.go
                |__hello.proto
```

### 2-Codage du client

Ce client étant un exécutable (indépendant du serveur), le package sera ```main``` et contiendra une fonction principale ```main```.

```go
package main

import "fmt"

func main() {
    fmt.Println("Client du projet boilerplate-code")
}
```

#### 1-Création d'une connexion

La première chose à faire est de créer une connexion vers le serveur : pour cela, on utilisera une fonction ```Dial()``` proposée par la bibliothèque ```gooogle.golange.org/grpc```. Cette fonction prend deux arguments :

- une adresse avec le port du serveur : ```localhost:50051```
- des options : ici, nous allons appeler une fonction ```WithInsecure()``` indiquant que les échanges client-serveur ne seront pas sécurisés. Par défaut, gRPC requiert une connexion SSL. [Nous y reviendrons plus tard](8-Notions-avancees.md#3-SSL).

Cette fonction retourne deux choses :

- un pointeur de type ```grpc.ClientConn```
- une erreur

```go
cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
if err != nil {
    log.Fatalf("Echec à la connexion : %v", err)
}
```

#### 2-Différer la fermeture de la connexion

Nous appellerons ```defer``` pour fermer la connexion à la fin de toutes les opérations de notre programme.

```go
defer cnx.Close()
```

#### 3-Création du client

Avec le nouvel objet ```cnx```, nous allons appeler une fonction ```NewHelloServiceClient()``` de notre bibliothèque ```hellopb```, à laquelle on passe notre objet connexion ```cnx``` (de type ```*grpc.ClientConn```). Cette fonction a été générée lors de la compilation avec ```protoc```.

Cette fonction retourne un *objet*  de type ```hellopb.HelloServiceClient```.

Pour savoir ce qui se cache derrière nous allons tout simplement afficher son contenu sur le terminal avec ```fmt.Println()```

### 3-Code complet du client

```go
package main

import (
    "boilerplate-code/hellopb/hellopb"
    "fmt"
    "log"

    "google.golang.org/grpc"
)

func main() {
    fmt.Println("Client du projet boilerplate-code")

    cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
    if err != nil {
        log.Fatalf("Echec à la connexion : %v", err)
    }

    defer cnx.Close()

    client := hellopb.NewHelloServiceClient(cnx)
    fmt.Printf("Client créé : %f\n", client)
}
```

## 2-Test des codes

Pour cela on ouvre deux terminaux : un pointant vers le serveur et un vers le client. On lance les ```go run``` respectifs : **d'abord le serveur puis le client**.

Le retour du client est de la forme suivante :

```txt
Client du projet boilerplate-code
Client créé : &{%!f(*grpc.ClientConn=&{0xc0000c4bc0 0x4946c0 localhost:50051 {passthrough  localhost:50051} localhost:50051 {<nil> <nil> [] [] <nil> <nil> {{1000000000 1.6 0.2 120000000000}} false false true 0 <nil>  {grpc-go/1.35.0 <nil> false [] <nil> <nil> {0 0 false} <nil> 0 0 32768 32768 0 <nil> true} [] <nil> 0 false true false <nil> <nil> <nil> <nil> 0x7f12c0 []} 0xc0000acea0 {<nil> <nil> <nil> 0 grpc-go/1.35.0 {passthrough  localhost:50051}} 0xc000144720 {{{0 0} 0 0 0 0} 0xc0000b4288} {{0 0} 0 0 0 0} 0xc0000b6900 0xc0000bc690 map[0xc0000ed080:{}] {0 0 false} pick_first 0xc0000c4cc0 {<nil>} 0xc0000ace80 0 0xc00009e720 {0 0} <nil>})}
```

[<- Précédent](2-pre-requis.md) || [Suivant ->](4-API-unaire.md)
