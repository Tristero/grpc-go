# 1-Introduction

gRPC est un protocole développé par Google qui repose sur deux éléments importants :

- Protocol Buffer : léger, binaire, fortement typé contrairement à JSON, XML.
- HTTP/2 : en-têtes compressés, une seule connexion TCP pour effectuer différentes requêtes (multiplexage), latence et bande-passante réduites, support du push côté serveur, manipule du binaire, "SSL" recommandé mais pas obligatoire.

## 1-Types d'échange avec gRPC

Il existe 4 types d'API réalisables avec gRPC :

- API unaire
- streaming serveur
- streaming client
- streaming bidirectionnel

### Unaire

Le client effectue une requête vers le serveur et ce dernier lui répond. C'est le cas le plus simple, le plus connu car les requêtes REST traditionnelles reposent sur ce principe. Mais ici nous emploierons le protocole HTTP/2 et Protocol Buffer.

### Streaming serveur

Le streaming est *de facto* inclu dans HTTP/2. Le client fait une requête sur le serveur, et ce dernier lui retourne X messages, un flux de message avec une seule et unique requête issue du client.

### Streaming client

Le principe est exactement inverse au précédent où c'est le client qui envoie un flux de message vers le serveur : il envoit un premier message, puis un second, et ainsi de suite jusqu'à épuisement. A ce moment là, le serveur répondra.

### Streaming bidirectionnel

Le flux des échanges s'effectue dans les deux sens : le client envoie un premier puis autre message et le serveur peut envoyer un message après le premier, le second ou un énième message reçu et le client peut envoyer un autre message. L'asynchronicité est au coeur de ce processus bidirectionnel. Son implémentation est la plus complexe à créer et à gérer.

## 2-Exemple d'un service gRPC

Concrètement la description gRPC est très simple avec Protocol Buffer :

```proto
service SalutService {
    // Unaire
    rpc Salut(RequeteSalut) returns (ReponseSalut) {};

    // Streaming serveur
    rpc SalutPlusieursFois(RequeteMultiplesSalutations) returns (stream ReponseMultiplesSalutations) {};

    // Streaming client
    rpc LongueSalutation(stream RequeteLongueSalutation) returns (ReponseSalutationLongue) {};

    // Streaming bidirectionnel
    rpc SalutTous(stream RequeteSalutATous) returns (stream ReponseSalutATous) {};
}
```

### Explications

On a créé un service définissant des méthodes avec le mot-clé ```service``` suivi d'un nom (ici ```SalutService```). Le principe d'un service est le même qu'une interface en Java/C#/Go : on définit des signatures de méthodes.

On définit à l'intérieur de ce service des requêtes avec le mot-clé ```rpc``` suivi d'un nom, d'un paramètre et d'un message de retour (qui sera **obligatoirement** préfixé avec ```returns```). Le type envoyé ou retourné peut être un simple message (pas de mot-clé). Dans le cas d'un flux de message il faudra **obligatoirement** préfixé le nom du message retourné avec ```stream```.

La première méthode définie est une méthode RPC ```salut```. Cette méthode attend un argument. Plus précisément, elle attend un message Protocol Buffer (que l'on aura défini préalablement) ```RequeteSalut```. Cette requête devra retourner un autre message unique : ```ReponseSalut```. Schématiquement parlant : le client envoit un message d'un certain type et attendra une réponse du serveur qui sera un message d'un certain type. La relation est 1-1.

La seconde méthode défini une méthode ```SalutPlusieursFois``` : l'argument est un message unique envoyé par le client. Le serveur retournera un flux de message : grâce au mot-clé ```stream``` le serveur et le client "savent" qu'il y aura X messages retournés par le serveur.

Les troisième et quatrième méthodes ne présentent aucune difficulté de compréhension.

C'est clair et simple avec Protocol Buffer.

## 3-Asynchronicité

Avec gRPC, il faudra gérer l'asynchronocité. Qui dit asynchrone, dit opération non-bloquante : on va pouvoir envoyer des quantités importantes de requête en parallèle. Côté client, le code pourra être asynchrone donc non-bloquant soit synchrone et donc bloquant. Ce ne sera pas le cas du côté serveur : il devra traiter plusieurs requêtes en parallèle. Par exemple, Google avec gRPC peut en interne gérer jusqu'à 10 milliards de requête par seconde. Côté client on aura le choix : soit synchrone donc bloquant ou asynchrone.

## 4-Sécurité

gRPC recommande très fortement l'utilisation du protocole SSL/TLS (par pure commodité on n'emploiera que l'acronyme SSL). Cela implique que SSL et la sécurité sont des citoyens de première classe. Chaque langage fournira une API pour toute requête requiérant la manipulation de certificats ainsi que tous les outils pour le chiffrement. De même, avec des intercepteurs, il nous sera possible de gérer l'authentification. Tout cela est abordé ultérieurement dans les [notions avancées](8-Notions-avancees.md). La sécurité est centrale dans les échanges avec gRPC.

## 5-Avantages

Reposant sur HTTP/2 et Protocol Buffer, gRPC offre une plus grande flexibilité dans la création d'APIs Web, plus légères, rapides (latences du réseau réduites, charge processeur plus légère). gRPC dépasse la conception CRUD propre aux APIs REST traditionnelles : le développeur, l'architecte sont dorénavant libres de la façon dont les API peuvent être élaborées. En utilisant HTTP/2, le streaming est pleinement supporté. Le code généré avec Protocol Buffer s'effectue dans le langage de son choix (au moment de la rédaction plus d'une dizaine de langages supportent officiellement ou non gRPC). Contrairement aux APIs REST, on n'a pas à se soucier d'utiliser des APIs diverses pour manipuler des verbes HTTP/1 : gRPC gère la génération du code nécessaire au bon fonctionnement des échanges. gRPC est 25 fois plus performant que la traditionnelle API REST pour l'obtention d'une réponse.

[<- Précédent](0-cours.md) || [Suivant ->](2-pre-requis.md)
