# 7-Streaming bidirectionnel

Encore une fois, cette communication bidirectionnelle en flux n'est possible qu'avec HTTP/2. Client et serveur communique en envoyant des flux de messages.

*Remarque* : pour aller plus vite, on écrit ```BiDi``` pour Bi Directional Streaming.

**Il faut bien garder à l'esprit que le nombre de requêtes et de réponses n'a pas à correspondre**.

L'utilité d'un tel système :

- lorsque l'on souhaite envoyer des données massives de façon asynchrone : pas besoin d'attendre une réponse du serveur pour envoyer une donnée et vice-versa.
- pour créer un chat
- pour des connexions nécessitant un long temps pour fonctionner

## 1-Mise en place du projet

```sh
mkdir -p ./api-bidi/serveur ./api-bidi/client ./api-bidi/hellopb
touch ./api-bidi/serveur/serveur.go ./api-bidi/client/client.go ./api-bidi/hellopb/hello.proto
cd ./api-bidi
go mod init api-bidi
```

## 2-Protocol Buffer et BiDi

Avec Protocol Buffer, pour créer notre service, il faudra utiliser en requête et en réponse le mot-clé ```stream```.

Du côté des services RPC, nous définissons notre nouvelle API comme suit :

```proto
syntax = "proto3";

package apiBidi;
option go_package=".;hellopb";

message HelloMessage {
    string Nom = 1;
    string Prenom = 2;
}

message HelloRequete {
    HelloMessage hello = 1;
}

message HelloReponse {
    string Reponse = 1;
}

service HelloService {
    rpc HelloStreamBiDi(stream HelloRequete) returns (stream HelloReponse) {};
}
```

On compile avec ```protoc```. Puis on appelle ```go mod tidy``` pour récupérer nos dépendances et ```go mod vendor```, si besoin est.

## 3-Serveur et API BiDi

### 1- Rappel sur le codage du serveur

Rappel de la création du serveur, encore une fois :

- création d'un type struct vide pour notre service gRPC
- appel de ```net.Listen()``` pour permettre la création d'un serveur en ouvrant un auditeur. On lui passe deux chaînes : le protocole et l'adresse IP suivi du port à écouter. Elle retourne deux valeurs : une instance ```Listener``` et une erreur
- gestion de l'erreur
- différer la fermeture du ```Listener```
- créer une nouvelle instance de serveur gRPC avec ```grpc.NewServer()```
- appel de ```hellopb.RegisterHelloServiceServer()``` pour enregistrer notre serveur gRPC et un pointeur de service vide
- lancement du serveur avec ```grpc.Serve()``` en lui passant en paramètre l'instance de ```Listener```

Code partiel :

```go
package main

import (
    "api-bidi/hellopb"
    "fmt"
    "log"
    "net"

    "google.golang.org/grpc"
)

type service struct{}

func main() {
    fmt.Println("Serveur - Api Stream Bidirectionnel")

    listen, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur à la création du Listener : %v", err)
    }
    defer listen.Close()

    s := grpc.NewServer()
    hellopb.RegisterHelloServiceServer(s, &service{})
    log.Panic(s.Serve(listen))
}
```

### 2-Implémentation du service

Le code n'est pas fonctionnel car nous devons implémenter :

```go
// HelloServiceServer is the server API for HelloService service.
type HelloServiceServer interface {
    HelloStreamBiDi(HelloService_HelloStreamBiDiServer) error
}
```

Pour notre exemple, le serveur devra répondre à chaque message reçu. Mais dans d'autres situations, ce n'est pas une obligation : le serveur est libre de choisir combien de réponses il faudra retourner.

De même, nous n'utiliserons pas de goroutines pour le serveur.

Contrairement à une communication en flux du client, les méthodes pour le serveur ne sont pas *fermantes* : **on envoie et on reçoit**. Dans le [chapitre précédent](6-Streaming-client.md), le serveur disposait une méthode ```Recv()``` et ```SendAndClose()``` : avec cette dernière, on envoyait une réponse unique et on clotûrait la connexion.

Ici on reçoit et envoie X fois. Pour notre projet, le serveur recevra une requête client et il y répondra de suite.

On va récupérer la requête client, itérer (boucle ```for``` infinie) à chaque fois que l'on reçoit une requête d'un client, le serveur devra répondre ! Dans cette boucle, on appelle la méthode ```Recv()``` de l'objet ```stream```. Cette méthode retourne deux choses :

- un objet de type ```*hellopb.HelloRequete``` qui est la requête client
- une erreur

Traditionnellement on traitait l'erreur ```io.EOF``` en renvoyant un message au client. Ici on retournera ```nil``` : la réception des données s'est déroulée normalement. Puis ensuite nous gérerons les erreurs inattendues.

Traitons le comportement de la réponse (toujours dans la boucle ```for```). On va récupérer les valeurs et retourner une simple chaîne concaténant prénom et nom. Et cette chaîne sera retournée au client. Pour cela on va utiliser l'objet ```stream``` et sa méthode ```Send``` à laquelle on passe un pointeur de la structure ```hellopb.HelloReponse``` contenant notre nouveau message :

```go
str := fmt.Sprintf("Hello, %s %s !", req.GetHello().GetPrenom(), req.GetHello().GetNnom())

rep := &hellopb.HelloReponse{
    Reponse: str,
}
```

Et comme ```Send()``` retourne une erreur, on va gérer cette dernière en retournant le message.

### 3-Code complet

```go
package main

import (
    "api-bidi/hellopb"
    "fmt"
    "io"
    "log"
    "net"

    "google.golang.org/grpc"
)

type service struct{}

func (*service) HelloStreamBiDi(stream hellopb.HelloService_HelloStreamBiDiServer) error {

    log.Println("Appel de la méthode HelloStreamBiDi")

    for {
        req, err := stream.Recv()
        if err == io.EOF {
            return nil
        }
        if err != nil {
            log.Printf("Erreur dans la réception d'une requête : %v", err)
            return err
        }

        str := fmt.Sprintf("Hello, %s %s !", req.GetHello().GetPrenom(), req.GetHello().GetNom())

        rep := &hellopb.HelloReponse{
            Reponse: str,
        }

        if err := stream.Send(rep); err != nil {
            log.Printf("Erreur à l'envoi d'une réponse : %v", err)
            return err
        }
    }
}

func main() {
    fmt.Println("Serveur - Api Stream Bidirectionnel")

    listen, err := net.Listen("tcp", "0.0.0.0:50051")
    if err != nil {
        log.Fatalf("Erreur à la création du Listener : %v", err)
    }
    defer listen.Close()

    s := grpc.NewServer()
    hellopb.RegisterHelloServiceServer(s, &service{})
    log.Panic(s.Serve(listen))
}

```

## 4-Client et API BiDi

Nous allons implémenter des goroutines pour simuler des connexions entrantes et sortantes. Il y a plusieurs choses à créer :

- un flux/stream en invoquant le client
- un envoi de X messages au serveur
- la réception de messages provenant du serveur.

Les opérations seront bloquées jusqu'à la fin des opérations

### 1-Rappel sur le codage du client

Etapes à suivre :

- appel de ```grpc.Dial()``` prenant en paramètre une chaîne pour l'adresse et le port et une option ```WithInsecure()``` pour l'adresse et le port du serveur , et retournant une connexion et une erreur
- gestion de l'erreur
- fermeture différée de la connexion
- création du client gRPC à l'aide de ```hellopb.NewHelloServiceClient()``` en passant en paramètre la connexion
- appel de ```HelloStreamBiDi()``` en lui passant seulement un ```context.Background()```. Cette méthode retourne un flux et une erreur
- gestion de l'erreur

### 3-Goroutine et channel

Avant de manipuler nos goroutines (il y en a deux : une pour l'envoi, une pour la réception), nous souhaitons passer ces messages dans un canal go traditionnel :

```go
waitc := make(chan struct{})
```

Cette utilisation est appelée *sémaphore*  (cf. Donovan & Kerninghan, The Go Programming Language, *Addison-Wesley*). Cette action est très simple : on crée un canal avec une structure vide, un booléen,... et le canal attend jusqu'à la fermeture du canal. Donc on "entoure" les goroutines comme suit :

```go
waitc := make(chan struct{})

// première go routine qui envoie les données
go func(){

}()

// seconde goroutine qui reçoit les données
go func(){

}()

// fermeture du canal : relâchement du blocage
<-waitc
```

Quelle goroutine cloturera le sémaphore ?

Ce sera la goroutine qui recevra les réponses du serveur (la seconde) : on attendra le renvoi toutes les réponses du serveur.

### 4-Envoi des messages

Pour envoyer nos messages, on crée une goroutine anonyme. Cette goroutine va envoyer des données factices dans une boucle et on simulera un temps de latence d'une seconde.

On va récupérer le code suivant pour générer un slice de ```*hellopb.HelloRequete``` :

```go
noms := [][]string{
    {"Ada", "Lovelace"},
    {"Alan", "Turing"},
    {"Brian", "Kernighan"},
    {"Dennis", "Ritchie"},
    {"Ken", "Thompson"},
    {"Rob", "Pike"},
    {"Robert", "Griesemer"},
}

lstNoms := []*hellopb.HelloRequete{}
for _, nom := range noms {
    req := &hellopb.HelloRequete{
        Hello: &hellopb.HelloMessage{
            Nom:    nom[1],
            Prenom: nom[0],
        },
    }
    lstNoms = append(lstNoms, req)
}
```

Que va faire la goroutine ?

Dans une boucle sur la liste de pointeur de requête, on envoie au serveur nos messages avec la méthode ```Send()``` issue du flux. On gèrera l'erreur en cassant la boucle seulement. Et on simulera une latence réseau. Et une fois que la boucle a achevé ses tours : on clôt le flux avec ```CloseSend()``` (méthode provenant de notre instance de flux)

```go
go func() {
    for _, req := range lstNoms {
        log.Printf("Envoi d'un message ... (%v)", req)
        if err := stream.Send(req); err != nil {
            log.Printf("Erreur sur l'envoi d'une requête : %v", err)
            break
        }
        // Simulation d'un temps de latence
        time.Sleep(500 * time.Millisecond)
    }
    // Fermeture de l'envoi
    if err := stream.CloseSend(); err != nil {
        log.Printf("Erreur à la fermeture de l'envoi : %v", err)
    }
}()
```

### 5-Réception des réponses

De notre flux nous allons appeler la méthode ```Recv()``` qui retourne un objet-réponse et une erreur.

Il faut **bien garder en mémoire** que l'on doit traiter l'erreur liée à l'**EOF** ! D'abord et en plus de toutes les autres erreurs.

De même que pour l'envoi nous devons boucler indéfiniment la réception. A chaque erreur, on "cassera" cette boucle infinie. Et une fois la boucle achevée, on fermera le canal.

```go
go func() {
    for {
        rep, err := stream.Recv()
        if err == io.EOF {
            log.Println("... Fin de transmission avec le serveur")
            break
        }
        if err != nil {
            log.Printf("Erreur à la réception d'un message  : %v", err)
            break
        }
        log.Printf("Message reçu : %s", rep.GetReponse())
    }
    close(waitc)
}()
```

### 6-Code complet du client

```go
package main

import (
    "api-bidi/hellopb"
    "context"
    "fmt"
    "io"
    "log"
    "time"

    "google.golang.org/grpc"
)

func main() {
    fmt.Println("Client - API Streaming Bidirectionnel")

    cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
    if err != nil {
        log.Fatalf("Erreur à la création de la connexion : %v", err)
    }
    defer cnx.Close()

    client := hellopb.NewHelloServiceClient(cnx)
    stream, err := client.HelloStreamBiDi(context.Background())
    if err != nil {
        log.Panicf("Erreur à la création du client : %v", err)
    }

    noms := [][]string{
        {"Ada", "Lovelace"},
        {"Alan", "Turing"},
        {"Brian", "Kernighan"},
        {"Dennis", "Ritchie"},
        {"Ken", "Thompson"},
        {"Rob", "Pike"},
        {"Robert", "Griesemer"},
    }

    lstNoms := []*hellopb.HelloRequete{}
    for _, nom := range noms {
        req := &hellopb.HelloRequete{
            Hello: &hellopb.HelloMessage{
                Nom:    nom[1],
                Prenom: nom[0],
            },
        }
        lstNoms = append(lstNoms, req)
    }

    waitc := make(chan struct{})

    // Goroutine gérant l'envoie de messages
    go func() {
        for _, req := range lstNoms {
            log.Printf("Envoi d'un message ... (%v)", req)
            if err := stream.Send(req); err != nil {
                log.Printf("Erreur sur l'envoi d'une requête : %v", err)
                break
            }
            // Simulation d'un temps de latence
            time.Sleep(500 * time.Millisecond)
        }
        // Fermeture de l'envoi
        if err := stream.CloseSend(); err != nil {
            log.Printf("Erreur à la fermeture de l'envoi : %v", err)
        }
    }()

    // Goroutine gérant la réception des messages provenant du serveur
    go func() {
        for {
            rep, err := stream.Recv()
            if err == io.EOF {
                log.Println("... Fin de transmission avec le serveur")
                break
            }
            if err != nil {
                log.Printf("Erreur à la réception d'un message  : %v", err)
                break
            }
            log.Printf("Message reçu : %s", rep.GetReponse())
        }
        close(waitc)
    }()

    <-waitc
}

```

[<- Précédent](6-Streaming-client.md) || [Suivant ->](8-Notions-avancees.md)
