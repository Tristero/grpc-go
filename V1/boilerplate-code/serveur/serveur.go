package main

import (
	"boilerplate-code/hellopb/hellopb"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"
)

type srv struct{}

func main() {
	fmt.Println("Premier serveur - boilerplate code")
	listener, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Erreur à l'écoute sur le port 50051 : %v", err)
	}

	s := grpc.NewServer()

	hellopb.RegisterHelloServiceServer(s, &srv{})

	if err := s.Serve(listener); err != nil {
		log.Fatalf("Echec serveur : %v", err)
	}
}
