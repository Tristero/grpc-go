package main

import (
	"boilerplate-code/hellopb/hellopb"
	"fmt"
	"log"

	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Client du projet boilerplate-code")

	cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Echec à la connexion : %v", err)
	}

	defer cnx.Close()

	client := hellopb.NewHelloServiceClient(cnx)
	fmt.Printf("Client créé : %f\n", client)
}
