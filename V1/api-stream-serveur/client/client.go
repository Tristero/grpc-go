package main

import (
	"api-stream-serveur/hellopb"
	"context"
	"fmt"
	"io"
	"log"

	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Client - API Stream Server")
	cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Erreur au lancement du client : %v", err)
	}
	defer cnx.Close()

	client := hellopb.NewHelloServiceClient(cnx)

	req := &hellopb.HelloRequete{
		Message: &hellopb.HelloMessage{
			Prenom: "John",
			Nom:    "Doe",
		},
	}

	rsltStream, err := client.HelloStreamServeur(
		context.Background(),
		req,
	)
	if err != nil {
		log.Panicf("Erreur sur l'envoi de la requête : %v", err)
	}

	for {
		msg, err := rsltStream.Recv()
		if err == io.EOF {
			log.Println("Fin d'envoi des données")
			break
		}
		if err != nil {
			log.Printf("Une erreur a eu lieu à la récupération d'une donnée : %v", err)
			break
		}

		fmt.Printf("[Réception donnée] : %v\n", msg.Reponse)
	}

}
