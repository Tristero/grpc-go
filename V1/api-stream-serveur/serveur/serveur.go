package main

import (
	"api-stream-serveur/hellopb"
	"fmt"
	"log"
	"net"
	"time"

	"google.golang.org/grpc"
)

type srvService struct{}

func (*srvService) HelloStreamServeur(req *hellopb.HelloRequete, stream hellopb.HelloService_HelloStreamServeurServer) error {
	log.Println("Appel de la méthode HelloStreamServer")

	nom, prenom := req.GetMessage().GetNom(), req.GetMessage().GetPrenom()
	// version alternative
	// nom, prenom := req.Message.Nom, req.Message.Prenom

	for i := 0; i < 10; i++ {
		rep := &hellopb.HelloReponse{
			Reponse: fmt.Sprintf("[Msg %d]Hello, %s %s", i, prenom, nom),
		}

		if err := stream.Send(rep); err != nil {
			return err
		}

		time.Sleep(1 * time.Second)
	}
	return nil
}

func main() {
	fmt.Println("Lancement du serveur - API stream server")

	lst, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Impossible de mettre à la disposition le port 50051 : %v", err)
	}

	defer lst.Close()

	srvGRPC := grpc.NewServer()
	hellopb.RegisterHelloServiceServer(srvGRPC, &srvService{})
	log.Panic(srvGRPC.Serve(lst))
}
