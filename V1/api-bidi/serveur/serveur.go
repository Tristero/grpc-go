package main

import (
	"api-bidi/hellopb"
	"fmt"
	"io"
	"log"
	"net"

	"google.golang.org/grpc"
)

type service struct{}

func (*service) HelloStreamBiDi(stream hellopb.HelloService_HelloStreamBiDiServer) error {

	log.Println("Appel de la méthode HelloStreamBiDi")

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			log.Printf("Erreur dans la réception d'une requête : %v", err)
			return err
		}

		str := fmt.Sprintf("Hello, %s %s !", req.GetHello().GetPrenom(), req.GetHello().GetNom())

		rep := &hellopb.HelloReponse{
			Reponse: str,
		}

		if err := stream.Send(rep); err != nil {
			log.Printf("Erreur à l'envoi d'une réponse : %v", err)
			return err
		}
	}
}

func main() {
	fmt.Println("Serveur - Api Stream Bidirectionnel")

	listen, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Erreur à la création du Listener : %v", err)
	}
	defer listen.Close()

	s := grpc.NewServer()
	hellopb.RegisterHelloServiceServer(s, &service{})
	log.Panic(s.Serve(listen))
}
