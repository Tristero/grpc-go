package main

import (
	"api-bidi/hellopb"
	"context"
	"fmt"
	"io"
	"log"
	"time"

	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Client - API Streaming Bidirectionnel")

	cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Erreur à la création de la connexion : %v", err)
	}
	defer cnx.Close()

	client := hellopb.NewHelloServiceClient(cnx)
	stream, err := client.HelloStreamBiDi(context.Background())
	if err != nil {
		log.Panicf("Erreur à la création du client : %v", err)
	}

	noms := [][]string{
		{"Ada", "Lovelace"},
		{"Alan", "Turing"},
		{"Brian", "Kernighan"},
		{"Dennis", "Ritchie"},
		{"Ken", "Thompson"},
		{"Rob", "Pike"},
		{"Robert", "Griesemer"},
	}

	lstNoms := []*hellopb.HelloRequete{}
	for _, nom := range noms {
		req := &hellopb.HelloRequete{
			Hello: &hellopb.HelloMessage{
				Nom:    nom[1],
				Prenom: nom[0],
			},
		}
		lstNoms = append(lstNoms, req)
	}

	waitc := make(chan struct{})

	// Goroutine gérant l'envoie de messages
	go func() {
		for _, req := range lstNoms {
			log.Printf("Envoi d'un message ... (%v)", req)
			if err := stream.Send(req); err != nil {
				log.Printf("Erreur sur l'envoi d'une requête : %v", err)
				break
			}
			// Simulation d'un temps de latence
			time.Sleep(500 * time.Millisecond)
		}
		// Fermeture de l'envoi
		if err := stream.CloseSend(); err != nil {
			log.Printf("Erreur à la fermeture de l'envoi : %v", err)
		}
	}()

	// Goroutine gérant la réception des messages provenant du serveur
	go func() {
		for {
			rep, err := stream.Recv()
			if err == io.EOF {
				log.Println("... Fin de transmission avec le serveur")
				break
			}
			if err != nil {
				log.Printf("Erreur à la réception d'un message  : %v", err)
				break
			}
			log.Printf("Message reçu : %s", rep.GetReponse())
		}
		close(waitc)
	}()

	<-waitc
}
