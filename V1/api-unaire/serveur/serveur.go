package main

import (
	hellopb "api-unaire/hellopb"
	"context"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"
)

type serveurService struct{}

func (*serveurService) Hello(ctx context.Context, in *hellopb.HelloRequete) (*hellopb.HelloReponse, error) {
	log.Printf("La fonction HelloRequete a été invoquée avec %v", in)
	// nous récupérons les valeurs associée à Prenom et Nom
	msg := fmt.Sprintf("Hello, %s %s", in.GetHello().Prenom, in.GetHello().Nom)
	// Version alternative
	//msg := fmt.Sprintf("Hello, %s %s", in.GetHello().GetPrenom, in.GetHello().GetNom)

	// Pour créer une réponse, on appelle simplement la structure publique
	// hellopb.Reponse{} et on affecte la chaîne au champ Reponse
	rep := hellopb.HelloReponse{
		Reponse: msg,
	}
	// Attention on retourne une référence !!!
	return &rep, nil
}

func main() {
	fmt.Println("Serveur - API unaire")

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Erreur à l'écoute sur le port 50051 : %v", err)
	}

	srvGRPC := grpc.NewServer()
	hellopb.RegisterHelloServiceServer(srvGRPC, &serveurService{})

	if err := srvGRPC.Serve(lis); err != nil {
		log.Fatalf("Erreur au lancement du serveur gRPC : %v", err)
	}

}
