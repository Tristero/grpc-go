package main

import (
	"api-unaire/hellopb"
	"context"
	"fmt"
	"log"

	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Client - API unaire")

	cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Erreur à l'ouverture de la connexion vers le serveur : %v", err)
	}
	defer cnx.Close()

	client := hellopb.NewHelloServiceClient(cnx)
	log.Printf("Client créé : %f", client)

	// Création de la requête et du message HelloMessage
	req := &hellopb.HelloRequete{
		Hello: &hellopb.HelloMessage{
			Prenom: "John",
			Nom:    "Doe",
		},
	}

	// Envoi de la requête
	rep, err := client.Hello(
		context.Background(),
		req,
	)

	if err != nil {
		// Attention au log.Fatal ! cf. Doc officielle : les defer ne sont pas pris en compte car os.Exit() termine immédiatement le programme.
		log.Panicf("Erreur sur le retour serveur : %v", err)
	}
	log.Printf("Message du serveur : %v", rep.Reponse)
}
