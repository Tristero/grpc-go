package main

import (
	"context"
	"erreurs/calculpb"
	"fmt"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func main() {
	fmt.Println("Client - calcul de racine carrée / gestion des erreurs")

	cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Erreur à la création de la connexion : %v", err)
	}
	defer cnx.Close()

	client := calculpb.NewRacineCarreServiceClient(cnx)

	var nbcorrect, nbincorrect int64 = 42, -42
	// appel correct
	calculRacineCarree(client, nbcorrect)

	// appel incorrect
	calculRacineCarree(client, nbincorrect)

}

func calculRacineCarree(client calculpb.RacineCarreServiceClient, nb int64) {
	rep, err := client.RacineCarre(context.Background(),
		&calculpb.RacineCarreReq{
			Nb: nb,
		})

	// Gestion des erreurs
	if err != nil {
		if s, ok := status.FromError(err); ok {
			// erreur utilisateur :
			// Affichage du contenu du pointeur ```status.Status```
			fmt.Printf("Message du serveur : %v\n", s.Message())
			fmt.Println(s.Code())
			// On traite notre erreur particulière InvalidArgument
			if s.Code() == codes.InvalidArgument {
				fmt.Println("Erreur : argument invalide. Le nombre passé en argument est négatif.")
				// on arrête ici
				return
			}
		} else {
			// erreur framework
			log.Fatalf("Erreur importante à l'appel de la fonction RacineCarre : %v\n", err)
			// on arrête ici
			return

		}
	}
	log.Printf("La racine carrée de 42 est %v", rep.GetReponse())
}
