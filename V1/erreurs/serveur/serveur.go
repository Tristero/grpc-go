package main

import (
	"context"
	"erreurs/calculpb"
	"fmt"
	"log"
	"math"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type service struct{}

func (*service) RacineCarre(ctx context.Context, req *calculpb.RacineCarreReq) (*calculpb.RacineCarreRep, error) {
	if req.GetNb() < 0 {
		return nil, status.Errorf(
			codes.InvalidArgument,
			"Le nombre reçu est négatif alors qu'il ne peut être que positif : %v", req.Nb,
		)
	}
	return &calculpb.RacineCarreRep{
		Reponse: math.Sqrt(float64(req.GetNb())),
	}, nil
}

func main() {
	fmt.Println("Serveur de calcul de racine carrée - Gestion des erreurs")

	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Erreur dans la création du Listener : %v", err)
	}
	defer l.Close()

	s := grpc.NewServer()

	calculpb.RegisterRacineCarreServiceServer(s, &service{})

	if err := s.Serve(l); err != nil {
		log.Panicf("Erreur au lancement du serveur : %v", err)
	}
}
