package main

import (
	"context"
	"exoApiUnaire/calcpb"
	"flag"
	"log"

	"google.golang.org/grpc"
)

var (
	flagX, flagY int64
)

func init() {
	flag.Int64Var(&flagX, "x", 0, "")
	flag.Int64Var(&flagY, "y", 0, "")
}

func main() {
	flag.Parse()

	log.Println("Lancement du client : somme de deux entiers")
	cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Erreur au lancement du client : %v", err)
	}

	defer cnx.Close()

	client := calcpb.NewSommeServiceClient(cnx)

	req := &calcpb.SommeRequete{
		Somme: &calcpb.SommeMessage{
			X: flagX,
			Y: flagY,
		},
	}

	rep, err := client.CalculSommeService(
		context.Background(),
		req,
	)
	if err != nil {
		log.Panicf("Erreur à l'envoi vers le serveur : %v", err)
	}

	log.Printf("Réponse du serveur :\nSomme de x (%d) et de y (%d) = %d", flagX, flagY, rep.Reponse)

}
