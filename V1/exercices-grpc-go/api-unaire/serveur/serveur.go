package main

import (
	"context"
	"exoApiUnaire/calcpb"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"
)

type serveurService struct{}

func (*serveurService) CalculSommeService(ctx context.Context, req *calcpb.SommeRequete) (*calcpb.SommeReponse, error) {
	log.Println("Appel de la méthode CalculSommeService")

	rslt := req.Somme.X + req.Somme.Y
	log.Printf("Avec \nX=%d\nY=%d\nRésultat calculé attendu par le serveur = %d",
		rslt,
		req.Somme.X,
		req.Somme.Y,
	)

	return &calcpb.SommeReponse{
		Reponse: rslt,
	}, nil
}

func main() {
	fmt.Println("Serveur d'addition - Api unaire")

	lst, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Erreur à l'ouverture du port : %v", err)
	}

	srvgrpc := grpc.NewServer()

	calcpb.RegisterSommeServiceServer(srvgrpc, &serveurService{})

	log.Fatal(srvgrpc.Serve(lst))
}
