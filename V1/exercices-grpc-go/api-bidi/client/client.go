package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"math/rand"
	"max/calculpb"
	"time"

	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Client BiDi - Find Max")

	cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Erreur à la création de la connexion avec le serveur : %v", err)
	}
	defer cnx.Close()

	client := calculpb.NewFindMaxServiceClient(cnx)
	stream, err := client.FindMax(context.Background())
	if err != nil {
		log.Panicf("Erreur à la création du flux : %v", err)
	}

	// création d'un sémaphore
	sema := make(chan struct{})

	// 1ère goroutine d'envoi des données
	go func() {
		for i := 0; i < 10; i++ {
			req := &calculpb.MaxRequete{
				Nb: genRand(),
			}
			if err := stream.Send(req); err != nil {
				log.Printf("Erreur à l'envoi d'une donnée : %v ", err)
				break
			}
			time.Sleep(500 * time.Millisecond)
		}
		if err := stream.CloseSend(); err != nil {
			log.Panicf("Erreur à la fin d'envoi des données : %v", err)
		}
	}()

	// 2nde goroutine de réception des données
	// une fois toutes les données reçues : on coupe le sémaphore
	go func() {
		slc := []int64{}
		for {
			rep, err := stream.Recv()
			if err == io.EOF {
				log.Printf("... Fin de réception des données. Résultats : %v", slc)
				break
			}
			if err != nil {
				log.Printf("Erreur à la récupération d'une donnée : %v", err)
				break
			}
			slc = append(slc, rep.GetNb())
			log.Printf("Réception d'une valeur : %d", rep.GetNb())
		}
		close(sema)
		if err := stream.CloseSend(); err != nil {
			log.Panicf("Erreur à la fin de réception des données : %v", err)
		}
	}()
	<-sema
}

func genRand() int64 {

	rand.Seed(time.Now().UnixNano())
	return rand.Int63n(1000)
}
