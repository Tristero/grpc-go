package main

import (
	"fmt"
	"io"
	"log"
	"max/calculpb"
	"net"

	"google.golang.org/grpc"
)

type service struct{}

func (*service) FindMax(stream calculpb.FindMaxService_FindMaxServer) error {

	var max int64

	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			log.Println("... Fin de réception et de transmission")
			return nil
		}
		if err != nil {
			log.Printf("Erreur à la réception d'une donnée : %v", err)
			return err
		}
		log.Printf("Réception de la valeur : %d", msg.GetNb())
		if msg.GetNb() > max {
			log.Printf("%d est supérieur à la dernière valeur stockée %d (réponse envoyée)", msg.GetNb(), max)
			max = msg.GetNb()

			if err := stream.Send(&calculpb.MaxReponse{
				Nb: max,
			}); err != nil {
				return err
			}
		}
	}

}

func main() {
	fmt.Println("Serveur BiDi - Recherche du maximum")
	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Erreur à la création du Listener : %v", err)
	}
	defer l.Close()

	sgrpc := grpc.NewServer()
	calculpb.RegisterFindMaxServiceServer(sgrpc, &service{})
	log.Panic(sgrpc.Serve(l))
}
