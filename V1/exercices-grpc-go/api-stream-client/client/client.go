package main

import (
	"api-stream-client/calculpb"
	"context"
	"fmt"
	"log"

	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Client - API Streaming Client")

	cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Erreur de connexion : %v", err)
	}
	defer cnx.Close()

	lst := []int64{1, 2, 3, 4, 5}

	client := calculpb.NewCalcMoyenneServiceClient(cnx)
	stream, err := client.Moyenne(context.Background())
	for i, nb := range lst {
		req := &calculpb.CalcRequete{
			Nb: nb,
		}
		if err := stream.Send(req); err != nil {
			log.Panicf("Erreur à l'envoi de la donnée : %v", err)
		}
		log.Printf("Envoi de la donnée n°%d = %d", i, nb)

	}

	rep, err := stream.CloseAndRecv()
	if err != nil {
		log.Printf("Erreur à la réception de la réponse : %v", err)
	} else {
		log.Printf("Réponse reçue du serveur : %v", rep.GetReponse())
	}
}
