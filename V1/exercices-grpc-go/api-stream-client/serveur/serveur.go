package main

import (
	"api-stream-client/calculpb"
	"fmt"
	"io"
	"log"
	"net"

	"google.golang.org/grpc"
)

type service struct{}

func (*service) Moyenne(stream calculpb.CalcMoyenneService_MoyenneServer) error {
	log.Println("Appel d'un calcul de moyenne")
	var somme int64 = 0

	i := 0
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Panicf("Erreur à la réception d'un message  : %v", err)
		}

		somme += req.GetNb()
		i++
	}

	rep := &calculpb.CalcReponse{
		Reponse: float32(somme) / float32(i),
	}

	return stream.SendAndClose(rep)
}

func main() {
	fmt.Println("Serveur - API Stream Client")

	lst, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Erreur à la création du Listener : %v", err)
	}
	defer lst.Close()

	s := grpc.NewServer()
	calculpb.RegisterCalcMoyenneServiceServer(s, &service{})
	log.Panic(s.Serve(lst))
}
