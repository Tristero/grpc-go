package main

import (
	"api-stream-serveur/premierpb"
	"context"
	"flag"
	"fmt"
	"io"
	"log"

	"google.golang.org/grpc"
)

var x int64

func init() {
	flag.Int64Var(&x, "nb", 0, "Donnez un nombre qui va être décomposé en nombre premier ")
}

func main() {

	flag.Parse()

	fmt.Println("Client - Calcul des nombres premiers")

	cnx, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Erreur à la création du client : %v", err)
	}

	defer cnx.Close()

	client := premierpb.NewPremierServiceClient(cnx)

	req := &premierpb.PremierRequete{
		Nb: x,
	}

	flux, err := client.CalculPremiers(context.Background(), req)
	if err != nil {
		log.Fatalf("Erreur à l'envoi de la requête : %v", err)
	}

	i := 1
	calcul := 1
	for {
		rslt, err := flux.Recv()
		if err == io.EOF {
			log.Println("** Fin de réception des données **")
			break
		}
		if err != nil {
			log.Panicf("Erreur à la réception des données : %v", err)
		}

		calcul *= int(rslt.GetRslt())
		log.Printf("[Réception du message n°%d] = %d\n", i, rslt.GetRslt())
		i++
	}
	fmt.Printf("Calcul du produit des nombres premiers = %d\n", calcul)
}
