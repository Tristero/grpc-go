package main

import (
	"api-stream-serveur/premierpb"
	"fmt"
	"log"
	"net"
	"time"

	"google.golang.org/grpc"
)

type service struct{}

// CalculPremiers décompose un nombre reçu en sous-nombres premiers
func (*service) CalculPremiers(req *premierpb.PremierRequete, stream premierpb.PremierService_CalculPremiersServer) error {
	log.Println("Début de la transaction")

	nb := req.GetNb()
	var k int64 = 2

	for nb > 1 {
		if (nb % k) == 0 {
			fmt.Println(k)
			reponse := &premierpb.PremierReponse{
				Rslt: k,
			}
			if err := stream.Send(reponse); err != nil {
				return err
			}
			nb = nb / k
		} else {
			k = k + 1
		}

		time.Sleep(50 * time.Millisecond)
	}
	log.Println("Fin de la transaction")
	return nil
}

func main() {
	fmt.Println("Serveur de calcul des nombres premiers")

	lst, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Erreur à la création du serveur : %v", err)
	}

	defer lst.Close()

	server := grpc.NewServer()
	premierpb.RegisterPremierServiceServer(server, &service{})

	log.Panic(server.Serve(lst))
}
