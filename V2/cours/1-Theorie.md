# Théorie : mécanismes internes de gRPC

## 1-Protocol Buffer et interopérabilité

### 1-Protocol Buffer

gRPC repose sur un autre protocole : Protocol Buffer. C'est un simple fichier portant l'extension `.proto`.

```proto
syntax = "proto3";

message Greeting {
    string first_name = 1;
}

message GreetRequest {
    Greeting greeting = 1;
}

message GreetResponse {
    string result = 1;
}

service GreetService {
    rpc Greet(GreetRequest) returns (GreetResponse) {}
}
```

On définit des messages (ici on en a 3) et des services (un seul). Le service définit les échanges (ce qui est demandé et ce qui sera retourné) : c'est un *endpoint*. Les messages sont simples : ils indiquent ce qui est contenu (en réception et/ou en en expédition). Ce sont des conteneurs.

De ce message, le système va générer du code à notre place

### 2-JSON vs gRPC

Un JSON comme :

```json
{
    "age": 26,
    "first_name": "Clement",
    "last_name": "JEAN"
}
```

semble léger (52 octets, une fois compressée) mais si on compare à ProtoBuf :

```proto
syntax = "proto3";

message Person {
    uint32 age = 1;
    string first_name = 2;
    string last_name = 3;
}
```

Le même message ne pèse plus que 17 octets.

Si JSON est intéressant c'est parce qu'un humain peut le lire mais pour un ordinateur :

- le parsing est consommateur en ressource processeur
- rien n'assure que le type de donnée transmis est respecté

En revanche Protocol Buffer :

- est moins gourmand en ressource
- conserve les types
- permet un traitement plus rapide des communications
- permet la création des message de façon plutôt intuitive

Un autre intérêt concerne les appareils nomades et microcontroleurs : moins de consommation CPU, mémoire signifie moins de consommation énergétique.

### 3-Langages de programmation

PB et gRPC disposent d'interface pour les gros langages (Java, C#, C) mais aussi Dart, Go, Kotlin... et même des plateformes telles que NodeJS, et Flutter. La liste complète des langages supportés est [https://grpc.io/docs/languages/](https://grpc.io/docs/languages/)

Mais il existe d'autres supports non-officiels qui permettent d'utiliser gRPC. Par exemple, Apple apporte sa propre implémentation pour Swift.

En fait seulement 4 langages disposent d'une implémentation pure (c'est-à-dire sans faire appel à un code d'un autre langage tel que le C) :

- Java
- Go
- C
- C#

Python, C++, ... reposent sur l'implémentation C.

Entre ces 4 langages disposent de différences : par exemple, C# intégre HTTP/3 mais pas en C (donc tous les autres langages qui en dépendent ne disposent pas non plus de cette fonctionnalité).

Donc il convient de faire attention à ces différences d'implémentation.

## 2-HTTP/2

gRPC est intrinsèquement lié à l'utilisation de HTTP/2. La version 1 ne permet qu'une communication par requête. On ouvre une communication avec le serveur, on effectue une requête puis on attend la réponse et enfin on clôture la connexion. Avec une page web avec plusieurs dizaines de médias (audio, vidéos, images), il y aura autant de requêtes, dont autant de processus pour récupérer ses différents éléments (autant d'ouverture et de fermeture) !

Autre problème : les en-têtes ne sont pas compressés (texte). Donc on aura une charge plus importante et *in fine*, une latence plus importante

Enfin nous avons ces problèmes de requêtes/réponses en chaîne, comme nous l'avons plus haut.

Il faut bien voir qu'un client demandant un accès à un page web avec un serveur HTTP/1 va ouvrir une connexion TCP pour chaque fichier requis (CSS, HTML, JS, images, ...). On aura un processus par requête d'ouverture et de fermeture de connexion.

HTTP/2 propose des solutions à ces trois problèmes avec :

- une seule connexion partagées par toutes les requêtes et réponses (meilleure efficacité)
- un système de push côté serveur : un serveur peut *push*er plusieurs messages à partir d'une seule requête. Le client n'a plus besoin d'effectuer de requêtes supplémentaires au serveur : il n'a qu'à attendre le serveur
- le multiplexing : client et serveur peuvent échanger des requêtes et réponses en parallèle au sein d'une même connexion TCP. Ce qui est plus rapide donc moins de latence
- les entêtes ne sont plus au format texte mais sont au format binaire
- la sécurisation des échanges : une connexion sécurisée est dorénavant requise par défaut.

Donc HTTP/2 est moins "verbeux" dans les échanges client-serveur donc consomme moins de bande passante. Il propose une meilleure sécurisation des échanges.

gRPC intègre tout cela.

## 3-Différents d'APIs gRPC

Il existe quatre types d'APIs avec gRPC :

- **Unaire** : c'est le plus familier car le plus proche des APIs REST. Le client envoie une requête et le serveur lui répond avec une réponse.
- **Flux serveur** ou **Server stream** : valable uniquement avec HHTP/2. Le client envoie une requête et le serveur lui répond avec un flux de messages (1 message ou plus). Le cas de figure d'utilisation est la mise à jour d'information (comme un cours boursier)
- **Flux client** ou **Client stream** : le client envoie un flux de messages (1 ou plusieurs) et le serveur ne retournera qu'une seule réponse. C'est le cas pour l'uploading (téléversement) de fichiers vers un serveur.
- **Flux bidirectionnel** ou **Bi-directional stream** : client et serveur s'échangent requêtes et réponses multiples. Et les réponses peuvent être envoyées quand le serveur le décide : il n'est pas obligé d'attendre la fin de l'envoi des requêtes pour répondre

La syntaxe ProtoBuf :

```proto
service GreetService {

    // Unaire
    rpc Greet(GreetRequest) returns (GreetResponse) {};

    // Flux serveur
    rpc GreetManyTimes(GreetRequest) returns (stream GreetResponse) {};

    // Flux client
    rpc LongGreet(stream GreetRequest) returns (GreetResponse) {};

    // Flux bidirectionnels
    rpc GreetEveryone(stream GreetRequest) returns (stream GreetResponse) {};
}
```

Pour la gestion des flux, nous devons utiliser le mot-clé `stream` avant le message.

## 4-Scalabilité et sécurité

### 1-La scalabilité de gRPC : quelle capacité de gestion ?

Cet anglicisme signifie mise à l'échelle : quelle est la capacité de la montée en puissance de gRPC ?

Tout d'abord, il faut savoir comment sont gérées les requêtes et les réponses. Côté serveur tout est asynchrone : il peut gérer plusieurs requêtes en même temps. En revanche, côté client, on a le choix entre une approche bloquante ou asynchrone.

La scalabilité promue par Google est de l'ordre de 10 milliards de requête par seconde au total sur leurs serveurs !

### 2- La sécurité

Celle-ci est assurée à plusieurs niveaux :

- au niveau du schéma de sérialisation : les données sont des binaires (non lisibles par des humaines)
- utilisation de certificats SSL pour le chiffrement dans les échanges : avec une initialisation d'une connexion avec TLS entre le client et le serveur est aisée
- utilisation (optionnelle) d'intercepteurs : ces intercepteurs seront très utiles pour l'authentification (ce sera abordé dans la partie avancée du cours).

## 5-REST vs gRPC

REST :

- utilise JSON (lisible, flexibilité)
- HTTP/1 et HTTP/2
- support d'échanges unidirectionnels
- échanges du client vers le serveur
- verbes prédéfinis : GET, POST, ...

gRPC

- utilise Protocol Buffer (léger, respect des types, schéma fixé)
- HTTP/2 obligatoire
- support du flux de données
- support des communications bidirectionnelles
- aucun verbe prédéfini : conception libre des APIs

gRPC dispose de très nombreux avantages (sécurité, performances, schéma script, APIs libres dans leur conception) mais il est peu développé face à l'omniprésence des APIs REST. Mais gRPC peut totalement remplacé REST grâce à son support des communications unaires.