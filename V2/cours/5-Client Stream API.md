# Server Stream API

ou API de flux client

L'objet est de créer avec une mutlitude de requêtes client (un flux de données), une seule réponse du serveur.

**Pré-requis** :

Pour notre présentation nous allons créer un fichier go.mod portant en tant que nom de module "api-client-stream". Et nous créerons nos trois répertoires :

- server
- client
- proto

## 1-Service Protocol Buffer

Définissons le fichier Protocol Buffer pour que le client envoie des données en flux. Pour cela nous utilisons le mot-clé `stream` :

```proto
syntax = "proto3";

package greet;

option go_package="api-client-stream/proto";

message GreetRequest {
    string first_name = 1;
}

message GreetResponse {
    string result = 1;
}

service GreetService {
    rpc LongGreetRequest (stream GreetRequest) returns (GreetResponse);
}
```

Compilons avec la commande :

```sh
protoc -Iproto --go_out=. --go_opt=module=api-client-stream --go-grpc_out=. --go-grpc_opt=module=api-client-stream proto/*.proto
```

Puis on appelle `go mod tidy` pour intégrer les dépendances au module.

## 2-Implémentation du serveur

Comme précédemment, nous reprenons un code de base :

```go
package main

import (
    "log"
    "net"

    pb "api-client-stream/proto"

    "google.golang.org/grpc"
)

const IP = "0.0.0.0:50051"

type Server struct {
    pb.GreetServiceServer
}

func main() {
    log.Println("Lancement du serveur")

    lst, err := net.Listen("tcp", IP)
    if err != nil {
        log.Printf("Erreur à l'écoute sur %v", IP)
        return
    }
    defer lst.Close()

    s := grpc.NewServer()
    pb.RegisterGreetServiceServer(s, &Server{})
    if err := s.Serve(lst); err != nil {
        log.Printf("Une erreur est survenue dans le lancement du serveur : %v", err)
        return
    }
}
```

Maintenant nous allons gérer la réception des messages du client. Pour cela nous devons connaître la signature pour créer la méthode côté serveur. On trouvera la signature suivante : `LongGreetRequest(GreetService_LongGreetRequestServer) error`.

Le comportement sera le même que le code client dans l'API de streaming côté serveur.

Puisque nous ne connaissons pas à l'avance le nombre d'itération, on utilisera une boucle infinie. Mais nous n'allons pas afficher tous les messages reçus mais seulement le nombre de messages reçus.

Pour recevoir chaque message, nous devons le faire dans la boucle for et utilisr la méthode `Recv()` fournie par le type `GreetService_LongGreetRequestServer`.

Mais contrairement au client, il faut du côté du serveur arrêter le flux et envoyer un message au client avec un résultat :

```go
func (s Server) LongGreetRequest(stream pb.GreetService_LongGreetRequestServer) error {
    log.Println("Appel de la méthode LongGreetRequest")

    i := 0
    for {
        _, err := stream.Recv()
        
        if err == io.EOF {
            log.Println("Fin de réception des données")
            return stream.SendAndClose(&pb.GreetResponse{
                Result: fmt.Sprintf("%d messages reçus par le serveur : ", i),
            })
        }

        if err != nil {
            log.Printf("Une erreur inattendue est survenue : %v", err)
            break
        }

        i++
    }

    log.Printf("Nombre de messages reçus : %d", i)
    return nil
}
```

Lorsque le serveur ne reçoit plus de données, on appelle la méthode `SendAndClose()` qui retourne un message vers le client et coupe la connexion. Ce message sera récupéré par le client (nous l'utiliserons en l'affichant dans le shell) avec une autre méthode `CloseAndRecv`.

On peut lancer notre code pour vérifier si celui-ci compile.

## 3-Implémentation du client

Reprenons un code de base :

```go
package main

import (
    "context"
    "log"

    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials/insecure"

    pb "api-client-stream/proto"
)

const IP = "localhost:50051"

func main() {

    log.Println("Lancement du client")
    cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
    if err != nil {
        log.Printf("Erreur à la création du client : %v", err)
        return
    }
    defer cnx.Close()

    client := pb.NewGreetServiceClient(cnx)

}
```

Maintenant ce que nous souhaitons faire c'est envoyer des données sans aucune valeur à travers une boucle. La méthode générée pour nous `LongGreetRequest()` retourne deux choses : un type `pb.GreetService_LongGreetRequestClient` et une erreur.

Une fois les données envoyées, notre client va récupérer les données via l'appel de `CloseAndRecv()` : ici on ferme la connexion et on attend le retour du serveur :

```go
func main() {

    log.Println("Lancement du client")
    cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
    if err != nil {
        log.Printf("Erreur à la création du client : %v", err)
        return
    }
    defer cnx.Close()

    client := pb.NewGreetServiceClient(cnx)

    log.Println("Envoi des données...")
    stream, err := client.LongGreetRequest(context.Background())

    if err != nil {
        log.Printf("Une erreur est survenue dans le lancement du stream : %v", err)
    }

    for i := 0; i < 5; i++ {
        req := &pb.GreetRequest{
            FirstName: strconv.Itoa(i),
        }

        log.Printf("Envoi de la requête : %+v", req)

        if err := stream.SendMsg(req); err != nil {
            log.Printf("Erreur à la réception des données : %v", err)
        }

        // mise en place d'une pause.
        time.Sleep(1 * time.Second)
    }

    rep, err := stream.CloseAndRecv()
    if err != nil {
        log.Printf("Une erreur inattendue est survenue à la fermeture de la connexion : %v", err)
    }
    log.Printf("Réponse du serveur : %v", rep)
}
```
