# Server Stream API

ou API de flux serveur

L'objet est de créer avec une seule requête client, un flux de données en retour généré par le serveur.

**Pré-requis** :

Pour notre présentation nous allons créer un fichier go.mod portant en tant que nom de module "api-server-stream". Et nous créerons nos trois répertoires :

- server
- client
- proto

## 1-Service Protocol Buffer

Pour mettre en place notre service de streaming côté serveur. Pour cela nous avons vu comment retourner une seule et unique réponse. gRPC nous propose un mot-clé pour exprimer que le résultat retourné sera un flux : `stream`

```proto
syntax = "proto3";

package greet;

option go_package="api-server-stream/proto";

message GreetRequest {
    string first_name = 1;
}

message GreetResponse {
    string result = 1;
}

service GreetService {
    rpc GreetManyTimes (GreetRequest) returns (stream GreetResponse);
}
```

Maintenant générons le code Go :

```sh
protoc -Iproto --go_out=. --go_opt=module=api-server-stream --go-grpc_out=. --go-grpc_opt=module=api-server-stream proto/*.proto
```

Une fois la génération effectuée, passons un `go mod tidy` pour récupérer les bonnes dépendances.

## 2-Implémentation du serveur

On reprend le code de départ :

```go
package main

import (
    "log"
    "net"

    pb "api-server-stream/proto"

    "google.golang.org/grpc"
)

const IP = "0.0.0.0:50051"

type Server struct {
    pb.GreetServiceServer
}

func main() {

    lst, err := net.Listen("tcp", IP)
    if err != nil {
        log.Fatalf("Impossible d'écouter sur %v - %v", IP, err)
    }
    defer lst.Close()

    s := grpc.NewServer()
    pb.RegisterGreetServiceServer(s, &Server{})
    if err := s.Serve(lst); err != nil {
        log.Fatalf("Erreur est survenue au lancement du serveur : %v", err)
    }
}
```

Nous devons définir une méthode pour gérer le flux. Pour cela nous devons fouiller le code de `greet_grpc.pb.go` à la recherche de l'interface `GreetServiceServer`. Celle-ci contient la définition utile à implémenter pour notre serveur.

```go
type GreetServiceServer interface {
    GreetManyTimes(*GreetRequest, GreetService_GreetManyTimesServer) error
    mustEmbedUnimplementedGreetServiceServer()
}
```

On récupère la première définition et implémentons la méthode sur notre structure `Server`. On remarquera que la signature diffère de la méthode unaire : il n'y a plus qu'un seul retour (une erreur) mais que l'on a DEUX arguments : la requête et un serveur. C'est via ce serveur que l'on va retourner un flux de données.

```go
func (s Server) GreetManyTimes(req *pb.GreetRequest, stream pb.GreetService_GreetManyTimesServer) error {
    log.Printf("Appel de la fonction GreetManyTimes avec %v+", req)
    
    return nil
}
```

Pour générer un flux nous allons créer une boucle. Et dans cette boucle nous allons appeler la méthode `Send()`. Cette méthode est fournie par le type `GreetService_GreetManyTimesServer` et elle prend en paramètre un type `*pb.GreetResponse`.

Le code est très simple et se comprend sans grande peine :

```go
func (s Server) GreetManyTimes(req *pb.GreetRequest, stream pb.GreetService_GreetManyTimesServer) error {
    log.Printf("Appel de la fonction GreetManyTimes avec %v+", req)
    for i := 0; i < 10; i++ {
        // simulation d'un temps de latence
        time.Sleep(1 * time.Second)

        rslt := fmt.Sprintf("Résultat n°%d - hello %s", i+1, req.GetFirstName())
        stream.Send(&pb.GreetResponse{
            Result: rslt,
        })
    }
    return nil
}
```

**NB** : nous sommes permis d'ajouter un temps de latence d'une seconde entre chaque envoi de données.

Tout notre système est en place. La requête sera récupérer par le serveur et transmis au type `Server`. Le code Go généré nous fournit aussi le serveur de flux de données. Nous n'avons qu'à nous focaliser sur la logique du code.

## 3-Implémentation du client

Reprenons un code de base :

```go
package main

import (
    "log"

    "google.golang.org/grpc"

    pb "api-server-stream/proto"

)

const IP = "localhost:50051"

func main() {
    cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
    if err != nil {
        log.Fatalf("Erreur à la création d'une connexion : %v", err)
    }
    defer cnx.Close()

    client := pb.NewGreetServiceClient(cnx)
}
```

à la différence d'un API unaire simple, il va nous falloir gérer l'arrêt de l'envoi de données. Pour cela nous allons gérer les erreurs. Car c'est là que se trouve la solution à l'arrêt de l'écoute.

Pour cela nous allons simplement générer une boucle infinie et ce sont les erreurs que nous rencontrerons qui stopperont la boucle infinie. Dans notre cas, nous allons surveillée l'erreur io.EOF nous indiquant que tout est bien reçu et que nous ne devons pas attendre d'autres données, d'une part et d'autre part toutes les autres seront gérées normalement :

```go
func main() {
    cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
    if err != nil {
        log.Fatalf("Erreur à la création d'une connexion : %v", err)
    }
    defer cnx.Close()

    client := pb.NewGreetServiceClient(cnx)
    log.Println("Récupération des données envoyées en flux")
    stream, err := client.GreetManyTimes(context.Background(), &pb.GreetRequest{
        FirstName: "Alice",
    })

    if err != nil {
        log.Printf("Erreur à l'appel de la méthode GreetManyTimes : %v", err)
        return
    }

    for {
        rep, err := stream.Recv()

        // fin de transmission
        if err == io.EOF {
            log.Println("Fin de réception des données")
            break
        }

        // en cas d'erreur autre que la fin de réception de données
        if err != nil {
            log.Printf("Une erreur inconnue a été levée : %v", err)
            break
        }

        // réception des données
        log.Printf("Réponse reçue : %s", rep.GetResult())
    }
}
```

NB : pourquoi ne pas utiliser `log.Fatalf` ou `log.Fatal` ? Il vaut mieux éviter autant que possible ces méthodes lorsque nous utilisons des méthodes avec `defer`. `Fatal` et `Fatalf` inhibe le comportement de toutes les méthodes différées.

Pour tester on lancera côte-à-côte deux terminaux :

- pour le serveur
- pour le client

Ce qui nous permettra d'oberserver le comportement du client et du serveur en simultanée.
