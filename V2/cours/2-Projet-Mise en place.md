# Mise en place du projet

Ici est abordé la mise en place

- Aller sur le site gRPC pour connaître les prérequis à l'utilisation de gRPC : [https://grpc.io/docs/languages/go/quickstart/](https://grpc.io/docs/languages/go/quickstart/)

Résumé :

- récupérer l'une des 3 dernières versions majeures de Go
- récupérer et installer un compilateur `protoc` pour [Protocol Buffer v3](https://developers.google.com/protocol-buffers/docs/proto3) : [https://grpc.io/docs/protoc-installation/](https://grpc.io/docs/protoc-installation/)

**ATTENTION** : l'utilisation des gestionnaires de package comme APT avec Ubuntu ne nous donnera pas la toute dernière version de gRPC. Si on le souhaite, il suffira de récupérer l'archive contenant les derniers binaires sur Github et installer l'archive là où on le souhaite.

Nous allons à partir d'ici modifier la suite des opérations :

- installer un éditeur : VSCode par exemple.
- Pour VSCode, on utilisera l'extenson `vscode-proto3`
- pour nos projets, on se crée un répertoire `codes` dans lequel on créera nos modules
- il faudra installer les plugins `protoc-gen-go` et `protoc-gen-go-grpc` (la méthode est globale mais nous installerons ces éléments localement : 1 projet = 1 module, 1 expérience = 1 module, etc.)

## 1-Présentation

### 1-Architecture des répertoires

Dans notre répertoire `codes`, créons

- un répertoire `apiunaire`
- dans ce répertoire on se crée :
  - un répertoire `server`
  - un répertoire `client`
  - un répertoire `proto`
- dans `apiunaire`, on initialise un module `apiunaire`
- on ajoute nos dépendances :

```txt
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
```

### 2-Création d'un fichier Protocol Buffer

Dans le répertoire `proto`, nous allons créer un nouveau fichier : `dummy.proto`

```proto
syntax = "proto3";

package greet;

option go_package = "apiunaire/greet/proto";

message Dummy {
    uint32 id = 1;
}

service DummyService {}
```

L'emploi de Go oblige de préciser un package utilisant le nom du module suivi du chemin vers le répertoire contenant les fichiers `proto`

Nous avons défini deux choses :

- un message qui ne contient qu'un entier positif ayant pour nom `id`
- un service qui ne fait rien

Il convient de respecter le format CamelCase pour les noms de message et de service.

Ce fichier est le fichier de base, le plus simple qui soit sur lequel repose tous les autres fichiers.

### 3-Compilation

#### 1-Ligne de commande

Manuellement, dans un terminal, à la racine de notre projet nous allons appeler le compilateur avec une série d'options

```sh
protoc -Iproto --go_out=.. --go-grpc_out=.. proto/dummy.proto  
```

- `-I` indique dans quel répertoire se trouvent les fichiers à importer (**attention** : pas d'espace entre le flag et l'argument passé)
- `--go_out` requiert le chemin vers lequel on veut exporter le code généré en Go pour Protocol Buffer (on pointe sur le répertoire racine : si on utilisait `.`, `protoc` considèrerait le répertoire courant pour créer une sous-arborescence `apiunaire/apiunaire/greet/proto` or nous voulons que cela soit `apiunaire/greet/proto` )
- `--go-grpc_out` procède exactement de la même manière. On précise où le code spécifique à gRPC doit être généré.
- chemin vers le fichier ̀ proto` avec le nom du fichier pointé

**Résultat** : on obtient un nouvel ensemble de sous-répertoires : `./greet/proto/` contenant deux nouveaux fichiers Go générés pour nous.

**Remarque** : le nom du sous-répertoire `greet` vient du nom du package dans le fichier `dummy.proto`.

Il est possible d'utiliser une autre méthode plus longue basée sur les noms de module complexe.

#### 2-Nom de module complexe

Si on utilise un nom de module complexe, comme par exemple : `github.com/machin/apiunaire`

Notre fichier `dummy.proto` ressemblera à :

```proto
syntax = "proto3";

package greet;

option go_package = "github.com/machin/apiunaire/greet/proto";

message Dummy {
    uint32 id = 1;
}

service DummyService {}
```

Si on rédige notre ligne de commande comme précédemment on va se retrouver avec un code généré au mauvais endroit. Pour régler ce problème, nous devons rajouter deux flags supplémentaires : `--go-opt=module=` et `--go-grpc_opt=module=`

Ces deux flags prennent en argument le nom du module. Cela va indiquer au compilateur  qu'il ne faut pas en tenir compte du chemin (le nom du module finalement) lors de la génération des fichiers Go :

```sh
protoc -Iproto --go_out=. --go_opt=module=github.com/machin/apiunaire --go-grpc_out=. --go-grpc_opt=module=github.com/machin/apiunaire proto/dummy.proto
```

**NB** : nous n'avons pas besoin de pointer le répertoire supérieur dans les flags out mais le répertoire courant.

Cela fonctionne pareillement avec notre précédent nom :

```sh
protoc -Iproto --go_out=. --go_opt=module=apiunaire --go-grpc_out=. --go-grpc_opt=module=apiunaire proto/dummy.proto
```

C'est plus long mais on s'assure que tout est installé au bon endroit.

---

On peut automatiser la compilation du code avec un fichier Markefile. Exemple : [https://github.com/Clement-Jean/grpc-go-course/blob/master/Makefile](https://github.com/Clement-Jean/grpc-go-course/blob/master/Makefile)

Nous préférons taper ces lignes pour bien les comprendre et les intégrer en mémoire.

Si tout a été correctement généré, nous pouvons supprimer les fichiers générés en Go et le répertoire `greet` ainsi que le `dummy.proto`.

## 2-Codage

### 1-Création du fichier proto

Pour notre projet, créons dans `./proto` un fichier `greet.proto`. Nous allons créer deux messages :

- un pour la requête : on envoie un prénom
- un pour la réponse : le serveur répondra une salutation

Puis nous créerons un service RPC qui va prendre une requête et retournera une réponse :

```proto
syntax = "proto3";

package greet;

option go_package="apiunaire/proto";

message GreetRequest {
    string first_name =1;
}

message GreetResponse {
    string result = 1;
}

service GreetService {
    rpc Greet (GreetRequest) returns (GreetResponse);
}
```

On génère : `protoc -Iproto --go_out=. --go_opt=module=apiunaire --go-grpc_out=. --go-grpc_opt=module=apiunaire proto/*.proto`

Maintenant nous avons deux fichiers Go générés pour nous par le compilateur dans le répertoire `greet`

### 2-Codage du serveur

on se crée un fichier `main.go`. Dans ce fichier, on se crée une constante correspondant à l'adresse du serveur.

Le serveur va devoir écouter l'adresse (constante) via la fonction `Listen()` du package `net` et on va gérer l'erreur ainsi que la fermeture différée du port :

```go
package main

import (
    "log"
    "net"
)

const addr = "0.0.0.0:50051"

func main() {
    lst, err := net.Listen("tcp", addr)
    if err != nil {
        log.Fatalf("Erreur à l'écoute du port TCP [%s] :%v\n", addr, err)
    }
    defer lst.Close()

    log.Printf("Ecoute sur %s\n", addr)
}
```

Maintenant nous allons nous créer un serveur gRPC. Pour cela, nous devons appeler `NewServer` de la bibliothèque pour créer un nouveau serveur. Cela nous retournera un pointeur. De ce pointeur nous lancerons le serveur en lui passant en argument notre objet Listener :

```go
package main

import (
    "log"
    "net"

    "google.golang.org/grpc"
)

const addr = "0.0.0.0:50051"

func main() {
    lst, err := net.Listen("tcp", addr)
    if err != nil {
        log.Fatalf("Erreur à l'écoute du port TCP [%s] :%v\n", addr, err)
    }
    defer lst.Close()

    log.Printf("Ecoute sur %s\n", addr)

    s := grpc.NewServer()

    if err := s.Serve(lst); err != nil {
        log.Fatalf("Echec au lancement du serveur : %v", err)
    }

}
```

En cas de problème : il se peut que des bibliothèques manquent alors à la console pointant la racine du projet, on tape `go mod tidy`.

Le code est normalement fonctionnel. Et nous avons le code de base pour tous les codes à venir pour créer un serveur.

### 3-Codage du client

Dans notre répertoire `client`, créons un fichier `main.go`.

Nous allons créer une constante pour l'adresse du serveur en localhost pointant sur le bon port : `const addr = "localhost:50051"`

Pour communiquer avec le serveur nous devons appeler le package `gprc` de Google et la fonction `Dial`. Celle-ci prend en paramètre une string correspondant à l'adresse et au port de notre serveur et retourne un pointeur de connexion et une erreur : `cnx, err := grpc.Dial(addr)`. Il est à noter que cette fonction prend d'autres options de configuration que nous allons voir.

Puis on gère l'erreur et on diffère la fermeture de la connexion.

```go
package main

import (
    "log"

    "google.golang.org/grpc"
)

const addr = "localhost:50051"

func main() {
    cnx, err := grpc.Dial(addr)
    if err != nil {
        log.Fatalf("Erreur à la connexion vers le serveur : %v\n", err)
    }
    defer cnx.Close()
}
```

À partir de ce code, nous pouvons tester : si tout se passe bien, nous ne devrions pas avoir d'erreur. Pour se faire, il faudra deux terminaux et on lancera dans l'ordre les codes :

- un pointant le répertoire `server`, et on lance le serveur
- un pointant le répertoire `client`, et on lance le client

Si on lance le serveur : pas d'erreur.

Si on lance le client : nous avons une erreur `no transport security set`. Heureusement l'erreur nous retourne une solution : indiquer que la connexion n'est pas sécurisée explicitement.

Pour cela nous devons placer cette solution en tant que paramètre supplémentaire dans `Dial()` : `cnx, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))`

Nous verrons dans la partie avancée comment mettre en place une connexion sécurisée.
