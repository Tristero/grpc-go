# Opérations CRUD avec MongoDB

Pré-requis :

il faut installer les logiciels suivants :

- Docker
- Docker compose

L'objectif est d'utiliser Docker avec une base de données MongoDB

## 1-Mise en place du projet

Dans un répertoire blog, nous créons le fichier docker-compose.yml et ajoutons le contenu suivant :

```yml
version: '3.1'

services:
  mongo:
    image: mongo
    restart: always
    ports:
      - 27017:27017
    environment:
      MONGO_INITDB_ROOT_USERNAME: root
      MONGO_INITDB_ROOT_PASSWORD: root

  mongo-express:
    image: mongo-express
    restart: always
    ports:
      - 8081:8081
    environment:
      ME_CONFIG_MONGODB_ADMINUSERNAME: root
      ME_CONFIG_MONGODB_ADMINPASSWORD: root
      ME_CONFIG_MONGODB_URL: mongodb://root:root@mongo:27017/
```

ce que ce fichier fait c'est de créer deux services : un pour MongoDB et un pour un serveur Web (utilisant NodeJS/Express) que l'on pourra accéder via le port 8081.

Sont définis les éléments de connexion au service pour MongoDB ainsi que les ports que l'on interrogera.

Une fois le contenu recopié dans le fichier, on ouvrira un terminal et lancer Docker-Compose : `docker-compose up`

Cela va lancer les services et affichera un grand nombre de log. Une fois tout chargé, il suffira de lancer un navigateur internet : [http://localhost:8081](http://localhost:8081)

## 2-Préparation du projet

### 1-Driver Go

Tou d'abord il faut télécharger le pilote officiel de MongoDB pour Go ([documentation officielle](https://www.mongodb.com/docs/drivers/go/current/)) :

```sh
go get go.mongodb.org/mongo-driver/mongo
```

### 2-Protocol Buffer

à la racine du projet, on initialise un module Go : `go mod init blog`

On crée un premier répertoire `proto`, dans lequel on ajoute un fichier `blog.proto` présentant le code suivant :

```proto
syntax = "proto3";

package blog;

option go_package="blog/proto";

import "google/protobuf/empty.proto";

message Blog {
    string id = 1;
    string author_id = 2;
    string title = 3;
    string content = 4;
}

message BlogId {
    string id = 1;
}

service BlogService {
    rpc CreateBlog (Blog) returns (BlogId);
    rpc ReadBlog (BlogId) returns (Blog);
    rpc UpdateBlog (Blog) returns (google.protobuf.Empty);
    rpc DeleteBlog (BlogId) returns (google.protobuf.Empty);
    rpc ListBlogs (google.protobuf.Empty) returns (stream Blog);
}
```

Quelques remarques :

- nous créons deux messages :
  - un pour créer un post pour un blog
  - un pour gérer (récupérer ou envoyer) un post selon l'ID du post
- nous avons créer 5 services : CRUD et un dernier nous récupérant la liste complète des blogs
- nous utilisons un type particulier `Empty` qui est proposé par Google : c'est un objet particulier qui indique que le message que l'on veut envoyer ou recevoir ne fait rien, ne contient rien. Si on observe `ListBlogs` : nous n'avons besoin de ne rien envoyer pour recevoir la liste complète des posts. Et si on supprime un post, nous n'avons pas besoin de recevoir de message. Mieux, en recevant un message `Empty` nous savons que l'opération s'est bien déroulée.

Maintenant nous pouvons générer le code Go : `protoc -Iproto --go_out=. --go_opt=module=blog --go-grpc_out=. --go-grpc_opt=module=blog proto/*.proto`

### 3-Modèle de données en Go

Créons un répertoire `serveur` ou `server` (peu importe le nom) puis, à l'intérieur de ce dernier, créons un fichier `blog_item.go`. Ce fichier sera le modèle de nos données. Nous allons concevoir un type `BlogItem`. Les tags que l'on va utiliser sont propres à MongoDB : si on a l'habitude de JSON, ces tags sont suffisamment parlants.

```go
package main

import (
    pb "blog/proto"

    "go.mongodb.org/mongo-driver/bson/primitive"
)

type BlogItem struct {
    ID       primitive.ObjectID `bson:"_id,omitempty"`
    AuthorId string             `bson:"author_id"`
    Title    string             `bson:"title"`
    Content  string             `bson:"content"`
}
```

**NB** : il peut y avoir un bug assez vicieux avec le premier tag. Ainsi si l'on rédige `bson:"_id, omitempty"` (avec un espace juste avant omitempty) on a une erreur.

Puis nous créons une première fonction (non une méthode) qui va récupérer les données et les transformer en un objet `*pb.Blog` :

```go
func DocumentToBlog(data BlogItem) *pb.Blog {
    return &pb.Blog{
        Id:       data.ID.Hex(),
        AuthorId: data.AuthorId,
        Title:    data.Title,
        Content:  data.Content,
    }
}
```

Le code `data.ID.Hex()` retourne sous format de chaîne la valeur hexadécimale de l'ID lié à la donnée. L'`ID` est un objet permettant d'allouer et de récupérer un identifiant unique à un document MongoDB.

## 2-Premier service : CreateBlog

### 1-Serveur

Nous allons dans un premier temps créer le code de base de notre serveur dans un fichier `./server/server.go`.

Nous n'implémenterons aucune des fonctionnalités avancées telles que la connexion SSL/TLS ou la gestion des contextes.

```go
package main

import (
    "log"
    "net"

    pb "blog/proto"

    "google.golang.org/grpc"
)

const IP = "50051"

type Server struct {
    pb.BlogServiceServer
}

func main() {
    log.Printf("Lancement du serveur sur %s", IP)

    lst, err := net.Listen("tcp", IP)
    if err != nil {
        log.Printf("Erreur au lancement du serveur : %v", err)
        return
    }
    defer lst.Close()

    s := grpc.NewServer()
    pb.RegisterBlogServiceServer(s, &Server{})
    if err := s.Serve(lst); err != nil {
        log.Printf("Erreur au lancement du serveur gRPC : %v", err)
        return
    }
}
```

Maintenant nous allons devoir déclarer une connexion avec MongoDB. Et avec cet objet nous allons pouvoir opérer les actions CRUD traditionnelles d'une base de données. Tout d'abord nous devons créer une nouvelle connexion qui prendra en argument une URI (`"mongodb://root:root@localhost:27017/"` qui se trouve dans le `docker-compose.yml` en changeant "mongo" en "localhost", et ce, à cause de l'utilisation de Docker). Puis on se connecte à MongoDB en lui fournissant un contexte et on récupère la base de données (si elle n'existe pas, elle est créée à la volée) puis la collection (idem : si elle n'existe pas, elle est créée pour nous par le système). La collection sera une variable globale :

```go
...

var collection *mongo.Collection

func main() {
    log.Printf("Lancement du serveur sur %s", IP)

    // gestion de mongoDB
    client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://root:root@localhost:27017/"))
    if err != nil {
        log.Printf("Erreur à la création du client : %v", err)
        return
    }

    // gestion de la connexion avec Mongo
    if err := client.Connect(context.Background()); err != nil {
        log.Printf("Erreur à la connexion")
    }

  // on ne change rien au reste du code 
  ...
}
```

Maintenant nous allons pouvoir mettre en place le premier service. Dans `blog_grpc.pb.go`, repérons la ligne `type BlogServiceServer interface` qui contient toutes les signatures utiles pour gérer nos services. Puis on va implémenter le premier service : `CreateBlog(context.Context, *Blog) (*BlogId, error)` :

```go

func (s Server) CreateBlog(ctx context.Context, blog *pb.Blog) (*pb.BlogId, error) {

    return nil, nil
}
```

Nous allons procéder aux étapes suivantes :

- créer un objet BlogItem
- insérer cet objet dans la base
- gérer l'erreur
- vérifier le retour de MongoDB
- gérer ce retour vers le client (erreur ou non)

Lors de la création de l'objet BlogItem nous ne créons pas d'ID : nous laissons la main à MongoDB pour gérer cet ID. Le système, après insertion, nous retournera cet ID.

Pour insérer un document, le pilote MongoDB nous fournit une méthode `InsertOne()` à laquelle on passe un contexte et notre item. Et retourne un résultat et une erreur. À la gestion de l'erreur nous retournerons un `status.Errorf()` avec un code `Internal`. Quant au résultat retourné c'est un type `struct` ne contenant qu'une seule chose un type `interface{}` contenu dans un champ `InsertedID`. Donc il faudra caster le résultat : or nous avons laissé la main à MongoDb pour gérer l'ID. Donc on doit caster le retour extrait avec `primitive.ObjectID`. Ce cast retournera un objet et un booléen. Avec le booléen, nous allons pouvoir retourner une valeur hexadécimal (avec une méthode `Hex()` fournit par l'objet retourné) sous forme de chaîne sinon nous retournerons une erreur :

```go
func (s Server) CreateBlog(ctx context.Context, blog *pb.Blog) (*pb.BlogId, error) {
    log.Println("Création d'un post")
    item := BlogItem{
        AuthorId: blog.AuthorId,
        Title:    blog.Title,
        Content:  blog.Content,
    }

    rslt, err := collection.InsertOne(ctx, item)
    if err != nil {
        log.Printf("Une erreur est survenue à l'insertion du document %v : %v", item, err)
        return nil, status.Errorf(
            codes.Internal,
            fmt.Sprintf("erreur à l'insertion du document : %v", err),
        )
    }

    oid, ok := rslt.InsertedID.(primitive.ObjectID)
    if !ok {

        return nil, status.Errorf(codes.Internal, "Convertion impossible de l'ObjectID")

    }

    return &pb.BlogId{
            Id: string(oid.Hex()),
        },
        nil
}
```

### 2-Client

Dans notre répertoire client, nous allons créer un nouveau code pour le client. Mais contrairement au serveur, nous n'avons pas à gérer de connexion MongoDB.

Nous pouvons très simplement créer notre code de base :

```go
package main

import (
    "log"

    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials/insecure"

    pb "blog/proto"
)

const IP = "localhost:50051"

func main() {
    log.Println("Lancement du client")

    cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
    if err != nil {
        log.Printf("Erreur au lancement du client sur %s, %v", IP, err)
        return
    }
    defer cnx.Close()

    client := pb.NewBlogServiceClient(cnx)

    id :=  client.cr
}
```

Maintenant nous allons pouvoir créer un nouveau post en utilisant la méthode `CreateBlog` correspondant au service Protocol Buffer éponyme. Cette méthode nous retournera un message de la part du serveur de type `BlogId` :

```go
func main() {
    ...

    client := pb.NewBlogServiceClient(cnx)

    // création d'un post
    createPostBlog(client)
}

func createPostBlog(client pb.BlogServiceClient) string {
    id, err := client.CreateBlog(context.Background(),
        &pb.Blog{
            AuthorId: "John Doe",
            Title:    "Premier post",
            Content:  "Duis ex amet esse culpa minim exercitation dolore labore dolore incididunt esse ea pariatur. Sint cupidatat ad sint voluptate mollit. Voluptate ad Lorem aute officia fugiat sunt cupidatat Lorem duis est. Cillum cillum incididunt culpa incididunt ut irure ad nisi mollit sunt sint. Sint enim commodo magna excepteur deserunt nisi consequat magna cupidatat aliqua ex est commodo. Occaecat aliqua ad deserunt dolor ullamco ullamco excepteur elit.",
        })

    if err != nil {
        log.Printf("Erreur à la création du post : %v", err)
    }

    log.Printf("Post enregistré avec l'id : %v", id.Id)
    return id.Id
}
```

Pour tester le code, il faudra 3 terminaux ouverts :

- pour docker : le répertoire de travail sera la racine du projet, là où se trouve le fichier `docker-compose.yml`
- pour le serveur : le répertoire courant sera `./server`
- pour le client : le répertoire courant sera `./client`

Dans le premier, on tapera la commande suivante : `docker-compose up`
Dans les deux autres, on lancera la commande `go run .`. D'abord pour le serveur et ensuite le client.

Si le client retourne une valeur avec des zéros uniquement, c'est qu'il y a eu une erreur (il se peut que le problème vient d'un des tags du type `BlogItem`).

## 3-Service ReadBlog

### 1-Serveur et ReadBlog

L'implémentation nécessite de récupérer dans `blog_grpc.pb.go`, la signature de la méthode ReadBlog : `ReadBlog(context.Context, *BlogId) (*Blog, error)`

Notre squelette de code ressemblera à :

```go
func (s Server) ReadBlog(ctx context.Context, id *pb.BlogId) (*pb.Blog, error) {
    log.Printf("Demande de lecture d'un post (id=%v)", id.Id)
    post := &pb.Blog{}

    return post, nil
}
```

Maintenant nous devons gérer la récupération d'une donnée ou non (erreur).

Les étapes :

- transformer l'ID reçu avec `ObjectIDFromHex()` du module `primitive`
- passer l'ID à cette fonction et récupérer le nouveau type et une erreur
- traiter l'erreur avec une erreur de type `codes.InvalidArgument` et un message
- utiliser `FindOne()` pour requêter sur la collection en cours avec un filtre de type `bson.M{"clé", "valeur"}`
- récupérer le type `SingleResult`
- puis décoder avec `Decode()` en passant un pointeur de type `Blog` et gérer l'erreur
- retourner un résultat

**NB** : `bson.M` permet de transformer la donnée en un type de document gérable par MongoDB. Il existe d'autres types de documents spécifiques comme D dont la principale différence porte sur l'ordre des éléments du documents qui doit être respecté.

```go
func (s Server) ReadBlog(ctx context.Context, id *pb.BlogId) (*pb.Blog, error) {
    log.Printf("Demande de lecture d'un post (id=%v)", id.Id)
    post := &pb.Blog{}

    oid, err := primitive.ObjectIDFromHex(id.Id)
    if err != nil {
        return nil, status.Errorf(codes.InvalidArgument, "L'id n'est pas valide.")
    }

    rslt := collection.FindOne(ctx, bson.M{"_id": oid})

    if err := rslt.Decode(post); err != nil {
        return nil, status.Errorf(
            codes.Internal,
            "Impossible de décoder les données",
        )
    }

    return post, nil
}
```

### 2-Client et ReadBlog

Ici nous souhaitons envoyer un ID et récupérer quelque chose : soit un type Blog soit une erreur.

Modifions d'abord notre client : nous avons créé un service pour envoyer une donnée. Cette fonction retournait une chaîne représentant l'ID :

```go
func main() {
    log.Println("Lancement du client")

    cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
    if err != nil {
        log.Printf("Erreur au lancement du client sur %s, %v", IP, err)
        return
    }
    defer cnx.Close()

    client := pb.NewBlogServiceClient(cnx)

    // création d'un post
    id := createPostBlog(client)

    // lecture du post envoyé
    readBlog(client, id)
}
```

Notre nouvelle fonction  `ReadBlog` se chargera d'envoyer un Id reçu un paramètre et retournera un pointeur de `Blog`

Les étapes :

- afficher un message
- appeler depuis le client via la méthode `ReadBlog()`
- récupérer le résultat (BlogItem) et l'erreur
- traiter l'erreur
- afficher le contenu du post
- retourner le post

```go
func readBlog(client pb.BlogServiceClient, id string) *pb.Blog {
    log.Printf("Demande de lire un post ayant l'id %s", id)

    rslt, err := client.ReadBlog(context.Background(), &pb.BlogId{
        Id: id,
    })

    if err != nil {
        log.Printf("Erreur à l'envoi de l'ID : %v", err)
    }

    log.Printf("Post récupéré avec succès : %+v", rslt)
    return rslt
}
```

On pourra tester l'ID retourner par `createBlog()` et tester un ID erroné

## 4-Service UpdateBlog

### 1-Serveur et UpdateBlog

Recherchons la signature UpdateBlog dans `blog_grpc.pb.go` :

```go
UpdateBlog(context.Context, *Blog) (*empty.Empty, error)
```

L'objectif est récupérer un type Blog avec des données mises à jour et nous demanderons une mise à jour sur le serveur MongoDB. En cas d'erreur nous retournerons un message spécifique si tout s'est bien déroulé nous retourné un type particulier: un pointeur de `empty.Empty`.

La mise à jour s'effectuera avec la méthode `UpdateOne`. Le driver de MongoDB propose des alternatives comme `UpdateMany` et `UpdateById`.

Avec `UpdateOne`, il faut passer trois argument :

- un contexte
- un ObjectID dans un type `bson.M`
- le jeu de donnée. Celui-ci sera stocké dans un `bson.M` ayant pour clé l'opérateur `"$set"`

Cette méthode nous retourne un résultat et une erreur. En fait, il y a deux types d'erreurs :

- soit elle nous n'avons aucun document
- soit c'est une erreur toute autre (gestion de `err != nil`)

Si tous les tests ont été passés avec succès, alors on retourne un Empty et nil :

```go
func (s Server) UpdateBlog(ctx context.Context, in *pb.Blog) (*empty.Empty, error) {
    log.Printf("Appel de UpdateBlog avec %v", in)

    oid, err := primitive.ObjectIDFromHex(in.Id)
    if err != nil {
        return nil, status.Error(codes.InvalidArgument, "ID erroné")
    }

    item := BlogItem{
        ID:       oid,
        AuthorId: in.AuthorId,
        Title:    in.Title,
        Content:  in.Content,
    }

    rslt, err := collection.UpdateOne(ctx, bson.M{"_id": oid}, bson.M{"$set": item})

    if err == nil {
        return nil, status.Errorf(
            codes.Internal,
            "Mise à jour impossible",
        )
    }

    if rslt.MatchedCount == 0 {
        return nil, status.Errorf(
            codes.NotFound,
            "Aucun document trouvé",
        )
    }

    return &emptypb.Empty{}, nil
}
```

### 2-Client et UpdateBlog

Notre client va devoir mettre à jour un document. Pour cela nous allons demander un ID (string) en plus du paramètre `client`. Le plus pertinent est de passer un nouvel objet `pb.Blog`. Donc nous n'allons mettre à jour que quelques éléments :

```go
nvBlog := &pb.Blog{
    Id:       id,
    AuthorId: "Bob et charlie",
    Title:    "Nouveau titre",
}
```

Le code client sera très simple :

```go
func updateBlog(client pb.BlogServiceClient, blog *pb.Blog) {
    log.Printf("Appel d'une mise à jour d'un post (id=%v)", blog.Id)

    _, err := client.UpdateBlog(context.Background(), blog)
    if err != nil {
        log.Printf("Erreur lors de la mise à jour : %v", err)
    }

    log.Println("Post mis à jour avec succès")
}
```

## 5-ListBlog

### 1-Serveur et ListBlog

À nouveau regardons l'interface BlogServiceServer pour rechercher la méthode à implémenter pour retourner la liste complète :

```go
ListBlogs(*empty.Empty, BlogService_ListBlogsServer) error
```

Le second argument de la signature est la fonction de streaming du serveur.

On code le premier squelette :

```go
func (s Server) ListBlogs(e *empty.Empty, stream pb.BlogService_ListBlogsServer) error {
    log.Println("Appel de la liste complète des posts")
    return nil
}
```

Pour le dialogue avec MongoDB, le driver fournit la méthode Find() qui retourne un curseur. Et on va pouvoir itérer dessus. À chaque itération, on récupère une donnée et on la retourne (stream).

La méthode `Find` demande un contexte et un filtre : même si nous n'utiliserons aucun filtre puisque nous voulons tout récupérer, il faut envoyer un document (`primitive.D{}`) vide (donc `{}`).

Les précisions du code sont placés en commentaire dans le code :

```go
func (s Server) ListBlogs(e *empty.Empty, stream pb.BlogService_ListBlogsServer) error {
    log.Println("Appel de la liste complète des posts")

    ctx := context.Background()

    // on récupère la liste complète de la collection : pas de filtrage
    cur, err := collection.Find(ctx, primitive.D{{}})
    if err != nil {
        return status.Errorf(
            codes.Internal,
            fmt.Sprintf("Impossible de récupérer les données du serveur de Mongo: %v", err),
        )
    }

    // ne pas oublier de cloturer le curseur
    defer cur.Close(ctx)

    for cur.Next(ctx) { // code s'arrêtera quand Next() sera épuisé (retourne False)
        // création d'un document
        doc := &BlogItem{}

        if err := cur.Decode(doc); err != nil {
            return status.Errorf(
                codes.Internal,
                fmt.Sprintf("Erreur au décodage du document : %v", err),
            )
        }
        // notre fonction va convertir un type BlogItem en type Blog
        // attendu par la méthode Send de stream
        stream.Send(DocumentToBlog(*doc))

    }
    // en cas d'erreur sur le curseur :
    if err := cur.Err(); err != nil {
        return status.Errorf(
            codes.Internal,
            fmt.Sprintf("Erreur interne inconnue : %v", err),
        )
    }
    return nil
}
```

### 2-Client et ListBlog

Problème : récupérer et gérer un flux de données provenant du serveur

La gestion du flux passe par une boucle et la gestion de `io.EOF`

```go
func listBlog(client pb.BlogServiceClient) {
    log.Println("Demande de la liste complète des posts")

    stream, err := client.ListBlogs(context.Background(), &emptypb.Empty{})

    if err != nil {
        log.Printf("Une erreur est survenue : %v", err)
    }

    for {
        doc, err := stream.Recv()

        if err == io.EOF {
            log.Println("Fin de transmission des données")
            break
        }

        if err != nil {
            log.Printf("Une erreur inconnue est survenue : %v", err)
            return
        }

        log.Printf("%+v", doc)
    }
}
```

**NB** : ne pas oublier de rajouter dans la fonction `main`, l'appel `listBlog(client)`

## 6-DeleteBlog

Voici la dernière partie consacrée à la suppression d'une donnée (un document).

### 1-Serveur et DeleteBlog

Si l'on observe la signature de l'interface `BlogServiceServer`, on a une signature identique à `UpdateBlog` :

```go
DeleteBlog(context.Context, *BlogId) (*empty.Empty, error)
```

L'objet est de passer un type BlogId (pointeur), de supprimer cet élément de la base de données et de retourner, soit une erreur soit un élément `Empty`

Du côté de MongoDB, on utilisera la méthode `DeleteOne()` qui prend en argument un contexte et un filtre. Nous devons gérer plusieurs erreurs : les traditionnelles mais aussi si aucun document n'a été supprimé. Le retour de `DeleteOne()` est un tuple avec :

- un pointeur de `mongo.DeleteResult`
- une erreur

C'est avec le pointeur de DeleteResult que nous allons créer une erreur si la valeur associée au champ `DeletedCount` est égal à 0.

Et si tout s'est bien déroulé on retourne un type `Empty` et `nil` pour l'erreur.

```go
func (s Server) DeleteBlog(ctx context.Context, id *pb.BlogId) (*empty.Empty, error) {
    log.Printf("Suppression d'un post avec l'id %s", id.Id)

    oid, err := primitive.ObjectIDFromHex(id.Id)

    if err != nil {
        return nil, status.Errorf(
            codes.Internal,
            "ID erroné",
        )
    }

    rslt, err := collection.DeleteOne(ctx, bson.M{"_id": oid})

    if err != nil {
        return nil, status.Errorf(
            codes.Internal,
            fmt.Sprintf("Objet impossible à supprimer : %v", err),
        )
    }

    if rslt.DeletedCount == 0 {
        return nil, status.Error(
            codes.NotFound,
            "Le post n'a pas été traité",
        )
    }

    return &emptypb.Empty{}, nil

}
```

### 2-Client et DeleteBlog

Maintenant nous allons implémenter la partie client : celui doit envoyer un ID et afficher soit les erreurs soit un message comme quoi le traitement a été parfaitement opéré.

Le code en soit ne posera aucun problème car il est très simple :

```go
func deleteBlog(client pb.BlogServiceClient, id string) {
    log.Println("Suppression d'un post")

    _, err := client.DeleteBlog(context.Background(), &pb.BlogId{Id: id})
    if err != nil {
        log.Printf("Erreur rencontrée : %v", err)
        return
    }

    log.Printf("Suppression du post (id: %s)", id)
}
```

Au niveau de la fonction `main`, il faudra rajouter l'appel suivant :

```go
deleteBlog(client, id)
```

Tout le code source se trouve dans [le chapitre 9 du répertoire "codes"](../codes/9-Blog/)