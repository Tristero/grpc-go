# API Unaire

Pour notre projet nous allons réutiliser le code précédent. Le code est placé dans `./codes/2-API-unaire`.

## 1-Implémentation du serveur

Pour appeler le service créé, nous allons utiliser le code généré par le compilateur Protocol Buffer.

Pour rappel le service PB :

```proto
service GreetService {
    rpc Greet (GreetRequest) returns (GreetResponse);
}
```

Dans un premier temps, nous devons importer notre bibliothèque générée par `protoc` :

```go
package main

import (
    "log"
    "net"

    pb "apiunaire/proto"

    "google.golang.org/grpc"
)
```

Maintenant, nous devons créer une structure contenant un service GreetServiceServer qui représente l'API du service GreetService. Ce type nous sera utile pour lier l'API au serveur.

```go
type Server struct {
    pb.GreetServiceServer
}
```

Maintenant nous avons toutes les briques, il nous faut les assembler.

Pour cela nous devons appeler depuis notre bibliothèque générée, la fonction `ReisterGreetServiceServer()`. Cette fonction a besoin de notre serveur gRPC que nous avions déjà instanciée en premier argument puis on crée un pointeur de type `Server` en second argument. Tout simplement, on associe l'API et ses endpoints, stockés dans notre référence du type `Server`, au serveur gRPC :

```go
s := grpc.NewServer() // notre serveur gRPC déjà créé
pb.RegisterGreetServiceServer(s, &Server{}) //on associe serveur gRPC à notre API et ses endpoints
```

Que nous reste-t-il à faire ? il nous faut définir un comportement. Le code généré dans `./proto/greet_grpc.pb.go` va nous être utile. Tout d'abord recherchons le service `GreetService` dans le code source généré. Nous avons plusieurs dizaines de références. Or nous, nous nous intéressons  uniquement à la partie serveur : or, on nous fournit une interface nommée `GreetServiceServer` :

```go
type GreetServiceServer interface {
    Greet(context.Context, *GreetRequest) (*GreetResponse, error)
    mustEmbedUnimplementedGreetServiceServer()
}
```

Et dans sa définition, la méthode qui nous intéresse est la toute première : `Greet(context.Context, *GreetRequest) (*GreetResponse, error)`. C'est cette méthode que devons implémenter.

*Remarque sur la gestion des fichiers* : soit on se crée un autre fichier (ce qui est le mieux en mode production et professionnel) mais ici nous allons implémenter nos fonctions directement dans le main.go pour éviter de naviguer dans des différents fichiers.

Si nous observons la signature, nous allons devoir importer le package `context`. Pour implémenter la signature, la méthode `Greet` est une fonction associée à notre type `Server`. Ce qui donne le squelette suivant :

```go
func (s *Server) Greet(ctx context.Context, req *pb.GreetRequest) (*pb.GreetResponse, error) { }
```

Maintenant nous allons ajouter de la logique :

- on affichera avec log la requête reçue
- on retournera un pointeur de type pb.GreetResponse.

Mais comment utiliser `GreetResponse` ? encore une fois, il faut étudier le code source généré. En fait `GreetResponse` est un type dont une seule valeur est accessible publiquement : `Result` qui est un champ de type string. Voici le code issu de `greet.pb.go` :

```go
type GreetResponse struct {
    state         protoimpl.MessageState
    sizeCache     protoimpl.SizeCache
    unknownFields protoimpl.UnknownFields

    Result string `protobuf:"bytes,1,opt,name=result,proto3" json:"result,omitempty"`
}
```

Nous souhaitons retourner une chaîne de type  `"Hello, <NOM REÇU>"`. Pour cela on dispose d'un accès à la valeur `FirstName` soit directement soit via la méthode `GetFirstName()` (pour des questions de bonne pratique, il convient non pas d'utiliser directement le champ mais la méthode). Voici le code complet de notre serveur :

```go
package main

import (
    "context"
    "log"
    "net"

    pb "apiunaire/proto"

    "google.golang.org/grpc"
)

type Server struct {
    pb.GreetServiceServer
}

const addr = "0.0.0.0:50051"

func main() {
    lst, err := net.Listen("tcp", addr)
    if err != nil {
        log.Fatalf("Erreur à l'écoute du port TCP [%s] :%v\n", addr, err)
    }
    defer lst.Close()

    log.Printf("Ecoute sur %s\n", addr)

    s := grpc.NewServer()
    pb.RegisterGreetServiceServer(s, &Server{})

    if err := s.Serve(lst); err != nil {
        log.Fatalf("Echec au lancement du serveur : %v", err)
    }

}

func (s *Server) Greet(ctx context.Context, req *pb.GreetRequest) (*pb.GreetResponse, error) {
    log.Printf("Fonction Greet (API unaire) appelée avec %v", req)
    return &pb.GreetResponse{Result: fmt.Sprintf("Hello %s", req.GetFirstName())},
        nil
}
```

On peut tester s'il n'y a pas d'erreurs. Mais tant que nous n'avons pas modifier notre client, il ne se passera rien.

## 2-Implémentation du client

Nous allons récupérer le résultat côté client. Nous avons le code suivant :

```go
package main

import (
    "log"

    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials/insecure"
)

const addr = "localhost:50051"

func main() {
    cnx, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
    if err != nil {
        log.Fatalf("Erreur à la connexion vers le serveur : %v\n", err)
    }
    defer cnx.Close()
}
```

Comme pour le serveur nous devons importer notre bibliothèque générée par `protoc`. Puis nous allons appeler le service pour le client de GreetService : `NewGreetServiceClient()`.Et nous lui passons la connexion en paramètre. Cette méthode retourne un type `GreetServiceClient`. C'est ce client qui va nous servir de lien de communication entre nous et le serveur.

Donc nous devons procéder en deux étapes :

- l'envoi d'une requête via le nouveau clien
- la réception de la réponse du serveur

La requête est un pointeur d'une structure simple de type `GreetRequest` possédant un seul champ public : `FirstName` auquel on passera une chaîne

```go
package main

import (
    "context"
    "fmt"
    "log"

    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials/insecure"

    pb "apiunaire/proto"
)

const addr = "localhost:50051"

func main() {
    cnx, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
    if err != nil {
        log.Fatalf("Erreur à la connexion vers le serveur : %v\n", err)
    }
    defer cnx.Close()

    // Création d'un nouveau client
    client := pb.NewGreetServiceClient(cnx)
    // envoi d'une requête et récupération de la réponse
    rep, err := client.Greet(context.Background(), &pb.GreetRequest{FirstName: "John"})
    log.Println("Envoi d'une requête")
    if err != nil {
        log.Fatalf("Erreur : %v\n", err)
    }
    log.Printf("Réponse du serveur : %v\n", rep.GetResult())
}
```

Remarque : le contexte n'est pas géré de la bonne manière ici mais comme il n'y a pas d'autres éléments côté client nécessitant la gestion d'un contexte on le gèrera ainsi.

Testons en démarrant notre serveur puis notre client.

Si tout s'est bien déroulé :

- côté serveur, nous avons eu un message de type `Fonction Greet (API unaire) appelée avec first_name:"John"`
- côté client, nous avons eu le résultat final `Réponse du serveur : Hello John`
