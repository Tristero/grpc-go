# Bidirectional Stream API

## 1-Implémentation dans Protocol Buffer

Nous devons préciser comme envoyer et recevoir des données depuis et vers le client et le serveur. Très simplement, nous devons préciser qu'aussi bien du côté client que du côté serveur, nous employons un flux de données :

```proto
syntax = "proto3";

package greet;

option go_package="api-bidi/proto";

message GreetRequest {
    string first_name = 1;
}

message GreetResponse {
    string result = 1;
}

service GreetService {
    rpc BidirectionalGreet (stream GreetRequest) returns (stream GreetResponse);
}
```

Puis on génère notre code : ``

## 2-Implémentation du serveur

Maintenant reprenons un code de départ pour notre serveur :

```go
package main

import (
    "log"
    "net"

    pb "api-bidi/proto"

    "google.golang.org/grpc"
)

const IP = "0.0.0.0:50051"

type Server struct {
    pb.GreetServiceServer
}

func main() {
    log.Println("Lancement du serveur")

    lst, err := net.Listen("tcp", IP)
    if err != nil {
        log.Printf("Erreur à l'écoute sur %v", IP)
        return
    }
    defer lst.Close()

    s := grpc.NewServer()
    pb.RegisterGreetServiceServer(s, &Server{})
    if err := s.Serve(lst); err != nil {
        log.Printf("Une erreur est survenue dans le lancement du serveur : %v", err)
        return
    }
}
```

Et récupérons dans le code généré l'interface du serveur dans `proto_grpc.pb.go` :

```go
type GreetServiceServer interface {
    BidirectionalGreet(GreetService_BidirectionalGreetServer) error
    mustEmbedUnimplementedGreetServiceServer()
}
```

Maintenant que nous avons la signature, nous allons pouvoir implémenter une méthode pour notre type  `Server`. Tout d'abord créons le squelette :

```go
func (s Server) BidirectionalGreet(stream pb.GreetService_BidirectionalGreetServer) error {
    return nil
}
```

Qu'allons faire ? il faut fusionner les connaissances que l'on a accumulées avec les API de streaming serveur ET client côté serveur.

Donc on va créer une boucle infinie pour la gestion des requêtes clients : bien évidemment on devra gérer l'arrêt de la réception des données. Dès que l'on recevra une requête nous émetterons une réponse.

```go
func (s Server) BidirectionalGreet(stream pb.GreetService_BidirectionalGreetServer) error {
    log.Println("Appel de la méthode BidirectionalGreet")

    // écoute du client
    for {

        // Gestion des messages reçus
        msg, err := stream.Recv()
        if err == io.EOF {
            log.Println("Fin de transmission des données")
            return nil
        }
        if err != nil {
            log.Println("Erreur lors de la lecture d'un message reçu")
            return err
        }
        log.Printf("Réception d'un message %s", msg.GetFirstName())

        // Gestion des réponses
        rep := &pb.GreetResponse{
            Result: fmt.Sprintf("Hello, %s !", msg.GetFirstName()),
        }

        if err := stream.SendMsg(rep); err != nil {
            log.Println("Erreur lors de l'envoi d'une réponse.")
            return err
        }
    }
}
```

On peut tester notre serveur.

## 3-Implémentation du client

Nous reprenons un code de base pour notre client :

```go
package main

import (
    "log"

    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials/insecure"

    pb "api-bidi/proto"
)

const IP = "localhost:50051"

func main() {

    log.Println("Lancement du client")
    cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
    if err != nil {
        log.Printf("Erreur à la création du client : %v", err)
        return
    }
    defer cnx.Close()

    client := pb.NewGreetServiceClient(cnx)
}
```

Comme pour le serveur nous allons devoir gérer l'envoi en flux vers le serveur et la réception en flux des réponses du serveur.

Dans un premier temps, on appelle la méthode du service nommé dans le fichier Protocol Buffer `BidirectionalGreet` : cette méthode retounera un flux (client) et une erreur.

Puis nous créons un slice de prénoms.

Mais contrairement à ce que nous avons jusqu'ici nous allons devoir gérer de front l'envoi et la réception de données : il nous faudra utiliser la programmation concurrentielle.

On aura une goroutine pour l'envoi et un pour la réception des données.

Commençons par la plus simple : l'envoi de données. Nous ne gèrerons pas les erreurs et nous simulerons un temps de latence pour observer les comportements. Il ne faut pas oublier que nous devons explicitement fermer le flux !

```go
    noms := []string{"Alice", "Bob", "Charlie"}

    go func() {
        for _, nom := range noms {
            req := &pb.GreetRequest{
                FirstName: nom,
            }
            log.Printf("Envoi de la requête %+v", req)
            if err := stream.Send(req); err != nil {
                log.Printf("Erreur survenue à l'envoi : %v", err)
                break
            }
            time.Sleep(1 * time.Second)
        }
        if err := stream.CloseSend(); err != nil {
            log.Printf("Erreur à la fermeture de l'envoi :%v", err)
        }
    }()
```

La seconde gère la réception des données. Dans ce cas, nous devons gérer un flux infini de données. Il y a trois étapes :

- la gestion EOF : fin de transmission des données
- toute autre erreur
- fermeture de la connexion

Le plus gros problème est la gestion de la goroutine : il nous faut un sémaphore. Il faut que la boucle main attende le résultat de notre goroutine. Sans ce sémaphore, on aura bien notre première goroutine gérée, mais pas la seconde !

Donc on va utiliser un channel très simple : tant qu'il est ouvert, c'est que l'on reçoit des données (pas de réception de `io.EOF`). Dans le cas où le serveur renvoit un io.EOF, alors on clôture le canal ! Puis on rejette le résultat du canal dans le vide, une fois un retour obtenu : ce qui est le cas quand on clôture le canal. Dans ce cas, soit on appelle `close()` soit on passe une structure vide :

```go
    semaphore := make(chan struct{})

    go func() {
        for {
            rep, err := stream.Recv()
            if err == io.EOF {
                log.Printf("Fin de transmission")
                break
            }
            if err != nil {
                log.Println("Une erreur inattendue est survenue : %v", err)
                break
            }
            log.Printf("Réponse reçue du serveur : %v", rep)
        }
        if err := stream.CloseSend(); err != nil {
            log.Printf("Erreur à la fermeture du stream : %v", err)
        }
        
        //close(semaphore)
        semaphore <- struct{}{}
    }()

    <-semaphore
```
