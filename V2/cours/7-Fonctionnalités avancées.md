# Les fonctionnalités avancées avec gRPC

## 1-Gestion des erreurs d'implémentation

Exemple : le calcul d'une racine carré.

Dans ce cadre, nous savons que le calcul d'une racine carré d'un nombre négatif est impossible. Donc nous allons développer côté serveur une réponse adaptée à cette erreur.

### 1-Protocol Buffer

Notre projet sera toujours composé dans un répertoire de son choix, des éléments suivants :

- ./server/main.go
- ./client/main.go
- ./proto/proto.proto

On initialisera notre module Go avec `go mod init erreur` (par exemple).

Puis on renseigne notre fichier proto.proto

```proto
syntax = "proto3";

package greet;

option go_package="erreur/proto";

message Request {
    int32 value = 1;
}

message Response {
    double result = 1;
}

service SquareRootService {
    rpc SqrtCalculator(Request) returns (Response);
}
```

On génère le projet : `protoc -Iproto --go_out=. --go_opt=module=erreur --go-grpc_out=. --go-grpc_opt=module=erreur proto/*.proto`

Puis on tape `go mod tidy`

**Remarque** : on pourrait restreindre non pas à `int32` mais à `uint32` !

### 2-Serveur

Nous reprenons toujours la même base de code : nous nous trouvons dans le cas très simple d'une API unaire !

```go
package main

import (
    "log"
    "net"

    pb "erreur/proto"

    "google.golang.org/grpc"
)

const IP = "0.0.0.0:50051"

type Server struct {
    pb.SquareRootServiceServer
}

func main() {
    log.Printf("Lancement du serveur sur %v", IP)

    lst, err := net.Listen("tcp", IP)
    if err != nil {
        log.Printf("Erreur à l'écoute de %s : %v", IP, err)
    }
    defer lst.Close()

    s := grpc.NewServer()
    pb.RegisterSquareRootServiceServer(s, &Server{})
    if err := s.Serve(lst); err != nil {
        log.Printf("Erreur survenue au lancement du serveur : %v", err)
        return
    }
}
```

On doit rechercher la signature dans le fichier généré `proto_grpc.pb.go` de la méthode à implémenter dans l'interface du serveur :

```go
type SquareRootServiceServer interface {
    SqrtCalculator(context.Context, *Request) (*Response, error)
    mustEmbedUnimplementedSquareRootServiceServer()
}
```

Maintenant nous pouvons implémenter la méthode `SqrtCalculator` dans notre serveur. Mais pour cela nous devons deux cas :

- si le chiffre est positif nous retournons une valeur calculée sous forme d'un pointeur de type `pb.Response` et nil en erreur
- si le chiffre est négatif nous devons retourner nil et une erreur

Pour créer une erreur, gRPC nous propose un type `status` avec la bibliothèque `google.golang.org/grpc/status` (il faut importer cette bibliothèque et pas une autre).

Avec `status`, nous allons appeler `Errorf()` qui prend deux arguments

- un code de type `codes.Code`
- un message (string)

Le choix du code est libre mais il faut s'accorder avec l'erreur donc nous choisirons `InvalidArgument`

```go
func (s Server) SqrtCalculator(ctx context.Context, req *pb.Request) (*pb.Response, error) {

    val := req.GetValue()

    log.Printf("Appel du calcul de la racine carrée de %d", val)

    if val >= 0 {
        rslt := math.Sqrt(float64(val))
        return &pb.Response{
            Result: rslt,
        }, nil
    }
    return nil, status.Errorf(codes.InvalidArgument, "La valeur transmise %d n'est pas positive", val)
}
```

**Remarque** : le calcul de la racine carré passe par l'utilisation d'un type `float64`. La fonction est importée depuis le package `math`

### 3-Client

Reprenons le code client pour une API Unaire :

```go
package main

import (
    "context"
    "log"

    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials/insecure"

    pb "erreur/proto"
)

const IP = "localhost:50051"

func main() {
    log.Printf("Lancement du client sur %v", IP)
    cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
    if err != nil {
        log.Printf("Erreur à la connexion sur %s : %v", IP, err)
    }
    defer cnx.Close()

    client := pb.NewSquareRootServiceClient(cnx)
}
```

Maintenant appelons la méthode `` :

```go
func main() {
    
    ...
    
    rep, err :=  client.SqrtCalculator(context.Background(), &pb.Request{
        Value: 16,
    })
    if err != nil {

        return
    }
    log.Println(rep.GetResult())
}
```

Comment gérer notre erreur ? nous allons transformer notre erreur en un status gRPC. Pour cela, la bibliothèque grpc nous propose une méthode d'extraction `fromError()` dans laquelle on passe notre erreur. On récupèrera un status et un booléen. De ce status, on nous allons pouvoir récupérer des informations tels que le code et le message :

```go
func main() {
    
    ...
    
    rep, err := service.SqrtCalculator(context.Background(), &pb.Request{
        Value: 16,
    })
    if err != nil {
        s, ok := status.FromError(err)
        if ok {
            log.Printf("[Code : %v] %v", s.Code(), s.Message())
            // là on rajoute un test inutile : démonstration de l'utilisation
            // d'une comparaison avec les codes prédéfinis
            if s.Code() == codes.InvalidArgument {
                log.Println("L'argument passé est sans doute négatif.")
            }
        } else {
            log.Printf("Erreur inconnue : %v", err)
        }
        return
    }
    log.Println(rep.GetResult())
}
```

On lance le serveur puis le client. Dans notre exemple : pas d'erreur on nous retourne 4.

Maintenant modifions 16 pour -16. Et observons le résultat :

```sh
> $ go run .
2022/08/09 17:24:20 Lancement du client sur localhost:50051
2022/08/09 17:24:20 [Code InvalidArgument] La valeur transmise -16 n'est pas positive
2022/08/09 17:24:20 L'argument passé est sans doute négatif.
```

## 2-Les deadlines et timeouts

Au lieu d'attendre X minutes, secondes ou heures pour une réponse, il est intéressant de gérer un temps limite.

Le serveur va attendre 3 secondes et après il retournera une réponse. Le client va d'abord attendre 5 secondes puis on modifiera pour 1 seconde. Nous allons créer un serveur et un client dans le cadre d'une API unaire.

**NB** : la vidéo pour le client propose un code inutile, nous proposons une alternative fonctionnelle.

**Il faut différencier le timeout de la deadline**. La **deadline** sera gérée du côté du serveur : ici le serveur va savoir si le client est toujours connecté, s'il ne l'est pas alors il coupe la communication et retourne une erreur. Dans le cas du **timeout** : c'est côté client que l'on géré ce problème. Si au-delà d'un certain délai, il n'a toujours pas reçu de réponse du serveur, il coupera la communication.

### 1-Création du projet et du fichier PB

On reprend la même architecture que précédemment et on crée un module go "deadlines". Puis on édite simplement le fichier Protocol Buffer avec le code suivant :

```proto
syntax = "proto3";

package greet;

option go_package="deadlines/proto";

message Request {
    string first_name = 1;
}

message Response {
    string result = 1;
}

service GreetService {
    rpc GreetWithDeadline (Request) returns (Response);
}
```

On génère le code : `protoc -Iproto --go_out=. --go_opt=module=deadlines --go-grpc_out=. --go-grpc_opt=module=deadlines proto/*.proto`

Puis on appelle `go mod tidy`

### 2-Serveur et deadline

On reprend toujours le même code de base pour le serveur. Ensuite nous recherchons une interface pour le serveur gérant le service `GreetService`... et on trouve la méthode à implémenter : `GreetWithDeadline(context.Context, *Request) (*Response, error)`. À l'implémentation on va pouvoir rajouter une pause de 3 secondes.

```go
package main

import (
    "context"
    "fmt"
    "log"
    "net"

    pb "deadlines/proto"

    "google.golang.org/grpc"
)

type Server struct {
    pb.GreetServiceServer
}

const IP = "0.0.0.0:50051"

func main() {
    lst, err := net.Listen("tcp", IP)
    if err != nil {
        log.Fatalf("Erreur à l'écoute du port TCP [%s] :%v\n", IP, err)
    }
    defer lst.Close()

    log.Printf("Ecoute sur %s\n", IP)

    s := grpc.NewServer()
    pb.RegisterGreetServiceServer(s, &Server{})

    if err := s.Serve(lst); err != nil {
        log.Fatalf("Echec au lancement du serveur : %v", err)
    }

}

func (s *Server) GreetWithDeadline(ctx context.Context, req *pb.Request) (*pb.Response, error) {
    log.Printf("Appel de GreetWithDeadline avec %v", req)

    time.Sleep(3*time.Second)

    return &pb.Response{Result: fmt.Sprintf("Hello %s", req.GetFirstName())},
        nil
}
```

Dans la vidéo du cours, il donnait un code qui est donnée :

```go
    for i := 0; i < 3; i++ {
        if ctx.Err() == context.DeadlineExceeded {
            log.Println("Le client n'est plus connecté")
            return nil, status.Error(codes.DeadlineExceeded, "Temps d'attente trop long")
        }
        time.Sleep(1 * time.Second)
    }
```

ne fait strictement rien car il ne vérifie pas que le serveur n'a pas défini de deadlines.

La solution est d'appeler `context.WithDeadline(ctx, deadline)` où l'on définit un temps. Voici une solution permettant de simuler une latence de 3 secondes ET une deadline de 1 seconde laissée au serveur pour répondre :

```go
func (s *Server) GreetWithDeadline(ctx context.Context, req *pb.Request) (*pb.Response, error) {
    log.Printf("Appel de GreetWithDeadline avec %v", req)

    time.Sleep(3 * time.Second)

    deadline := time.Now().Add(1 * time.Second)
    ctx, cancel := context.WithDeadline(ctx, deadline)
    defer cancel()

    if err := ctx.Err(); err != nil {
        log.Printf("Le client n'est plus connecté : %v", err)
        return nil, status.Error(codes.DeadlineExceeded, "Temps d'attente trop long")
    }

    log.Println("Envoi réponse")

    return &pb.Response{Result: fmt.Sprintf("Hello %s", req.GetFirstName())},
        nil

}
```

L'avantage de ce code, on sait si le client est connecté ou pas. Lorsque le client aura son timeout à 3 secondes, nous aurons deux erreurs:

- côté serveur : la déconnexion du client
- côté client : temps d'attente dépassé.

### 3-Client et timeout

Côté client c'est un timeout qu'il faut gérer et non une deadline.

Pour le client nous reprenons le même code que pour une API unaire.

```go
package main

import (
    "context"
    "log"

    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials/insecure"

    pb "deadlines/proto"
)

const IP = "localhost:50051"

func main() {
    log.Printf("Lancement du client sur %s", IP)

    cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
    if err != nil {
        log.Printf("Erreur à la connexion : %v", err)
    }
    defer cnx.Close()

    client := pb.NewGreetServiceClient(cnx)

}
```

Maintenant nous devons définir un timeout à partir du module `context` de la bibliothèque standard. Pour cela nous allons utiliser la méthode `Background()` comme paramètre `WithTimeout()` puis en second paramètre une durée (type `time.Duration`).

```go
ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
```

Le retour de cette fonction est un contexte et une fonction `cancel` qui sera appelée si la deadline est dépassée. Pour cela on appelle `defer cancel()`. Et nous n'avons rien à faire dessus.

À partir de maintenant on peut appeler notre point terminal RPC. On va pouvoir appeler notre méthode `GreetWithDeadline` avec le contexte et une réponse.

La gestion de l'erreur va demander d'extraire comme on l'a vu au précédent chapitre les éléments de l'erreur à l'aide de la méthode `status.FromError()` à laquelle on passe notre erreur et on récupèrera un pointeur et un booléen. Avec le pointeur nous allons pouvoir vérifier le code, le message.

Enfin pour un premier test, et vérifier que tout fonctionne bien, on met un timeout de 5 secondes.

```go
func main() {
    log.Printf("Lancement du client sur %s", IP)

    cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
    if err != nil {
        log.Printf("Erreur à la connexion : %v", err)
    }
    defer cnx.Close()

    client := pb.NewGreetServiceClient(cnx)

    ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
    defer cancel()

    rep, err := client.GreetWithDeadline(ctx, &pb.Request{
        FirstName: "Alice",
    })

    if err != nil {
        e, ok := status.FromError(err)
        if ok {
            if e.Code() == codes.DeadlineExceeded {
                log.Printf("Erreur temps dépassé : %v", e.Message())
            } else {
                log.Printf("%v - %s", e.Code(), e.Message())
            }
            return
        }
        // si toute autre erreur
        log.Printf("Erreur non liée au gRPC : %v", err)
        return
    }

    log.Printf("Réponse : %+v", rep)
}
```

Si on passe le timeout à une seconde, le client va lever une erreur : "Erreur temps dépassé : context deadline exceeded"

**NB** : avec le code alternatif côté serveur, tout timeout de plus de 3 secondes sera parfaitement géré des deux côtés. On pourra tester à 1 seconde, 3 secondes et 4 secondes pour observer les différences de comportement de nos outils.

## 3-SSL

L'utilisation d'une connexion SSL/TLS est nécessaire pour un fonctionnement optimal de gRPC et surtout de HTTP/2.

Il va nous falloir générer des certificats pour tester en local des échanges sécurisés.

La création sous Unix et Unix-like ne devrait pas poser de soucis.

On dispose dans le répertoire [./8-SSL/ressources/](../codes/8-SSL/ressources/) les scripts nécessaires en fonction de l'OS !

### 1-Windows

il faut installer `openssl`. Le plus simple est de passer par `Chocolatey` et la commande `choco install openssl`. À noter que Windows fournit son propre outil pour créer des [certificats autosignés](http://woshub.com/how-to-create-self-signed-certificate-with-powershell/). Une autre solution est de recourir à WSL et une installation d'une distribution Linux puis utiliser les instructions pour Unix.

Si OpenSSL a été installé sur Windows :

```powershell
.\ssl.ps1
```

En cas de problème `SecurityError`:

```powershell
powershell -ExecutionPolicy unrestricted .\ssl.ps1
```

### 2-Environnements Unix

```shell
chmod +x ssl.sh
./ssl.sh
```

De manière superficielle, openssl va créer différents fichiers. Certains seront privés et d'autres à partager. Si on lit les différents fichiers, notre domaine est `localhost` et peut être remplacé par un nom de domaine (si on en possède un).

### 3-Utilisation

On se place dans le répertoire où est placé le script et on exécute... cela prendra un peu temps mais on obtiendra 6 nouveaux fichiers. Ces différents seront à placer dans les répertoires `serveur` et `client`

### 4-Fichier Protocol Buffer

On va se baser sur l'API la plus simple : l'API unaire.

L'architecture est la même mais on aura rajouté le répertoire `ressources` (cf[./codes/8-SSL](../codes/8-SSL/)). Et notre nom de module Go sera "ssl".

Donc notre fichier Protocol Buffer reprendra ce que nous avons ci-dessus :

```proto
syntax = "proto3";

package greet;

option go_package="ssl/proto";

message Request {
    string first_name = 1;
}

message Response {
    string result = 1;
}

service GreetService {
    rpc GreetWithDeadline (Request) returns (Response);
}
```

On compile le tout avec `protoc -Iproto --go_out=. --go_opt=module=ssl --go-grpc_out=. --go-grpc_opt=module=ssl proto/*.proto`

On n'oubliera pas le `go mod tidy` !

### 5-Serveur et SSL

Notre code de base est inchangé :

```go
package main

import (
    "context"
    "fmt"
    "log"
    "net"

    pb "grpc-ssl/proto"

    "google.golang.org/grpc"
)

type Server struct {
    pb.GreetServiceServer
}

const IP = "0.0.0.0:50051"

func main() {
    lst, err := net.Listen("tcp", IP)
    if err != nil {
        log.Fatalf("Erreur à l'écoute du port TCP [%s] :%v\n", IP, err)
    }
    defer lst.Close()

    log.Printf("Ecoute sur %s\n", IP)

    s := grpc.NewServer()
    pb.RegisterGreetServiceServer(s, &Server{})

    if err := s.Serve(lst); err != nil {
        log.Fatalf("Echec au lancement du serveur : %v", err)
    }

}

func (s *Server) Greet(ctx context.Context, req *pb.Request) (*pb.Response, error) {
    log.Printf("Fonction Greet (API unaire) appelée avec %v", req)
    return &pb.Response{Result: fmt.Sprintf("Hello %s", req.GetFirstName())},
        nil
}
```

C'est au moment de la création du serveur grpc que l'on va passer le certificat sécurisé.

Avant tout nous allons créer deux variables :

- une basique : un booléen permet de basculer en mode SSL/TLS ou non (initialisé à `true`)
- un slice d'options pour le serveur de type `[]grpc.ServerOption` (vide)

Dans une condition if vérifiant que l'on est en mode sécurisé, nous allons créer une crédance à partir de la bibliothèque `google.golang.org/grpc/credentials`. Appelons la fonction `credentials.NewServerTLSFromFile()` en lui passant deux références vers des fichiers cert et key. Le fichier cert est `server.crt` et le fichier key est `server.pem`. Cette opération nous retournera deux valeurs :

- un type `credentials.TransportCredentials`
- une erreur

Et la crédance récupérée sera placée dans le slice d'options pour le serveur.

Enfin nous devons modifier l'appel de `NewServer` en lui passant nos options :

```go
func main() {

    log.Printf("Lancement du serveur sur %s", IP)

    cnx, err := net.Listen("tcp", IP)
    if err != nil {
        log.Printf("Connexion impossible : %v", err)
        return
    }
    defer cnx.Close()

    tls := false
    opts := []grpc.ServerOption{}

    if tls {
        cred, err := credentials.NewServerTLSFromFile("../ressources/unix/server.crt", "../ressources/unix/server.pem")
        if err != nil {
            log.Printf("Erreur à récupération des fichiers pour la connexion sécurisée : %v", err)
            return
        }
        opts = append(opts, grpc.Creds(cred))
    }

    s := grpc.NewServer(opts...)
    pb.RegisterGreetServiceServer(s, &Server{})
    if err := s.Serve(cnx); err != nil {
        log.Printf("Erreur au lancement du serveur : %v", err)
        return
    }
}

```

Nous pouvons lancer notre serveur...

### 6-Client

... et tester à la fois avec `tls` avec `true` et `false` avec notre client. Avec `false` tout devrait être bon. Avec `true`, nous devrions avoir une erreur

Côté du client nous devons modifier le type de connexion avec le serveur. Si on teste avec le code suivant :

```go
package main

import (
    "context"
    "log"

    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials/insecure"

    pb "grpc-ssl/proto"
)

const addr = "localhost:50051"

func main() {
    cnx, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
    if err != nil {
        log.Fatalf("Erreur à la connexion vers le serveur : %v\n", err)
    }
    defer cnx.Close()

    // Création d'un nouveau client
    client := pb.NewGreetServiceClient(cnx)
    // envoi d'une requête et récupération de la réponse
    rep, err := client.Greet(context.Background(), &pb.Request{FirstName: "John"})
    log.Println("Envoi d'une requête")
    if err != nil {
        log.Fatalf("Erreur : %v\n", err)
    }
    log.Printf("Réponse du serveur : %v\n", rep.GetResult())
}
```

Nous obtenons une erreur :

```txt
Erreur à l'envoi de la requête : rpc error: code = Unavailable desc = connection closed before server preface received
```

à nouveau nous allons créer, comme pour le serveur, une variable de test booléenne `tls` et un slice de `grpc.DialOption`

Pour créer la crédance sécurisée nous devrons appeler la fonction `credentials.NewClient TLSFromFiLe()` qui prend 2 arguments :

- fichier `ca.cert`
- sera une chaine vide car nous fonctionnons en mode `localhost`

Puis on ajoutera la crédence créée à notre slice d'options en l'empaquetant avec `grpc.WithTransportCredentials()`

Enfin nous remplacerons le second argument de `grpc.Dial()` avec notre variable `opts` "dépliée" :

```go
package main

import (
    "context"
    "log"

    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials"
    "google.golang.org/grpc/credentials/insecure"

    pb "grpc-ssl/proto"
)

const addr = "localhost:50051"

func main() {

    tls := true
    opts := []grpc.DialOption{}

    if tls {
        cert := "../ressources/unix/ca.crt"
        c, err := credentials.NewClientTLSFromFile(cert, "")
        if err != nil {
            log.Printf("Erreur à la récupération des certificats : %v", err)
            return
        }
        opts = append(opts, grpc.WithTransportCredentials(c))

    } else {
        opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
    }

    cnx, err := grpc.Dial(addr, opts...)
    if err != nil {
        log.Fatalf("Erreur à la connexion vers le serveur : %v\n", err)
    }
    defer cnx.Close()

    client := pb.NewGreetServiceClient(cnx)
    rep, err := client.Greet(context.Background(), &pb.Request{FirstName: "John"})
    log.Println("Envoi d'une requête")
    if err != nil {
        log.Fatalf("Erreur à l'envoi de la requête : %v\n", err)
    }
    log.Printf("Réponse du serveur : %v\n", rep.GetResult())
}
```

## 4-Evans

Evans est un logiciel en ligne de commande permettant d'interroger les services grpc d'un serveur. C'est un logiciel libre disponible sur [Github](https://github.com/ktr0731/evans).

Il suffira d'aller dans `releases` pour récupérer les derniers binaires. Comme il est écrit en Go, il n'y a aucune dépendance à installer.

Si on appelle le soft sans paramètre, on tombe sur l'aide.

Pour se connecter à un serveur :

- il faut le nom du serveur (ici "localhost")
- son port (50051)
- utiliser le paramètre `-r` pour la réflexion
- et indiquer que l'on veut utiliser le client en mode REPL

```sh
evans --host localhost --port 50051 -r repl
```

Pour que cela soit possible, il faut modifier le serveur pour autoriser les opérations de réflexivité :

- on doit importer le module `google.golang.org/grpc/reflection`
- il faudra appeler la fonction `reflection.Register()`
- lui passer en argument le serveur gRPC (créé avec `grpc.NewServer()`)

et c'est tout :

```go
package main

import (
    "context"
    "log"
    "net"

    pb "reflexivite/proto"

    "google.golang.org/grpc"
    "google.golang.org/grpc/reflection"
)

type Server struct {
    pb.GreetServiceServer
}

const addr = "0.0.0.0:50051"

func main() {
    lst, err := net.Listen("tcp", addr)
    if err != nil {
        log.Fatalf("Erreur à l'écoute du port TCP [%s] :%v\n", addr, err)
    }
    defer lst.Close()

    log.Printf("Ecoute sur %s\n", addr)

    s := grpc.NewServer()
    pb.RegisterGreetServiceServer(s, &Server{})
    reflection.Register(s)
    if err := s.Serve(lst); err != nil {
        log.Fatalf("Echec au lancement du serveur : %v", err)
    }

}
```

Il suffit de lancer le serveur et dans un autre terminal le client evans.

La documentation est très explicite pour gérer les commandes. Cela va nous permettre d'interroger le serveur :

- sur les packages disponibles (`show package`)
- sur les messages (`show message`) et services (`show service`)

Mais on peut interroger le serveur et lui envoyer des données. Tout d'abord, il faut lui indiquer quel service employer avec la commande `service` suivi du nom du service. Puis on appelle `call` suivi du nom de la fonction RPC à appeler.

**NB** : Evans permet l'autocomplétion, il "sait" quels sont les packages, services, fonctions et messages disponibles. L'utilisateur n'est jamais perdu.

**NB** : dans le cas du streaming, pour arrêter l'envoi ou la réception des données : ctrl + d
