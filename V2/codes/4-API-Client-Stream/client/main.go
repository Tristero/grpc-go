package main

import (
	"context"
	"log"
	"strconv"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	pb "api-client-stream/proto"
)

const IP = "localhost:50051"

func main() {

	log.Println("Lancement du client")
	cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Printf("Erreur à la création du client : %v", err)
		return
	}
	defer cnx.Close()

	client := pb.NewGreetServiceClient(cnx)

	log.Println("Envoi des données...")
	stream, err := client.LongGreetRequest(context.Background())

	if err != nil {
		log.Printf("Une erreur est survenue dans le lancement du stream : %v", err)
	}

	for i := 0; i < 5; i++ {
		req := &pb.GreetRequest{
			FirstName: strconv.Itoa(i),
		}

		log.Printf("Envoi de la requête : %+v", req)

		if err := stream.SendMsg(req); err != nil {
			log.Printf("Erreur à la réception des données : %v", err)
		}

		// mise en place d'une pause.
		time.Sleep(1 * time.Second)
	}

	rep, err := stream.CloseAndRecv()
	if err != nil {
		log.Printf("Une erreur inattendue est survenue à la fermeture de la connexion : %v", err)
	}
	log.Printf("Réponse du serveur : %v", rep)
}
