package main

import (
	"fmt"
	"io"
	"log"
	"net"

	pb "api-client-stream/proto"

	"google.golang.org/grpc"
)

const IP = "0.0.0.0:50051"

type Server struct {
	pb.GreetServiceServer
}

func main() {
	log.Println("Lancement du serveur")

	lst, err := net.Listen("tcp", IP)
	if err != nil {
		log.Printf("Erreur à l'écoute sur %v", IP)
		return
	}
	defer lst.Close()

	s := grpc.NewServer()
	pb.RegisterGreetServiceServer(s, &Server{})
	if err := s.Serve(lst); err != nil {
		log.Printf("Une erreur est survenue dans le lancement du serveur : %v", err)
		return
	}
}

func (s Server) LongGreetRequest(stream pb.GreetService_LongGreetRequestServer) error {
	log.Println("Appel de la méthode LongGreetRequest")

	i := 0
	for {
		_, err := stream.Recv()

		if err == io.EOF {
			log.Println("Fin de réception des données")
			return stream.SendAndClose(&pb.GreetResponse{
				Result: fmt.Sprintf("%d messages reçus par le serveur", i),
			})
		}

		if err != nil {
			log.Printf("Une erreur inattendue est survenue : %v", err)
			break
		}

		i++
	}

	log.Printf("Nombre de messages reçus : %d", i)
	return nil
}
