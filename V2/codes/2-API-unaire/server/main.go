package main

import (
	"context"
	"fmt"
	"log"
	"net"

	pb "apiunaire/proto"

	"google.golang.org/grpc"
)

type Server struct {
	pb.GreetServiceServer
}

const addr = "0.0.0.0:50051"

func main() {
	lst, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("Erreur à l'écoute du port TCP [%s] :%v\n", addr, err)
	}
	defer lst.Close()

	log.Printf("Ecoute sur %s\n", addr)

	s := grpc.NewServer()
	pb.RegisterGreetServiceServer(s, &Server{})

	if err := s.Serve(lst); err != nil {
		log.Fatalf("Echec au lancement du serveur : %v", err)
	}

}

func (s *Server) Greet(ctx context.Context, req *pb.GreetRequest) (*pb.GreetResponse, error) {
	log.Printf("Fonction Greet (API unaire) appelée avec %v", req)
	return &pb.GreetResponse{Result: fmt.Sprintf("Hello %s", req.GetFirstName())},
		nil
}
