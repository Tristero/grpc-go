package main

import (
	"context"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	pb "apiunaire/proto"
)

const addr = "localhost:50051"

func main() {
	cnx, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("Erreur à la connexion vers le serveur : %v\n", err)
	}
	defer cnx.Close()

	// Création d'un nouveau client
	client := pb.NewGreetServiceClient(cnx)
	// envoi d'une requête et récupération de la réponse
	rep, err := client.Greet(context.Background(), &pb.GreetRequest{FirstName: "John"})
	log.Println("Envoi d'une requête")
	if err != nil {
		log.Fatalf("Erreur : %v\n", err)
	}
	log.Printf("Réponse du serveur : %v\n", rep.GetResult())
}
