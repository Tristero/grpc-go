package main

import (
	"context"
	"io"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/types/known/emptypb"

	pb "blog/proto"
)

const IP = "localhost:50051"

func main() {
	log.Println("Lancement du client")

	cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Printf("Erreur au lancement du client sur %s, %v", IP, err)
		return
	}
	defer cnx.Close()

	client := pb.NewBlogServiceClient(cnx)

	// création d'un post
	id := createPostBlog(client)

	// lecture du post envoyé
	readBlog(client, id)
	readBlog(client, "trucbidule")

	// mise à jour du blog
	nvBlog := &pb.Blog{
		Id:       id,
		AuthorId: "Bob et charlie",
		Title:    "Nouveau titre",
	}

	updateBlog(client, nvBlog)

	// récupération de tous les posts
	listBlog(client)

	// suppression du post
	deleteBlog(client, id)
}

func createPostBlog(client pb.BlogServiceClient) string {
	id, err := client.CreateBlog(context.Background(),
		&pb.Blog{
			AuthorId: "John Doe",
			Title:    "Premier post",
			Content:  "Duis ex amet esse culpa minim exercitation dolore labore dolore incididunt esse ea pariatur. Sint cupidatat ad sint voluptate mollit. Voluptate ad Lorem aute officia fugiat sunt cupidatat Lorem duis est. Cillum cillum incididunt culpa incididunt ut irure ad nisi mollit sunt sint. Sint enim commodo magna excepteur deserunt nisi consequat magna cupidatat aliqua ex est commodo. Occaecat aliqua ad deserunt dolor ullamco ullamco excepteur elit.",
		})

	if err != nil {
		log.Printf("Erreur à la création du post : %v", err)
	}

	log.Printf("Post enregistré avec l'id : %v", id.Id)
	return id.Id
}

func readBlog(client pb.BlogServiceClient, id string) *pb.Blog {
	log.Printf("Demande de lire un post ayant l'id %s", id)

	rslt, err := client.ReadBlog(context.Background(), &pb.BlogId{
		Id: id,
	})

	if err != nil {
		log.Printf("Erreur à l'envoi de l'ID : %v", err)
	}

	log.Printf("Post récupéré avec succès : %+v", rslt)
	return rslt
}

func updateBlog(client pb.BlogServiceClient, blog *pb.Blog) {
	log.Printf("Appel d'une mise à jour d'un post (id=%v)", blog.Id)

	_, err := client.UpdateBlog(context.Background(), blog)
	if err != nil {
		log.Printf("Erreur lors de la mise à jour : %v", err)
	}

	log.Println("Post mis à jour avec succès")
}

func listBlog(client pb.BlogServiceClient) {
	log.Println("Demande de la liste complète des posts")

	stream, err := client.ListBlogs(context.Background(), &emptypb.Empty{})

	if err != nil {
		log.Printf("Une erreur est survenue : %v", err)
	}

	for {
		doc, err := stream.Recv()

		if err == io.EOF {
			log.Println("Fin de transmission des données")
			break
		}

		if err != nil {
			log.Printf("Une erreur inconnue est survenue : %v", err)
			return
		}

		log.Printf("%+v", doc)
	}
}

func deleteBlog(client pb.BlogServiceClient, id string) {
	log.Println("Suppression d'un post")

	_, err := client.DeleteBlog(context.Background(), &pb.BlogId{Id: id})
	if err != nil {
		log.Printf("Erreur rencontrée : %v", err)
		return
	}

	log.Printf("Suppression du post (id: %s)", id)
}
