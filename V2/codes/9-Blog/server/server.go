package main

import (
	"context"
	"fmt"
	"log"
	"net"

	pb "blog/proto"

	"github.com/golang/protobuf/ptypes/empty"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

const IP = "50051"

type Server struct {
	pb.BlogServiceServer
}

var collection *mongo.Collection

func main() {
	log.Printf("Lancement du serveur sur %s", IP)

	// gestion de mongoDB
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://root:root@localhost:27017/"))
	if err != nil {
		log.Printf("Erreur à la création du client : %v", err)
		return
	}

	// gestion de la connexion avec Mongo
	if err := client.Connect(context.Background()); err != nil {
		log.Printf("Erreur à la connexion")
	}

	// Récupération de la collection
	collection = client.Database("blogdb").Collection("blog")

	lst, err := net.Listen("tcp", IP)
	if err != nil {
		log.Printf("Erreur au lancement du serveur : %v", err)
		return
	}
	defer lst.Close()

	s := grpc.NewServer()
	pb.RegisterBlogServiceServer(s, &Server{})
	if err := s.Serve(lst); err != nil {
		log.Printf("Erreur au lancement du serveur gRPC : %v", err)
		return
	}
}

func (s Server) CreateBlog(ctx context.Context, blog *pb.Blog) (*pb.BlogId, error) {
	log.Println("Création d'un post")
	item := BlogItem{
		AuthorId: blog.AuthorId,
		Title:    blog.Title,
		Content:  blog.Content,
	}

	rslt, err := collection.InsertOne(ctx, item)
	if err != nil {
		log.Printf("Une erreur est survenue à l'insertion du document %v : %v", item, err)
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("erreur à l'insertion du document : %v", err),
		)
	}

	oid, ok := rslt.InsertedID.(primitive.ObjectID)
	if !ok {

		return nil, status.Errorf(codes.Internal, "Convertion impossible de l'ObjectID")

	}

	return &pb.BlogId{
			Id: string(oid.Hex()),
		},
		nil
}

func (s Server) ReadBlog(ctx context.Context, id *pb.BlogId) (*pb.Blog, error) {
	log.Printf("Demande de lecture d'un post (id=%v)", id.Id)
	post := &pb.Blog{}

	oid, err := primitive.ObjectIDFromHex(id.Id)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "L'id n'est pas valide.")
	}

	rslt := collection.FindOne(ctx, bson.M{"_id": oid})

	if err := rslt.Decode(post); err != nil {
		return nil, status.Errorf(
			codes.Internal,
			"Impossible de décoder les données",
		)
	}

	return post, nil
}

func (s Server) UpdateBlog(ctx context.Context, in *pb.Blog) (*empty.Empty, error) {
	log.Printf("Appel de UpdateBlog avec %v", in)

	oid, err := primitive.ObjectIDFromHex(in.Id)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "ID erroné")
	}

	item := BlogItem{
		ID:       oid,
		AuthorId: in.AuthorId,
		Title:    in.Title,
		Content:  in.Content,
	}

	rslt, err := collection.UpdateOne(ctx, bson.M{"_id": oid}, bson.M{"$set": item})

	if err == nil {
		return nil, status.Errorf(
			codes.Internal,
			"Mise à jour impossible",
		)
	}

	if rslt.MatchedCount == 0 {
		return nil, status.Errorf(
			codes.NotFound,
			"Aucun document trouvé",
		)
	}

	return &emptypb.Empty{}, nil
}

func (s Server) ListBlogs(e *empty.Empty, stream pb.BlogService_ListBlogsServer) error {
	log.Println("Appel de la liste complète des posts")

	ctx := context.Background()

	// on récupère la liste complète de la collection : pas de filtrage
	cur, err := collection.Find(ctx, primitive.D{{}})
	if err != nil {
		return status.Errorf(
			codes.Internal,
			fmt.Sprintf("Impossible de récupérer les données du serveur de Mongo: %v", err),
		)
	}

	// ne pas oublier de cloturer le curseur
	defer cur.Close(ctx)

	for cur.Next(ctx) { // code s'arrêtera quand Next() sera épuisé (retourne False)
		// création d'un document
		doc := &BlogItem{}

		if err := cur.Decode(doc); err != nil {
			return status.Errorf(
				codes.Internal,
				fmt.Sprintf("Erreur au décodage du document : %v", err),
			)
		}
		// notre fonction va convertir un type BlogItem en type Blog
		// attendu par la méthode Send de stream
		stream.Send(DocumentToBlog(*doc))

	}
	// en cas d'erreur sur le curseur :
	if err := cur.Err(); err != nil {
		return status.Errorf(
			codes.Internal,
			fmt.Sprintf("Erreur interne inconnue : %v", err),
		)
	}
	return nil
}

func (s Server) DeleteBlog(ctx context.Context, id *pb.BlogId) (*empty.Empty, error) {
	log.Printf("Suppression d'un post avec l'id %s", id.Id)

	oid, err := primitive.ObjectIDFromHex(id.Id)

	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			"ID erroné",
		)
	}

	rslt, err := collection.DeleteOne(ctx, bson.M{"_id": oid})

	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("Objet impossible à supprimer : %v", err),
		)
	}

	if rslt.DeletedCount == 0 {
		return nil, status.Error(
			codes.NotFound,
			"Le post n'a pas été traité",
		)
	}

	return &emptypb.Empty{}, nil

}
