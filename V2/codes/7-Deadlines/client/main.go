package main

import (
	"context"
	"log"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"

	pb "deadlines/proto"
)

const IP = "localhost:50051"

func main() {
	log.Printf("Lancement du client sur %s", IP)

	cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Printf("Erreur à la connexion : %v", err)
	}
	defer cnx.Close()

	client := pb.NewGreetServiceClient(cnx)

	ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
	defer cancel()

	rep, err := client.GreetWithDeadline(ctx, &pb.Request{
		FirstName: "Alice",
	})

	if err != nil {
		e, ok := status.FromError(err)
		if ok {
			if e.Code() == codes.DeadlineExceeded {
				log.Printf("Erreur temps dépassé : %v", e.Message())
			} else {
				log.Printf("%v - %s", e.Code(), e.Message())
			}
			return
		}
		// si toute autre erreur
		log.Printf("Erreur non liée au gRPC : %v", err)
		return
	}

	log.Printf("Réponse : %+v", rep)
}
