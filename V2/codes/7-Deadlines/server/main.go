package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"time"

	pb "deadlines/proto"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Server struct {
	pb.GreetServiceServer
}

const IP = "0.0.0.0:50051"

func main() {
	lst, err := net.Listen("tcp", IP)
	if err != nil {
		log.Fatalf("Erreur à l'écoute du port TCP [%s] :%v\n", IP, err)
	}
	defer lst.Close()

	log.Printf("Ecoute sur %s\n", IP)

	s := grpc.NewServer()
	pb.RegisterGreetServiceServer(s, &Server{})

	if err := s.Serve(lst); err != nil {
		log.Fatalf("Echec au lancement du serveur : %v", err)
	}

}

func (s *Server) GreetWithDeadline(ctx context.Context, req *pb.Request) (*pb.Response, error) {
	log.Printf("Appel de GreetWithDeadline avec %v", req)

	time.Sleep(3 * time.Second)

	deadline := time.Now().Add(1 * time.Second)
	ctx, cancel := context.WithDeadline(ctx, deadline)
	defer cancel()

	if err := ctx.Err(); err != nil {
		log.Printf("Le client n'est plus connecté : %v", err)
		return nil, status.Error(codes.DeadlineExceeded, "Temps d'attente trop long")
	}

	log.Println("Envoi réponse")

	return &pb.Response{Result: fmt.Sprintf("Hello %s", req.GetFirstName())},
		nil

}
