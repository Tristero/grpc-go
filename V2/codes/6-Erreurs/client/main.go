package main

import (
	"context"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"

	pb "erreur/proto"
)

const IP = "localhost:50051"

func main() {
	log.Printf("Lancement du client sur %v", IP)
	cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Printf("Erreur à la connexion sur %s : %v", IP, err)
	}
	defer cnx.Close()

	service := pb.NewSquareRootServiceClient(cnx)
	rep, err := service.SqrtCalculator(context.Background(), &pb.Request{
		Value: -16,
	})
	if err != nil {
		s, ok := status.FromError(err)
		if ok {
			log.Printf("[Code : %v] %v", s.Code(), s.Message())
			if s.Code() == codes.InvalidArgument {
				log.Println("L'argument passé est sans doute négatif.")
			}
		} else {
			log.Printf("Erreur inconnue : %v", err)
		}
		return
	}
	log.Println(rep.GetResult())
}
