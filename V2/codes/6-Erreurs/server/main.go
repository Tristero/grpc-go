package main

import (
	"context"
	"log"
	"math"
	"net"

	pb "erreur/proto"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const IP = "0.0.0.0:50051"

type Server struct {
	pb.SquareRootServiceServer
}

func main() {
	log.Printf("Lancement du serveur sur %v", IP)

	lst, err := net.Listen("tcp", IP)
	if err != nil {
		log.Printf("Erreur à l'écoute de %s : %v", IP, err)
	}
	defer lst.Close()

	s := grpc.NewServer()
	pb.RegisterSquareRootServiceServer(s, &Server{})
	if err := s.Serve(lst); err != nil {
		log.Printf("Erreur survenue au lancement du serveur : %v", err)
		return
	}
}

func (s Server) SqrtCalculator(ctx context.Context, req *pb.Request) (*pb.Response, error) {

	val := req.GetValue()

	log.Printf("Appel du calcul de la racine carrée de %d", val)

	if val >= 0 {
		rslt := math.Sqrt(float64(val))
		return &pb.Response{
			Result: rslt,
		}, nil
	}
	return nil, status.Errorf(codes.InvalidArgument, "La valeur transmise %d n'est pas positive", val)
}
