package main

import (
	"context"
	"io"
	"log"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	pb "api-bidi/proto"
)

const IP = "localhost:50051"

func main() {

	log.Println("Lancement du client")
	cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Printf("Erreur à la création du client : %v", err)
		return
	}
	defer cnx.Close()

	client := pb.NewGreetServiceClient(cnx)

	log.Println("Envoi de données ...")

	stream, err := client.BidirectionalGreet(context.Background())
	if err != nil {
		log.Printf("Erreur à l'appel du service : %v", err)
		return
	}

	noms := []string{"Alice", "Bob", "Charlie"}

	go func() {
		for _, nom := range noms {
			req := &pb.GreetRequest{
				FirstName: nom,
			}
			log.Printf("Envoi de la requête %+v", req)
			if err := stream.Send(req); err != nil {
				log.Printf("Erreur survenue à l'envoi : %v", err)
				break
			}
			time.Sleep(1 * time.Second)
		}
		if err := stream.CloseSend(); err != nil {
			log.Printf("Erreur à la fermeture de l'envoi :%v", err)
		}
	}()

	semaphore := make(chan struct{})

	go func() {
		for {
			rep, err := stream.Recv()
			if err == io.EOF {
				log.Printf("Fin de transmission")
				break
			}
			if err != nil {
				log.Printf("Une erreur inattendue est survenue : %v", err)
				break
			}
			log.Printf("Réponse reçue du serveur : %v", rep)
		}
		if err := stream.CloseSend(); err != nil {
			log.Printf("Erreur à la fermeture du stream : %v", err)
		}

		// close(semaphore)
		semaphore <- struct{}{}
	}()

	<-semaphore
}
