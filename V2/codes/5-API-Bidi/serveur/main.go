package main

import (
	"fmt"
	"io"
	"log"
	"net"

	pb "api-bidi/proto"

	"google.golang.org/grpc"
)

const IP = "0.0.0.0:50051"

type Server struct {
	pb.GreetServiceServer
}

func main() {
	log.Println("Lancement du serveur")

	lst, err := net.Listen("tcp", IP)
	if err != nil {
		log.Printf("Erreur à l'écoute sur %v", IP)
		return
	}
	defer lst.Close()

	s := grpc.NewServer()
	pb.RegisterGreetServiceServer(s, &Server{})
	if err := s.Serve(lst); err != nil {
		log.Printf("Une erreur est survenue dans le lancement du serveur : %v", err)
		return
	}
}

func (s Server) BidirectionalGreet(stream pb.GreetService_BidirectionalGreetServer) error {
	log.Println("Appel de la méthode BidirectionalGreet")

	// écoute du client
	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			log.Println("Fin de transmission des lsdonnées")
			return nil
		}
		if err != nil {
			log.Println("Erreur lors de la lecture d'un message reçu")
			return err
		}
		log.Printf("Réception d'un message %s", msg.GetFirstName())
		rep := &pb.GreetResponse{
			Result: fmt.Sprintf("Hello, %s !", msg.GetFirstName()),
		}

		if err := stream.SendMsg(rep); err != nil {
			log.Println("Erreur lors de l'envoi d'une réponse.")
			return err
		}
	}
}
