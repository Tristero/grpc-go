package main

import (
	"fmt"
	"log"
	"net"
	"time"

	pb "api-server-stream/proto"

	"google.golang.org/grpc"
)

const IP = "0.0.0.0:50051"

type Server struct {
	pb.GreetServiceServer
}

func main() {

	lst, err := net.Listen("tcp", IP)
	if err != nil {
		log.Fatalf("Impossible d'écouter sur %v - %v", IP, err)
	}
	defer lst.Close()

	log.Printf("Lancement du serveur sur %v", IP)

	s := grpc.NewServer()
	pb.RegisterGreetServiceServer(s, &Server{})
	if err := s.Serve(lst); err != nil {
		log.Fatalf("Erreur est survenue au lancement du serveur : %v", err)
	}
}

func (s Server) GreetManyTimes(req *pb.GreetRequest, stream pb.GreetService_GreetManyTimesServer) error {
	log.Printf("Appel de la fonction GreetManyTimes avec %+v", req)
	for i := 0; i < 10; i++ {
		// simulation d'un temps de latence
		time.Sleep(1 * time.Second)

		rslt := fmt.Sprintf("Résultat n°%d - hello %s", i+1, req.GetFirstName())
		stream.Send(&pb.GreetResponse{
			Result: rslt,
		})
	}
	return nil
}
