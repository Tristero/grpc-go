package main

import (
	"context"
	"io"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	pb "api-server-stream/proto"
)

const IP = "localhost:50051"

func main() {
    cnx, err := grpc.Dial(IP, grpc.WithTransportCredentials(insecure.NewCredentials()))
    if err != nil {
        log.Fatalf("Erreur à la création d'une connexion : %v", err)
    }
    defer cnx.Close()

    client := pb.NewGreetServiceClient(cnx)
    log.Println("Récupération des données envoyées en flux")
    stream, err := client.GreetManyTimes(context.Background(), &pb.GreetRequest{
        FirstName: "Alice",
    })

    if err != nil {
        log.Printf("Erreur à l'appel de la méthode GreetManyTimes : %v", err)
        return
    }

    for {
        rep, err := stream.Recv()

        // fin de transmission
        if err == io.EOF {
            log.Println("Fin de réception des données")
            break
        }

        // en cas d'erreur autre que la fin de réception de données
        if err != nil {
            log.Printf("Une erreur inconnue a été levée : %v", err)
            break
        }

        // réception des données
        log.Printf("Réponse reçue : %s", rep.GetResult())
    }
}
