package main

import (
	"log"
	"net"

	"google.golang.org/grpc"
)

const addr = "0.0.0.0:50051"

func main() {
	lst, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("Erreur à l'écoute du port TCP [%s] :%v\n", addr, err)
	}
	defer lst.Close()

	log.Printf("Ecoute sur %s\n", addr)

	s := grpc.NewServer()

	if err := s.Serve(lst); err != nil {
		log.Fatalf("Echec au lancement du serveur : %v", err)
	}

}
