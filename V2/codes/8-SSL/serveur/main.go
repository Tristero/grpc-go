package main

import (
	"context"
	"fmt"
	"log"
	"net"

	pb "grpc-ssl/proto"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

type Server struct {
	pb.GreetServiceServer
}

const IP = "0.0.0.0:50051"

func main() {
	lst, err := net.Listen("tcp", IP)
	if err != nil {
		log.Fatalf("Erreur à l'écoute du port TCP [%s] :%v\n", IP, err)
	}
	defer lst.Close()

	log.Printf("Ecoute sur %s\n", IP)

	tls := true
	opts := []grpc.ServerOption{}

	if tls {
		cert := "../ressources/unix/server.crt"
		key := "../ressources/unix/server.pem"

		c, err := credentials.NewServerTLSFromFile(cert, key)
		if err != nil {
			log.Printf("Erreur au chargement des certificats : %v", err)
			return
		}

		opts = append(opts, grpc.Creds(c))
	}

	s := grpc.NewServer(opts...)
	pb.RegisterGreetServiceServer(s, &Server{})

	if err := s.Serve(lst); err != nil {
		log.Fatalf("Echec au lancement du serveur : %v", err)
	}

}

func (s *Server) Greet(ctx context.Context, req *pb.Request) (*pb.Response, error) {
	log.Printf("Fonction Greet (API unaire) appelée avec %v", req)
	return &pb.Response{Result: fmt.Sprintf("Hello %s", req.GetFirstName())},
		nil
}
