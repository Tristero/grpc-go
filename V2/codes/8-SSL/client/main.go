package main

import (
	"context"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"

	pb "grpc-ssl/proto"
)

const addr = "localhost:50051"

func main() {

	tls := true
	opts := []grpc.DialOption{}

	if tls {
		cert := "../ressources/unix/ca.crt"
		c, err := credentials.NewClientTLSFromFile(cert, "")
		if err != nil {
			log.Printf("Erreur à la récupération des certificats : %v", err)
			return
		}
		opts = append(opts, grpc.WithTransportCredentials(c))

	} else {
		opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	}

	cnx, err := grpc.Dial(addr, opts...)
	if err != nil {
		log.Fatalf("Erreur à la connexion vers le serveur : %v\n", err)
	}
	defer cnx.Close()

	client := pb.NewGreetServiceClient(cnx)
	rep, err := client.Greet(context.Background(), &pb.Request{FirstName: "John"})
	log.Println("Envoi d'une requête")
	if err != nil {
		log.Fatalf("Erreur à l'envoi de la requête : %v\n", err)
	}
	log.Printf("Réponse du serveur : %v\n", rep.GetResult())
}
